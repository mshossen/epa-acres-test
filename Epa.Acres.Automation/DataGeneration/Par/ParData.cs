﻿using System;
using Models.TestDataModel;
using FizzWare.NBuilder;

namespace DataGeneration.Par
{
    public static class ParData
    {
        private static readonly string[] AccomplishmentTypes = new[]
        {
            "All",
            "Assessments Completed",
            "Cleanups Completed",
            "Dollars Leveraged",
            "Jobs Leveraged",
            "Acres and Properties Ready for Anticipated Use (RAU)"
        };

        private static readonly string[] CaTypes = new[]
        {
            "All",
            "Assessment Cooperative Agreements",
            "Cleanup Cooperative Agreements",
            "RLF Cooperative Agreements",
            "Multipurpose Cooperative Agreements",
            "TBAs",
            "Section 128(a) State/Tribal Cooperative Agreements",
            "Tribal Only Cooperative Agreements"
        };

        public static ProgramAccomplishmentReportModel CreateRegionTenParData()
        {
            var builder = Builder<ProgramAccomplishmentReportModel>.CreateNew()
                .With(p => p.SelectRandomAccomplishmentTypes = new[] {AccomplishmentTypes[0]})
                .With(p => p.SelectRandomCaTypes = new[] {CaTypes[0]})
                .With(p => p.Year = 19)
                .With(p => p.Region = 10)
                .With(p => p.State = "Idaho")
                .Build();

            return builder;
        }

        public static ProgramAccomplishmentReportModel CreateParData()
        {
            var builder = Builder<ProgramAccomplishmentReportModel>.CreateNew()
                .With(p => p.SelectRandomAccomplishmentTypes = new[] {AccomplishmentTypes[0]})
                .With(p => p.SelectRandomCaTypes = new[] {CaTypes[0]})
                .With(p => p.Year = int.Parse(DateTime.Now.ToString("yy")) - new Random().Next(3))
                .With(p => p.Region = 10)
                .With(p => p.State = "Idaho")
                .Build();

            return builder;
        }
    }
}