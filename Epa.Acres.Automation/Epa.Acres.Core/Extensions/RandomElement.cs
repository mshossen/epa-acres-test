﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Epa.Acres.Core.Extensions
{
    public static class RandomElement
    {
        public static T SelectARandomElement<T>(this IEnumerable<T> collection)
        {
            if (collection is null) throw new ArgumentNullException(nameof(collection));

            var random = new Random();
            var list = collection as IList<T> ?? collection.ToList();

            return list.Count == 0
                ? default
                : list[random.Next(0, list.Count)];
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> collection, int take)
        {
            var random = new Random();
            var enumerable = collection as T[] ?? collection.ToArray();
            var available = enumerable.Count();
            var needed = take;
            var returnList = new List<T>();

            foreach (var item in enumerable)
            {
                if (random.Next(available) < needed)
                {
                    needed--;
                    returnList.Add(item);

                    if (needed == 0) break;
                }

                available--;
            }

            return returnList;
        }
    }
}