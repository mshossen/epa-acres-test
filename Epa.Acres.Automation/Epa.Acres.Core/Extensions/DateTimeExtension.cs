﻿using System;

namespace Epa.Acres.Core.Extensions
{
    public static class DateTimeExtension
    {
        public static string ToFinancialYearShort(this DateTime dateTime)
        {
            return dateTime.Month >= 4
                ? $"FY{dateTime.AddYears(1).Year:yy}"
                : $"FY{(int) dateTime.Year - 2000}";
        }

        public static string ToFiscalYear(this DateTime dateTime)
        {
            return dateTime.Month >= 4
                ? $"FY{dateTime.AddYears(0):yy}"
                : $"FY{dateTime.AddYears(-1):yy}";
        }
    }
}