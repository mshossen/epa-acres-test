﻿using System.Linq;
using Newtonsoft.Json;

namespace Epa.Acres.Core.Extensions
{
    /// <summary>
    /// Json Schema Validation
    /// </summary>
    public static class JsonSchemaExtension
    {
        /// <summary>
        /// extension that validates if Json string is copmplient to TSchema.
        /// Solution found in StakoverFlow
        /// https://stackoverflow.com/questions/19544183/validate-json-against-json-schema-c-sharp
        /// </summary>
        /// <typeparam name="TSchema">schema</typeparam>
        /// <param name="value">json string</param>
        /// <returns>is valid? Boolean</returns>
        public static bool IsValidJson<TSchema>(this string value) where TSchema : class, new()
        {
            //JsonConvert serializer = new JsonConvert();
            var deserializeObject = JsonConvert.DeserializeObject<TSchema>(value);
            var properties = typeof(TSchema).GetProperties();

            var propertyValue = deserializeObject.GetType().GetProperty(
                properties.Select(p => p.Name).FirstOrDefault()
            ).GetValue(deserializeObject, null);

            return propertyValue is null
                ? false
                : true;
        }
    }
}