﻿using System;
using System.ComponentModel;

namespace Epa.Acres.Core.Extensions
{
    public static class EnumExtension
    {
        public static string GetDescription(this Enum enumType)
        {
            var attributes = (DescriptionAttribute[]) enumType
                .GetType()
                .GetField(enumType.ToString())
                ?.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes?.Length > 0
                ? attributes[0].Description
                : string.Empty;
        }

        public static T GetValue<T>(string description) where T : Enum
        {
            foreach (var field in typeof(T).GetFields())
                if (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description == description)
                        return (T) field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T) field.GetValue(null);
                }

            throw new ArgumentException("Not found.", nameof(description));
        }
    }
}