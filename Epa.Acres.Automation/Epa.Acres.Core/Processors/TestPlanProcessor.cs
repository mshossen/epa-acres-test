﻿using System;
using System.Collections.Generic;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models.TestResultsDtos;

namespace Epa.Acres.Core.Processors
{
    public class TestPlanProcessor : ITestPlanProcessor
    {
        private readonly ISqLiteDataAccess _sqLite;

        public TestPlanProcessor(ISqLiteDataAccess sqLite)
        {
            _sqLite = sqLite;
        }

        private static FileReader Reader => new FileReader {FileExtension = FileExtension.txt};

        public List<TestPlanDto> GetTestPlanByDates(DateTime startDate, DateTime endDate)
        {
            return _sqLite.LoadData<TestPlanDto, dynamic>(
                $"SELECT * FROM TestPlan WHERE ExecutedDate BETWEEN @startdate AND @enddate",
                new {startdate = startDate, enddate = endDate}, _sqLite.GetConnectionString());
        }

        public void SaveTestPlan(TestPlanDto testPlan)
        {
            _sqLite.SaveData(Reader.ReadContentAsString("spInsertTestPlan"), testPlan, _sqLite.GetConnectionString());
        }
    }
}