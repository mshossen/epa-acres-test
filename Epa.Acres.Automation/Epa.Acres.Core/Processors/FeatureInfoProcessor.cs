﻿using System.Linq;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models.TestResultsDtos;

namespace Epa.Acres.Core.Processors
{
    public class FeatureInfoProcessor : IFeatureInfoProcessor
    {
        private readonly ISqLiteDataAccess _sqLite;

        public FeatureInfoProcessor(ISqLiteDataAccess sqLite)
        {
            _sqLite = sqLite;
        }

        private static FileReader Reader => new FileReader {FileExtension = FileExtension.txt};

        public FeatureInfoDto GetFeatureTitle(string featureTitle)
        {
            var data = _sqLite.LoadData<FeatureInfoDto, dynamic>("Select * FROM FeatureInfo WHERE Title = @title",
                new {title = featureTitle}, _sqLite.GetConnectionString()).First();

            return data;
        }

        public void InsertFeatureInfo(FeatureInfoDto featureInfo)
        {
            var data = _sqLite.LoadData<FeatureInfoDto, dynamic>("Select * FROM FeatureInfo WHERE Title = @title",
                new {title = featureInfo.Title}, _sqLite.GetConnectionString());

            if (data?.Count == 0)
                _sqLite.SaveData<FeatureInfoDto>(Reader.ReadContentAsString("spInsertFeatureInfo"), featureInfo,
                    _sqLite.GetConnectionString());
            else if (featureInfo?.Description != string.Empty || featureInfo?.Tags != string.Empty ||
                     featureInfo?.ProgrammingLanguage != string.Empty)
                _sqLite.SaveData<FeatureInfoDto>(Reader.ReadContentAsString("spUpdateFeatureInfo"), featureInfo,
                    _sqLite.GetConnectionString());
        }
    }
}