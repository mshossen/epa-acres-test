﻿using System;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models;

namespace Epa.Acres.Core.Processors
{
    public class PropertyDataProcessor : IPropertyDataProcessor
    {
        private readonly ISqLiteDataAccess _sqLite;

        public PropertyDataProcessor(ISqLiteDataAccess sqLite)
        {
            _sqLite = sqLite; //ContainerConfig.ConfigBuilder().Resolve<ISQLiteDataAccess>();
        }

        public PropertyModel GetRandomProperty(string abbreviation)
        {
            if (abbreviation is null) throw new ArgumentNullException(nameof(abbreviation));

            var properties = _sqLite.LoadData<PropertyModel, dynamic>(
                "Select * From PropertyData Where StateAbbreviation = @stateAbbreviation",
                new {stateAbbreviation = abbreviation}, _sqLite.GetConnectionString());
            var random = new Random().Next(0, properties.Count);

            return properties[random];
        }
    }
}