﻿using System.Linq;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models.TestResultsDtos;

namespace Epa.Acres.Core.Processors
{
    public class ScenarioInfoProcessor : IScenarioInfoProcessor
    {
        private readonly ISqLiteDataAccess _sqLite;

        public ScenarioInfoProcessor(ISqLiteDataAccess sqLite)
        {
            _sqLite = sqLite;
        }

        private static FileReader Reader => new FileReader {FileExtension = FileExtension.txt};

        public void SaveScenarioInfo(ScenarioInfoDto scenarioInfo, string featureTitle)
        {
            scenarioInfo.FeatureId = _sqLite.LoadData<int, dynamic>("SELECT Id FROM FeatureInfo WHERE Title = @title",
                new {title = featureTitle}, _sqLite.GetConnectionString()).FirstOrDefault();

            _sqLite.SaveData(Reader.ReadContentAsString("spInsertScenarioInfo"), scenarioInfo,
                _sqLite.GetConnectionString());
        }
    }
}