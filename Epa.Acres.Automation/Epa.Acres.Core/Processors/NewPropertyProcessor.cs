﻿using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models.TestData;

namespace Epa.Acres.Core.Processors
{
    public class NewPropertyProcessor : INewPropertyProcessor
    {
        private readonly ISqLiteDataAccess _sqLite;

        public NewPropertyProcessor(ISqLiteDataAccess sqLite)
        {
            _sqLite = sqLite;
        }

        private static FileReader Reader => new FileReader {FileExtension = FileExtension.txt};

        public void CreateNewProperty(NewPropertyModel newProperty)
        {
            _sqLite.SaveData<NewPropertyModel>(Reader.ReadContentAsString("spInsertNewProperty"), newProperty,
                _sqLite.GetConnectionString());
        }
    }
}