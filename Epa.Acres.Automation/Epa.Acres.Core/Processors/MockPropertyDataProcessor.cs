﻿using System;
using System.Collections.Generic;
using System.Linq;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Extensions;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Models;
using FizzWare.NBuilder;
using Newtonsoft.Json;

namespace Epa.Acres.Core.Processors
{
    public class MockPropertyDataProcessor
    {
        private readonly string _dataAsString;
        private readonly string _guid = Guid.NewGuid().ToString()[..4];

        public MockPropertyDataProcessor()
        {
            _dataAsString = Reader.ReadMockJsonAsString("MockPropertyData");
        }

        private static FileReader Reader => new FileReader() {FileExtension = FileExtension.json};

        public IList<MockPropertyModel> GetPropertiesByState(string stateAbbreviation)
        {
            return GetProperties().Where(d => d.StateAbbreviation.Equals(stateAbbreviation)).ToList();
        }

        public MockPropertyModel SelectARandomProperty(string stateAbbreviation)
        {
            var property = GetProperties().Where(d => d.StateAbbreviation.Equals(stateAbbreviation)).ToList()
                .SelectARandomElement();

            var transFormProperty = Builder<MockPropertyModel>.CreateNew()
                .With(p => p.Acres = property.Acres)
                .With(p => p.Address = $"{property.Address} {_guid}")
                .With(p => p.City = property.City)
                .With(p => p.Latitude = property.Latitude)
                .With(p => p.Longitude = property.Longitude)
                .With(p => p.ParcelNumbers = property.ParcelNumbers)
                .With(p => p.PropertyName = $"AutoGen {property.PropertyName} {_guid}")
                .With(p => p.State = property.State)
                .With(p => p.StateAbbreviation = property.StateAbbreviation)
                .With(p => p.ZipCode = property.ZipCode)
                .Build();

            return transFormProperty;
        }

        private IEnumerable<MockPropertyModel> GetProperties()
        {
            return JsonConvert.DeserializeObject<List<MockPropertyModel>>(_dataAsString);
        }
    }
}