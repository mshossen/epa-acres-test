﻿using Epa.Acres.Core.Models.BrownfiledStateVRP;
using Faker;
using FizzWare.NBuilder;

namespace Epa.Acres.Core.Processors
{
    public class MockSvrpContactInfoProcess
    {
        public static ContactInfoSvrpModel CreateContactInfo()
        {
            var properties = Builder<ContactInfoSvrpModel>.CreateNew()
                .With(c => c.ContactName = Name.FullName())
                .With(c => c.Organization = Company.Name())
                .With(c => c.Title = "Test Automation Title")
                .With(c => c.Email = Internet.Email(c.ContactName))
                .With(c => c.PhoneNumber = Phone.Number())
                .With(c => c.ProgramWebsite = Internet.Url())
                .Build();

            return properties;
        }
    }
}