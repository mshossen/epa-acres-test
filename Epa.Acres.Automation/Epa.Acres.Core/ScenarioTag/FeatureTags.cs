﻿namespace Epa.Acres.Core.ScenarioTag
{
    public class FeatureTags
    {
        public const string AddCa = "addCA";
        public const string AddProperty = "addProperty";
        public const string Assessment = "assessment";
        public const string Bcrlf = "bcrlf";
        public const string CarUser = "carUser";
        public const string Cleanup = "cleanup";
        public const string DataAdmin = "dataAdmin";
        public const string EpaUser = "epaUser";
        public const string StvrpContaminants = "stvrpContaminants";
    }
}