﻿using Autofac;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Processors;
using Epa.Acres.Core.Services;
using Microsoft.Extensions.Configuration;

namespace Epa.Acres.Core.Config
{
    public static class ContainerConfig
    {
        public static IContainer ConfigBuilder()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<SqLiteDataAccess>().As<ISqLiteDataAccess>();
            builder.RegisterType<FeatureInfoProcessor>().As<IFeatureInfoProcessor>();
            builder.RegisterType<TestPlanProcessor>().As<ITestPlanProcessor>();
            builder.RegisterType<PropertyDataProcessor>().As<IPropertyDataProcessor>();
            builder.RegisterType<ScenarioInfoProcessor>().As<IScenarioInfoProcessor>();
            builder.RegisterType<OracleDataAccess>().As<IOracleDataAccess>();
            builder.RegisterType<NewPropertyProcessor>().As<INewPropertyProcessor>();
            builder.RegisterType<TeamsNotificationService>().As<IReportingService>();

            return builder.Build();
        }

        public static IConfigurationBuilder Configure()
        {
            var builder = new ConfigurationBuilder();
            return builder;
        }
    }
}