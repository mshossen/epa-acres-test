﻿using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models;
using Microsoft.Extensions.Configuration;

namespace Epa.Acres.Core.Services
{
    internal class UserService : IUserService
    {
        public UserService()
        {
            var configBuilder = ContainerConfig.Configure()
                .AddUserSecrets<UserConfig>()
                .Build();
            UserConfig = configBuilder.Get<UserConfig>();
        }

        public UserConfig UserConfig { get; private set; }
    }
}