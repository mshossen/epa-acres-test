﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using Epa.Acres.Core.Interfaces;

namespace Epa.Acres.Core.Services
{
    public class TeamsNotificationService : IReportingService
    {
        private readonly MailMessage _mailMessage;
        private readonly IUserService _service;
        private readonly SmtpClient _smtpClient;

        public TeamsNotificationService(IUserService service)
        {
            _service = service;

            _mailMessage = new MailMessage()
            {
                IsBodyHtml = true
            };

            _smtpClient = new SmtpClient
            {
                Credentials = new NetworkCredential(_service.UserConfig.Outlook.UserName,
                    _service.UserConfig.Outlook.Password),
                Host = _service.UserConfig.Outlook.Host,
                Port = _service.UserConfig.Outlook.Port,
                EnableSsl = true
            };

            _smtpClient.Timeout = 50000;
        }

        public IReportingService AddAttachment(string screenShot)
        {
            _mailMessage.Attachments.Add(new Attachment(screenShot));

            return this;
        }

        public IReportingService AddAttachments(IEnumerable<string> screenShots)
        {
            foreach (var imageFile in screenShots) _mailMessage.Attachments.Add(new Attachment(imageFile));

            return this;
        }

        public void Dispose()
        {
            _mailMessage?.Dispose();
            _smtpClient?.Dispose();
        }

        public IReportingService SendEmailReport()
        {
            _mailMessage.From = new MailAddress(_service.UserConfig.Outlook.UserName);

            foreach (var to in _service.UserConfig.ToEmails) _mailMessage.To.Add(to);
            _smtpClient.Send(_mailMessage);

            return this;
        }

        public IReportingService WithEmailBody(string body = "")
        {
            _mailMessage.Body = body;
            return this;
        }

        public IReportingService WithEmailSubject(string subject = "ACRES EPA Regression Results")
        {
            _mailMessage.Subject = subject;
            return this;
        }
    }
}