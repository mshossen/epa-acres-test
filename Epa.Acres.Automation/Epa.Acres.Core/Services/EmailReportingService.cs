﻿using System.Net;
using System.Net.Mail;
using Epa.Acres.Core.Interfaces;

namespace Epa.Acres.Core.Services
{
    public class EmailReportingService
    {
        private readonly MailAddress _fromAddress;
        private readonly IUserService _userService;

        public EmailReportingService(IUserService userService)
        {
            _userService = userService;
            _fromAddress = new MailAddress(_userService.UserConfig.FromEmail, "Acres Regression");
        }

        public void SendEmailReport(string body = null)
        {
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(_fromAddress.Address, _userService.UserConfig.EmailPassword)
            };

            var message = new MailMessage()
            {
                From = _fromAddress,
                Subject = "Acres6 Regression Result",
                IsBodyHtml = true,
                Body = !string.IsNullOrWhiteSpace(body)
                    ? body
                    : "<h1>Testing Regression Email</h1>"
            };

            message.Attachments.Add(new Attachment(
                @"C:\LinTechProjects\epa-acres-test\Epa.Acres.Automation\Epa.Acres.Ui.Tests\bin\Debug\netcoreapp3.1\LivingDoc.html"));
            foreach (var address in _userService.UserConfig.ToEmails) message.To.Add(new MailAddress(address));

            smtp.Send(message);
            message.Dispose();
            smtp.Dispose();
        }
    }
}