﻿using System;
using System.Text;

namespace Epa.Acres.Core.Services
{
    public class NotificationBuilder
    {
        public static string ReportBuilder(string value)
        {
            var sb = new StringBuilder();
            sb.Append("<html><body>");
            sb.Append(@$"<div>{value}</div>");
            sb.Append("<table>");
            sb.Append("<thead>");
            sb.Append("<th>");
            sb.Append("<td>Browser</td>");
            sb.Append("<td>Executed Date</td>");
            sb.Append("</th>");
            sb.Append("</thead>");
            sb.Append("<tbody>");
            sb.Append("<tr>");
            sb.Append("<td>chrome</td>");
            sb.Append($"<td>{DateTime.Now:dd/mm/yyyy}</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</body></html>");

            return sb.ToString();
        }
    }
}