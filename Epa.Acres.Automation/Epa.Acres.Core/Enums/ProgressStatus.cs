﻿using System.ComponentModel;

namespace Epa.Acres.Core.Enums
{
    public enum ProgressStatus
    {
        [Description("Not Started")] NotStarted = 0,

        [Description("In Progress")] InProgress = 1,

        [Description("Completed")] Completed = 2
    }
}