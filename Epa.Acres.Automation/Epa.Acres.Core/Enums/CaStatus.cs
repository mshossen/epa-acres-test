﻿using System.ComponentModel;

namespace Epa.Acres.Core.Enums
{
    public enum CaStatus
    {
        [Description("All status")] AllStatus,

        [Description("Open")] Open = 1,

        [Description("Cancelled")] Cancelled = 2,

        [Description("Closed")] Closed = 3
    }
}