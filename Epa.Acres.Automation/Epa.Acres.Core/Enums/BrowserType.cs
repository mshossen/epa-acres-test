﻿namespace Epa.Acres.Core.Enums
{
    public enum BrowserType
    {
        Chrome,
        Firefox,
        Edge
    }
}