﻿namespace Epa.Acres.Core.Enums
{
    public enum GtStatusCode
    {
        NOTSTARTED,
        INPROGRESS,
        COMPLETED
    }
}