﻿namespace Epa.Acres.Core.Enums
{
    public enum QrStatusCode
    {
        RETFORCLARIFY,
        READYFORREVIEW,
        CAREDIT,
        ACCEPTED
    }
}