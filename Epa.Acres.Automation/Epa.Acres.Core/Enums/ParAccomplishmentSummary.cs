﻿using System.ComponentModel;

namespace Epa.Acres.Core.Enums
{
    public enum ParAccomplishmentSummary
    {
        [Description("Region")] Region = 1,

        [Description("Number of Assessments Completed")]
        NumberOfAssessmentsCompleted = 2,

        [Description("Number of Cleanups Completed")]
        NumberOfCleanupsCompleted = 3,

        [Description("Dollars Leveraged")] DollarsLeveraged = 4,

        [Description("Jobs Leveraged")] JobsLeveraged = 5,

        [Description("Acres/Properties Ready for Anticipated Use")]
        AcresPropertiesReadyForAnticipatedUse = 6
    }
}