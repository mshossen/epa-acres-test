﻿namespace Epa.Acres.Core.Enums
{
    public enum FileExtension
    {
        txt,
        csv,
        json,
        pdf,
        html,
        png
    }
}