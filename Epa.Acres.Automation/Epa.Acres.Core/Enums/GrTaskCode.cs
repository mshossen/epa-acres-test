﻿namespace Epa.Acres.Core.Enums
{
    public enum GrTaskCode
    {
        COMINVOLVE,
        OVERSIGHT,
        CUPLAN,
        HEALTH,
        SITECUACTS,
        SITEASSESS
    }
}