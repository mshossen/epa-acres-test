﻿using Epa.Acres.Core.Models.TestResultsDtos;

namespace Epa.Acres.Core.Interfaces
{
    public interface IScenarioInfoProcessor
    {
        void SaveScenarioInfo(ScenarioInfoDto scenarioInfo, string featureTitle);
    }
}