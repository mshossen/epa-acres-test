﻿using Epa.Acres.Core.Models.TestResultsDtos;

namespace Epa.Acres.Core.Interfaces
{
    public interface IFeatureInfoProcessor
    {
        FeatureInfoDto GetFeatureTitle(string featureTitle);

        void InsertFeatureInfo(FeatureInfoDto featureInfo);
    }
}