﻿using System.Collections.Generic;

namespace Epa.Acres.Core.Interfaces
{
    public interface ISqLiteDataAccess
    {
        string SetSqLiteDbName { get; set; }

        string GetConnectionString(string name = null);

        List<T> LoadData<T, TU>(string storedProcedure, TU parameters, string connectionStringName);

        List<T> LoadData<T>(string sqlCommand, string connectionString);

        void SaveData<T>(string storedProcedure, T parameters, string connectionStringName);
    }
}