﻿using System.Collections.Generic;

namespace Epa.Acres.Core.Interfaces
{
    public interface ISqlDataAccess
    {
        string GetConnectionString(string name);

        List<T> LoadData<T, TU>(string storedProcedure, TU parameters, string connectionStringName);

        void SaveData<T>(string storedProcedure, T parameters, string connectionStringName);
    }
}