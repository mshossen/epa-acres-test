﻿using System;
using System.Collections.Generic;
using Epa.Acres.Core.Models.TestResultsDtos;

namespace Epa.Acres.Core.Interfaces
{
    public interface ITestPlanProcessor
    {
        List<TestPlanDto> GetTestPlanByDates(DateTime startDate, DateTime endDate);

        void SaveTestPlan(TestPlanDto testPlan);
    }
}