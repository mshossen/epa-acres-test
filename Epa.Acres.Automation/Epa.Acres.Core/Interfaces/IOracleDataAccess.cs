﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Epa.Acres.Core.Interfaces
{
    public interface IOracleDataAccess
    {
        void CloseDbConnection();

        Task<int> GetTableRowCount(string tableName);

        List<T> LoadData<T>(string sqlQuery);

        List<T> LoadData<T, TU>(string sqlQuery, TU parameter);

        Task<List<T>> LoadDataAsync<T, TU>(string sqlQuery, TU parameter);

        Task<List<T>> LoadDataAsync<T>(string sqlQuery);

        List<T> LoadDataRows<T, TU>(string storedProcedure, TU parameters);
    }
}