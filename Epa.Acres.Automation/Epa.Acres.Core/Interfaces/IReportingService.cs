﻿using System.Collections.Generic;

namespace Epa.Acres.Core.Interfaces
{
    public interface IReportingService
    {
        IReportingService AddAttachment(string screenShot);

        IReportingService AddAttachments(IEnumerable<string> screenShots);

        void Dispose();

        IReportingService SendEmailReport();

        IReportingService WithEmailBody(string body = "");

        IReportingService WithEmailSubject(string subject = "ACRES EPA Regression Results");
    }
}