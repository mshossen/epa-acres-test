﻿using Epa.Acres.Core.Models;

namespace Epa.Acres.Core.Interfaces
{
    public interface IUserService
    {
        UserConfig UserConfig { get; }
    }
}