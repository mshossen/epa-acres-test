﻿using Epa.Acres.Core.Models;

namespace Epa.Acres.Core.Interfaces
{
    public interface IPropertyDataProcessor
    {
        PropertyModel GetRandomProperty(string abbreviation);
    }
}