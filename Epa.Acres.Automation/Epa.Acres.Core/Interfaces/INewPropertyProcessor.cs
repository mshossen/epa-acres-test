﻿using Epa.Acres.Core.Models.TestData;

namespace Epa.Acres.Core.Interfaces
{
    public interface INewPropertyProcessor
    {
        void CreateNewProperty(NewPropertyModel newProperty);
    }
}