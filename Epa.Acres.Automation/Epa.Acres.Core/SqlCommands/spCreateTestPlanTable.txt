﻿CREATE TABLE TestPlan (
	                                              Id	INTEGER NOT NULL UNIQUE,
	                                              TestPlanId	NVARCHAR(32) NOT NULL UNIQUE,
	                                              ExecutedDate	DATETIME2 NOT NULL DEFAULT current_timestamp,
	                                              MachineName	NVARCHAR(50) NOT NULL,
	                                              UserName	NVARCHAR(50) NOT NULL,
	                                              BuildNumber	NVARCHAR(50) NOT NULL,
	                                              AcceptInsecureCerts	BIT,
	                                              BrowserName	NVARCHAR(10),
	                                              BrowserVersion	NVARCHAR(25),
	                                              PageLoadStrategy	NVARCHAR(50),
	                                              PlatformName	NVARCHAR(50),
	                                              PlatformVersion	NVARCHAR(100),
	                                              PRIMARY KEY(Id AUTOINCREMENT)
                                               );