﻿UPDATE FeatureInfo
Set Description = @Description, Tags = @Tags, ProgrammingLanguage = @ProgrammingLanguage
Where Title = @Title