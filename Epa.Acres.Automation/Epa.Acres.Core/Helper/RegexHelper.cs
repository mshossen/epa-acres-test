﻿using System.Text.RegularExpressions;

namespace Epa.Acres.Core.Helper
{
    public class RegexHelper
    {
        private readonly string _pattern;
        private readonly string _text;

        public RegexHelper(string pattern, string text)
        {
            _pattern = pattern;
            _text = text;
        }

        public string GetMatchedPatter(int matchGroup = 1)
        {
            const RegexOptions options = RegexOptions.IgnoreCase;
            var match = new Regex(_pattern, options).Match(_text);

            return !match.Success
                ? string.Empty
                : match.Groups[matchGroup].Value;
        }
    }
}