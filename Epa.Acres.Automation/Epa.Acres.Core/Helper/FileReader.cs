﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Epa.Acres.Core.Enums;

namespace Epa.Acres.Core.Helper
{
    public class FileReader
    {
        private readonly string _currentDirectory;

        public FileReader()
        {
            _currentDirectory = typeof(FileReader).Assembly.Location;
        }

        public FileExtension FileExtension { get; set; }

        private IEnumerable<string> ListOfJsonFiles
        {
            get
            {
                return Directory.GetFiles(Directory.GetParent(_currentDirectory).FullName + @"\DataSource")
                    .Where(f => f.EndsWith(".json")).ToList();
            }
        }

        private IEnumerable<string> ListOfTextFiles
        {
            get
            {
                return Directory.GetFiles(Directory.GetParent(_currentDirectory).FullName + @"\SqlCommands")
                    .Where(f => f.EndsWith(".txt")).ToList();
            }
        }

        public string ReadContentAsString(string fileName)
        {
            var file = ListOfTextFiles.FirstOrDefault(n => n.EndsWith($"{fileName}.{FileExtension}"));

            return File.Exists(file)
                ? File.ReadAllText(file)
                : string.Empty;
        }

        public string ReadMockJsonAsString(string jsonFileName)
        {
            var file = ListOfJsonFiles.FirstOrDefault(n => n.EndsWith($"{jsonFileName}.{FileExtension}"));

            return File.Exists(file)
                ? File.ReadAllText(file)
                : string.Empty;
        }
    }
}