﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Epa.Acres.Core.Helper
{
    public class ImageConverter
    {
        public void SaveImageFromString(string base64String)
        {
            var bytes = Convert.FromBase64String(base64String);
            using var memory = new MemoryStream(bytes);
            var image = Image.FromStream(memory);

            image.Save($@"C:\LinTechProjects\ConvertedFrom{DateTime.Now:yyyyMMddHHmmsss}.png", ImageFormat.Png);
        }
    }
}