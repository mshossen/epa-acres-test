﻿using System;

namespace Epa.Acres.Core.Helper
{
    public class TimestampToDateHelper
    {
        private readonly DateTime _dateTime;
        private readonly TimeSpan _diff;

        public TimestampToDateHelper(long timeStamp)
        {
            _dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(timeStamp / 1000d));
        }

        public TimestampToDateHelper(string dateString)
        {
            DateTime.TryParse(dateString, out var date);
            _dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            _diff = date - _dateTime;
        }

        public DateTime Date => _dateTime;

        public int Day => _dateTime.Day;

        public double? LongEpoch => Math.Floor(_diff.TotalSeconds) * 1000;

        public int Month => _dateTime.Month;

        public int Year => _dateTime.Year;
    }
}