﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using Dapper;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Interfaces;

namespace Epa.Acres.Core.Helper
{
    public class SqLiteDataAccess : ISqLiteDataAccess
    {
        private const string SqlDir = @"c:\sqlite\db";
        private string _sqLite;

        public SqLiteDataAccess()
        {
            SetDbName();
            CreateSqLiteTable("ScenarioInfo", ScenarioInfo);
            CreateSqLiteTable("TestPlan", TestPlan);
            // CreateSqliteTable("TestCase", TestCase);
            CreateSqLiteTable("FeatureInfo", FeatureInfo);
            CreateSqLiteTable("NewProperty", NewProperty);
        }

        public string SetSqLiteDbName { get; set; } = "AcresReporting.db";
        private static string FeatureInfo => Reader.ReadContentAsString("spCreateFeatureInfoTable");
        private static string NewProperty => Reader.ReadContentAsString("spCreateNewPropertyTable");
        private static FileReader Reader => new FileReader() {FileExtension = FileExtension.txt};
        private static string ScenarioInfo => Reader.ReadContentAsString("spCreateScenarioInfo");
        private static string TestPlan => Reader.ReadContentAsString("spCreateTestPlanTable");

        public string GetConnectionString(string name = null)
        {
            if (name is { }) return name;

            var builder = new SQLiteConnectionStringBuilder
            {
                DataSource = _sqLite,
                Version = 3
            };

            return builder.ToString();
        }

        public List<T> LoadData<T, TU>(string storedProcedure, TU parameters, string connectionStringName)
        {
            var connectionString = GetConnectionString(connectionStringName);
            using IDbConnection connection = new SQLiteConnection(connectionString);
            var rows = connection.Query<T>(storedProcedure, parameters, commandType: CommandType.Text).ToList();

            return rows;
        }

        public List<T> LoadData<T>(string sqlCommand, string connectionString)
        {
            var connection = GetConnectionString(connectionString);
            using IDbConnection dbConnection = new SQLiteConnection(connection);
            var rows = dbConnection.Query<T>(sqlCommand, commandType: CommandType.Text).ToList();

            return rows;
        }

        public void SaveData<T>(string storedProcedure, T parameters, string connectionStringName)
        {
            var connectionString = GetConnectionString(connectionStringName);
            IDbConnection connection = new SQLiteConnection(connectionString);
            connection?.Open();
            connection.Execute(storedProcedure, parameters);
            connection?.Dispose();
        }

        private void CreateSqLiteTable(string tableName, string script)
        {
            using var connection = new SQLiteConnection(GetConnectionString());
            connection.Open();
            var schema = $@"SELECT name FROM sqlite_master WHERE type='table' AND name='{tableName}';";
            using var command = new SQLiteCommand(schema, connection);
            if (command.ExecuteScalar() != null) return;
            using var cmd = new SQLiteCommand(script, connection);
            cmd.ExecuteNonQuery();
        }

        private void SetDbName()
        {
            _sqLite = SqlDir + @"\" + SetSqLiteDbName;

            if (File.Exists(_sqLite)) return;
            Directory.CreateDirectory(SqlDir);
            SQLiteConnection.CreateFile(_sqLite);
        }
    }
}