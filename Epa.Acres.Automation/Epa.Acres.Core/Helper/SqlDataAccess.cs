﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Epa.Acres.Core.Helper
{
    public class SqlDataAccess
    {
        public string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public List<T> LoadData<T, TU>(string storedProcedure, TU parameters, string connectionStringName)
        {
            var connectionString = GetConnectionString(connectionStringName);
            using IDbConnection connection = new SqlConnection(connectionString);

            var rows = connection.Query<T>(storedProcedure, parameters, commandType: CommandType.StoredProcedure)
                .ToList();

            return rows;
        }

        public void SaveData<T>(string storedProcedure, T parameters, string connectionStringName)
        {
            var connectionString = GetConnectionString(connectionStringName);
            using IDbConnection connection = new SqlConnection(connectionString);
            connection.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}