﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Epa.Acres.Core.Interfaces;
using Oracle.ManagedDataAccess.Client;

namespace Epa.Acres.Core.Helper
{
    public class OracleDataAccess : IOracleDataAccess
    {
        private readonly IUserService _userService;
        private readonly IDbConnection _dbConn;

        public OracleDataAccess(IUserService userService)
        {
            _userService = userService;
            _dbConn = new OracleConnection(ConnectionString);
            _dbConn.Open();
        }

        private string ConnectionString =>
            $"DATA SOURCE={_userService.UserConfig.OracleHost}:{_userService.UserConfig.OraclePort}/{_userService.UserConfig.OracleDb};USER ID={_userService.UserConfig.OracleUser};Password={_userService.UserConfig.OraclePassword}";

        public void CloseDbConnection()
        {
            _dbConn?.Close();
        }

        public async Task<int> GetTableRowCount(string tableName)
        {
            var dataResult = await _dbConn.ExecuteScalarAsync<int>($"SELECT count(1) FROM {tableName}");
            return dataResult;
        }

        public List<T> LoadData<T>(string sqlQuery)
        {
            // using var dbConn = new OracleConnection(ConnectionString); dbConn.Open();
            return _dbConn.Query<T>(sqlQuery).ToList();
        }

        public List<T> LoadData<T, TU>(string sqlQuery, TU parameters)
        {
            // using IDbConnection dbConn = new OracleConnection(ConnectionString); dbConn.Open();
            return _dbConn.Query<T>(sqlQuery, parameters, commandType: CommandType.Text).ToList();
        }

        public async Task<List<T>> LoadDataAsync<T>(string sqlQuery)
        {
            // using var dbConn = new OracleConnection(ConnectionString); dbConn.Open();
            var data = await _dbConn.QueryAsync<T>(sqlQuery);

            return data.ToList();
        }

        public async Task<List<T>> LoadDataAsync<T, TU>(string sqlQuery, TU parameter)
        {
            // using IDbConnection dbConn = new OracleConnection(ConnectionString); dbConn.Open();
            var dataResult = await _dbConn.QueryAsync<T>(sqlQuery, parameter, commandType: CommandType.Text);

            return dataResult.ToList();
        }

        public List<T> LoadDataRows<T, TU>(string storedProcedure, TU parameters)
        {
            // using IDbConnection connection = new OracleConnection(ConnectionString);
            //connection.Open();
            var rows = _dbConn.Query<T>(storedProcedure, parameters, commandType: CommandType.Text).ToList();

            return rows;
        }
    }
}