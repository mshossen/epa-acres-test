﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Epa.Acres.Core.Models
{
    public class UserConfig
    {
        [JsonProperty("acresUrl")] public string AcresUrl { get; set; }
        [JsonProperty("adminuser")] public string AdminUser { get; set; }
        [JsonProperty("carusers")] public List<User> CarUsers { get; set; }
        [JsonProperty("emailPassword")] public string EmailPassword { get; set; }
        [JsonProperty("epausers")] public List<User> EpaUsers { get; set; }
        [JsonProperty("fromEmail")] public string FromEmail { get; set; }
        [JsonProperty("MockApi")] public MockApi MockApi { get; set; }
        public string OracleDb { get; set; }
        public string OracleHost { get; set; }
        public string OraclePassword { get; set; }
        public int OraclePort { get; set; }
        public string OracleUser { get; set; }
        public Outlook Outlook { get; set; }
        [JsonProperty("password")] public string Password { get; set; }
        [JsonProperty("stvrpuser")] public string StvrpUser { get; set; }
        [JsonProperty("toEmails")] public List<string> ToEmails { get; set; }
    }
}