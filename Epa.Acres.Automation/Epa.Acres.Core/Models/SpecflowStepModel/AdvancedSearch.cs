﻿using Epa.Acres.Core.Enums;

namespace Epa.Acres.Core.Models.SpecflowStepModel
{
    public class AdvancedSearch
    {
        public string Filter { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public CaStatus Status { get; set; }
    }
}