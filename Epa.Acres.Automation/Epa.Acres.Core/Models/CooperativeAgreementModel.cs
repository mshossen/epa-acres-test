﻿namespace Epa.Acres.Core.Models
{
    public class CooperativeAgreementModel
    {
        public string AnnouncementYear { get; set; }
        public string CaName { get; set; }
        public string CaNumber { get; set; }
        public string CaStatus { get; set; }
        public string CaType { get; set; }
        public string State { get; set; }
    }
}