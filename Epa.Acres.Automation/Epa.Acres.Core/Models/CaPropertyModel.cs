﻿using System.Collections.Generic;

namespace Epa.Acres.Core.Models
{
    public class CaPropertyModel
    {
        public string AnnouncementYear { get; set; }
        public List<string> AssociatedProperties { get; set; }
        public double? AwardAmount { get; set; }
        public string CaName { get; set; }
        public string CaNumber { get; set; }
        public string CaStatus { get; set; }
        public string CaType { get; set; }
        public string FundingType { get; set; }
        public int GrantId { get; set; }
        public string State { get; set; }
    }
}