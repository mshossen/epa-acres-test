﻿namespace Epa.Acres.Core.Models
{
    public class SourceFundingModel
    {
        public string ActivityFunded { get; set; }
        public double AmmountFunded { get; set; }
        public string ProvidingEntity { get; set; }
        public string SourceFunding { get; set; }
    }
}