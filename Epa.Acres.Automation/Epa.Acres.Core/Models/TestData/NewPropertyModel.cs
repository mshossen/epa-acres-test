﻿using System;

namespace Epa.Acres.Core.Models.TestData
{
    /// <summary>
    ///   Sqlite Data model for New Property tabel
    /// </summary>
    public class NewPropertyModel
    {
        public decimal CaAwardedAmount { get; set; }
        public string CaName { get; set; }
        public string CaNumber { get; set; }
        public string CaState { get; set; }
        public string CaStateAbbreviation { get; set; }
        public string CaStatus { get; set; }
        public string CaType { get; set; }
        public string CaYear { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsApprovedWorkPackage { get; set; }
        public bool IsNewProperty { get; set; }
        public string PropertyId { get; set; }
        public string PropertyJson { get; set; }

        /// <summary>
        ///   SessionId is equal to TestPlanId
        /// </summary>
        public string SessionId { get; set; }
    }
}