﻿using System;
using Newtonsoft.Json;

namespace Epa.Acres.Core.Models.TestResultsDtos
{
    public class TestPlanDto
    {
        [JsonProperty("acceptInsecureCerts")] public bool AcceptInsecureCerts { get; set; }
        [JsonProperty("browserName")] public string BrowserName { get; set; }
        [JsonProperty("browserVersion")] public string BrowserVersion { get; set; }
        public string BuildNumber { get; set; }
        public DateTime ExecutedDate { get; set; } = DateTime.Now;
        public string MachineName { get; set; }
        [JsonProperty("pageLoadStrategy")] public string PageLoadStrategy { get; set; }
        [JsonProperty("platformName")] public string PlatformName { get; set; }
        public string PlatformVersion { get; set; }
        public string TestPlanId { get; set; }
        public string UserName { get; set; }
    }
}