﻿namespace Epa.Acres.Core.Models.TestResultsDtos
{
    /// <summary>
    ///   Sqlite feature info
    /// </summary>
    public class FeatureInfoDto
    {
        public string Description { get; set; }
        public int Id { get; }
        public string ProgrammingLanguage { get; set; }
        public string Tags { get; set; }
        public string Title { get; set; }
    }
}