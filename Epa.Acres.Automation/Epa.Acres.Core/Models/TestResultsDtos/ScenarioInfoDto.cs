﻿using System;

namespace Epa.Acres.Core.Models.TestResultsDtos
{
    public class ScenarioInfoDto
    {
        public string ErrorMessage { get; set; }
        public DateTime ExecutedDate { get; set; }
        public int FeatureId { get; set; }
        public string ScenarioTitle { get; set; }
        public string ScreenShot { get; set; }
        public string Status { get; set; }
        public int StepCompleationTime { get; set; }
        public string StepDefinition { get; set; }
        public string StepDefinitionType { get; set; }
        public string TestPlanId { get; set; }
    }
}