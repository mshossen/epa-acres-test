﻿namespace Epa.Acres.Core.Models
{
    /// <summary>
    ///   Sqlite data model for test data Property tabel
    /// </summary>
    public class PropertyModel
    {
        public double Acres { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PropertyName { get; set; }
        public int Region { get; set; }
        public string State { get; set; }
        public string StateAbbreviation { get; set; }
        public string ZipCode { get; set; }
    }
}