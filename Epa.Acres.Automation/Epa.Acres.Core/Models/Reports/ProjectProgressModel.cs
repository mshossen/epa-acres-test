﻿namespace Epa.Acres.Core.Models.Reports
{
    public class ProjectProgressModel
    {
        public int Id { get; set; }
        public string QuarterProgressSummary { get; set; }
        public string Status { get; set; }
        public string Tasks { get; set; }
    }
}