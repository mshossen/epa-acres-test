﻿using System;

namespace Epa.Acres.Core.Models.Reports
{
    public class QrFundsExpendedModel
    {
        public double CostIncurredCurrentQuarter { get; set; }
        public double CostIncurredToDate => CostIncurredCurrentQuarter;
        public double CurrentApprovedBudget { get; set; }
        public double InitialApprovedBudget { get; set; }
        public double TotalRemaining => Math.Round(CurrentApprovedBudget - CostIncurredCurrentQuarter, 2);
    }
}