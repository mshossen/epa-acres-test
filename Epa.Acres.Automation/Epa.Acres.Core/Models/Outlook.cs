﻿namespace Epa.Acres.Core.Models
{
    public class Outlook
    {
        public string Host { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
    }
}