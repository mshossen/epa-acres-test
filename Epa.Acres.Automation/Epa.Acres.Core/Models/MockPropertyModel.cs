﻿namespace Epa.Acres.Core.Models
{
    /// <summary>
    ///   Json Mock Property test data
    /// </summary>
    public class MockPropertyModel
    {
        public double Acres { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string ParcelNumbers { get; set; }
        public string PropertyName { get; set; }
        public string State { get; set; }
        public string StateAbbreviation { get; set; }
        public string ZipCode { get; set; }
    }
}