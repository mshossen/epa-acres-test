﻿using Newtonsoft.Json;

namespace Epa.Acres.Core.Models
{
    public class User
    {
        [JsonProperty("region")] public int Region { get; set; }

        [JsonProperty("username")] public string UserName { get; set; }
    }
}