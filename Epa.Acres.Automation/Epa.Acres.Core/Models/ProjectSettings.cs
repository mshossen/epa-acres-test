﻿using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Epa.Acres.Core.Models
{
    public static class ProjectSettings
    {
        public const string ParallelScreenShotDir = @"C:\LinTechProjects\Parallel";
        public const string ScreenShotDirectory = @"C:\LinTechProjects\SeleniumReport";

        public static void AutoCreateDirectories()
        {
            var fields = typeof(ProjectSettings).GetFields();

            foreach (var field in fields)
            {
                // var projectSettings = typeof(ProjectSettings);
                // var propertyValue = (string) projectSettings.GetProperty(property.Name)
                //     ?.GetValue(projectSettings, null);

                var fieldValue = (string) typeof(ProjectSettings).GetField(field.Name)
                    ?.GetValue(null);
                
                if (!Directory.Exists(fieldValue)) Directory.CreateDirectory(fieldValue);
            }
        }
    }
}