﻿namespace Epa.Acres.Core.Models.BrownfiledStateVRP
{
    public class ContactInfoSvrpModel
    {
        public string ContactName { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public string PhoneNumber { get; set; }
        public string ProgramWebsite { get; set; }
        public string Title { get; set; }
    }
}