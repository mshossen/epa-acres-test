﻿using Newtonsoft.Json;

namespace Epa.Acres.Core.Models
{
    public class MockApi
    {
        [JsonProperty("MockPropertyApi")] public string MockPropertyApi { get; set; }
    }
}