﻿using System;
using System.Collections.Generic;
using System.Linq;
using Epa.Acres.Core.Models;
using Faker;
using FizzWare.NBuilder;

namespace Epa.Acres.Core.DataSource
{
    public class FakerPropertyInformationData
    {
        private const double MaximumAcres = 500.99;
        private const double MinimumAcres = 0.01;

        public static IList<MockPropertyModel> CreateProperties()
        {
            var properties = Builder<MockPropertyModel>.CreateListOfSize(100)
                .All()
                .With(p => p.PropertyName = $"AutoGen {Address.StreetName()} {DateTime.Now:MMddyyyyHHmmsss}")
                .With(p => p.Address = Address.StreetAddress())
                .With(p => p.ZipCode = Address.ZipCode().Substring(0, 5))
                .With(p => p.City = Address.City())
                .With(p => p.StateAbbreviation = Address.UsStateAbbr())
                .With(p => p.State = Address.UsState())
                .With(p => p.Acres =
                    Math.Round(new Random().NextDouble() * (MaximumAcres - MinimumAcres) + MinimumAcres, 2))
                .Build();

            return properties;
        }

        public static MockPropertyModel CreateProperty(string stateAbbreviation)
        {
            return TryMatchState(stateAbbreviation).FirstOrDefault();
        }

        private static IEnumerable<MockPropertyModel> TryMatchState(string stateAbbreviation)
        {
            IList<MockPropertyModel> addresses;

            do
            {
                addresses = CreateProperties().Where(p => p.StateAbbreviation.Equals(stateAbbreviation)).ToList();
            } while (!addresses.Any() && !addresses.Any(p => p.StateAbbreviation.Equals(stateAbbreviation)));

            return addresses;
        }
    }
}