﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class RefState
    {
        [JsonProperty("fipsSt")] public int FipsSt { get; set; }
        [JsonProperty("labelState")] public string LabelState { get; set; }
        [JsonProperty("labelStateLong")] public string LabelStateLong { get; set; }
        [JsonProperty("numberOfDistricts")] public int NumberOfDistricts { get; set; }
        [JsonProperty("regionId")] public int RegionId { get; set; }
        [JsonProperty("stateId")] public int StateId { get; set; }
    }
}