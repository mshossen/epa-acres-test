﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class Grant
    {
        [JsonProperty("addDate")] public long AddDate { get; set; }
        [JsonProperty("allOtherExpended")] public object AllOtherExpended { get; set; }
        [JsonProperty("allOtherObligated")] public object AllOtherObligated { get; set; }
        [JsonProperty("allReportsReceived")] public object AllReportsReceived { get; set; }
        [JsonProperty("announceDate")] public long AnnounceDate { get; set; }
        [JsonProperty("areaPopulation")] public object AreaPopulation { get; set; }

        [JsonProperty("awardAmountCumulative")]
        public object AwardAmountCumulative { get; set; }

        [JsonProperty("awardClassification")] public AwardClassification AwardClassification { get; set; }
        [JsonProperty("awardDate")] public long AwardDate { get; set; }
        [JsonProperty("beInsuranceExpended")] public object BeInsuranceExpended { get; set; }
        [JsonProperty("beInsuranceObligated")] public object BeInsuranceObligated { get; set; }
        [JsonProperty("bhHealthExpended")] public object BhHealthExpended { get; set; }
        [JsonProperty("bhHealthObligated")] public object BhHealthObligated { get; set; }

        [JsonProperty("bhInstControlsExpended")]
        public object BhInstControlsExpended { get; set; }

        [JsonProperty("bhInstControlsObligated")]
        public object BhInstControlsObligated { get; set; }

        [JsonProperty("bhLocalGovExpended")] public object BhLocalGovExpended { get; set; }
        [JsonProperty("bhLocalGovObligated")] public object BhLocalGovObligated { get; set; }

        [JsonProperty("bsRlfSubgrantsExpended")]
        public object BsRlfSubgrantsExpended { get; set; }

        [JsonProperty("bsRlfSubgrantsObligated")]
        public object BsRlfSubgrantsObligated { get; set; }

        [JsonProperty("closeoutDate")] public object CloseoutDate { get; set; }
        [JsonProperty("closeoutExplainOther")] public object CloseoutExplainOther { get; set; }

        [JsonProperty("closeoutReportReceived")]
        public object CloseoutReportReceived { get; set; }

        [JsonProperty("closeoutReportReceivedDate")]
        public object CloseoutReportReceivedDate { get; set; }

        [JsonProperty("closingProjectOfficerId")]
        public object ClosingProjectOfficerId { get; set; }

        [JsonProperty("coopAgreementNumber")] public string CoopAgreementNumber { get; set; }

        [JsonProperty("costShareHardshipWaiver")]
        public object CostShareHardshipWaiver { get; set; }

        [JsonProperty("currentProjectPeriodEnd")]
        public object CurrentProjectPeriodEnd { get; set; }

        [JsonProperty("dcn")] public object Dcn { get; set; }
        [JsonProperty("dcnSupplemental")] public object DcnSupplemental { get; set; }
        [JsonProperty("duns")] public object Duns { get; set; }
        [JsonProperty("flagCloseoutYn")] public object FlagCloseoutYn { get; set; }
        [JsonProperty("flagEpaUpdate")] public object FlagEpaUpdate { get; set; }
        [JsonProperty("flagPpg")] public object FlagPpg { get; set; }
        [JsonProperty("flagPrepopAuth")] public object FlagPrepopAuth { get; set; }

        [JsonProperty("fundLimitWaiverAmount")]
        public object FundLimitWaiverAmount { get; set; }

        [JsonProperty("fundLimitWaiverApproved")]
        public object FundLimitWaiverApproved { get; set; }

        [JsonProperty("grantId")] public int GrantId { get; set; }

        [JsonProperty("grantProjectName")] public string GrantProjectName { get; set; }

        [JsonProperty("inventoryCompletionDate")]
        public object InventoryCompletionDate { get; set; }

        [JsonProperty("inventoryPlanned")] public object InventoryPlanned { get; set; }
        [JsonProperty("inventoryStartDate")] public object InventoryStartDate { get; set; }
        [JsonProperty("largerCleanup")] public object LargerCleanup { get; set; }
        [JsonProperty("lessonsLearned")] public object LessonsLearned { get; set; }
        [JsonProperty("missingReports")] public object MissingReports { get; set; }
        [JsonProperty("modDate")] public long ModDate { get; set; }
        [JsonProperty("modUser")] public string ModUser { get; set; }

        [JsonProperty("otherEntityDescription")]
        public object OtherEntityDescription { get; set; }

        [JsonProperty("otherGrantType")] public object OtherGrantType { get; set; }
        [JsonProperty("pareaMinority")] public object PareaMinority { get; set; }
        [JsonProperty("pareaPoverty")] public object PareaPoverty { get; set; }
        [JsonProperty("pareaUnemployment")] public object PareaUnemployment { get; set; }
        [JsonProperty("pcaRecievedHq")] public object PcaRecievedHq { get; set; }

        [JsonProperty("plegislativeAuthority")]
        public object PlegislativeAuthority { get; set; }

        [JsonProperty("precipientAddress1")] public object PrecipientAddress1 { get; set; }
        [JsonProperty("precipientAddress2")] public object PrecipientAddress2 { get; set; }
        [JsonProperty("precipientCity")] public object PrecipientCity { get; set; }
        [JsonProperty("precipientCounty")] public object PrecipientCounty { get; set; }

        [JsonProperty("precipientOrganization")]
        public object PrecipientOrganization { get; set; }

        [JsonProperty("precipientZipCode")] public object PrecipientZipCode { get; set; }
        [JsonProperty("prepopAuthDate")] public object PrepopAuthDate { get; set; }
        [JsonProperty("projectPeriodEnd")] public long ProjectPeriodEnd { get; set; }
        [JsonProperty("projectPeriodStart")] public long ProjectPeriodStart { get; set; }
        [JsonProperty("ptargetMinority")] public object PtargetMinority { get; set; }
        [JsonProperty("ptargetPoverty")] public object PtargetPoverty { get; set; }
        [JsonProperty("ptargetUnemployment")] public object PtargetUnemployment { get; set; }
        [JsonProperty("purl")] public object Purl { get; set; }
        [JsonProperty("pworkplanDate")] public object PworkplanDate { get; set; }
        [JsonProperty("pworkplanRecieved")] public object PworkplanRecieved { get; set; }
        [JsonProperty("recipientName")] public string RecipientName { get; set; }
        [JsonProperty("refAward")] public RefAward RefAward { get; set; }
        [JsonProperty("refCaCloseoutExplain")] public object RefCaCloseoutExplain { get; set; }
        [JsonProperty("refEntityType")] public RefEntityType RefEntityType { get; set; }
        [JsonProperty("refGrantStatusType")] public RefGrantStatusType RefGrantStatusType { get; set; }
        [JsonProperty("refGrantType")] public RefGrantType RefGrantType { get; set; }
        [JsonProperty("refProgram")] public RefProgram RefProgram { get; set; }
        [JsonProperty("refState")] public RefState RefState { get; set; }
        [JsonProperty("rlfCoalitionPartners")] public object RlfCoalitionPartners { get; set; }
        [JsonProperty("showcaseIndicator")] public object ShowcaseIndicator { get; set; }
        [JsonProperty("targetPopulation")] public object TargetPopulation { get; set; }
    }
}