﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class RefAward
    {
        [JsonProperty("awardId")] public int AwardId { get; set; }

        [JsonProperty("labelAward")] public string LabelAward { get; set; }
    }
}