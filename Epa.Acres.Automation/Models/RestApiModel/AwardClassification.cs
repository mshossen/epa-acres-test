﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class AwardClassification
    {
        [JsonProperty("awardClassCode")] public string AwardClassCode { get; set; }

        [JsonProperty("awardClassDescription")]
        public string AwardClassDescription { get; set; }

        [JsonProperty("awardClassName")] public string AwardClassName { get; set; }
    }
}