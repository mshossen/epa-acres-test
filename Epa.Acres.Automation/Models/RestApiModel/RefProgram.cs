﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class RefProgram
    {
        [JsonProperty("programCode")] public string ProgramCode { get; set; }

        [JsonProperty("programName")] public string ProgramName { get; set; }
    }
}