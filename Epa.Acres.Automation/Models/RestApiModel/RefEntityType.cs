﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class RefEntityType
    {
        [JsonProperty("entityTypeId")] public int EntityTypeId { get; set; }

        [JsonProperty("flagNonGovernmental")] public string FlagNonGovernmental { get; set; }
        [JsonProperty("labelEntityType")] public string LabelEntityType { get; set; }
    }
}