﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class RefGrantStatusType
    {
        [JsonProperty("grantStatusTypeId")] public int GrantStatusTypeId { get; set; }

        [JsonProperty("labelGrantStatusType")] public string LabelGrantStatusType { get; set; }
    }
}