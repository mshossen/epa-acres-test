﻿using Newtonsoft.Json;

namespace Models.RestApiModel
{
    public class RefGrantType
    {
        [JsonProperty("grantTypeId")] public int GrantTypeId { get; set; }

        [JsonProperty("labelGfsDisplay")] public string LabelGfsDisplay { get; set; }
        [JsonProperty("labelGrantType")] public string LabelGrantType { get; set; }
    }
}