﻿using System.ComponentModel.DataAnnotations;

namespace Models.NUnitReports
{
    public class Test
    {
        [Key] public int TestCaseId { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string MethodName { get; set; }
        public string FullName { get; set; }
        public string ClassName { get; set; }
        public Properties Properties { get; set; }
        public string Arguments { get; set; }
    }
}