﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.NUnitReports
{
    public class TestCurrentContext
    {
        [Key] public int Id { get; set; }
        public Guid TestExecutionId { get; set; }
        public Test Test { get; set; }
        public Result Result { get; set; }
        public string WorkerId { get; set; }
        public string TestDirectory { get; set; }
        public string WorkDirectory { get; set; }
        public Random Random { get; set; }
        public int AssertCount { get; set; }
        public int CurrentRepeatCount { get; set; }
    }
}