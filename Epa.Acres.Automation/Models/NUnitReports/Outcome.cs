﻿namespace Models.NUnitReports
{
    public class Outcome
    {
        public int Status { get; set; }
        public string Label { get; set; }
        public int Site { get; set; }
    }
}