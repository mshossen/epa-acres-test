﻿using System.Collections.Generic;

namespace Models.NUnitReports
{
    public class Result
    {
        public Outcome Outcome { get; set; }
        public List<object> Assertions { get; set; }
        public object Message { get; set; }
        public object StackTrace { get; set; }
        public int FailCount { get; set; }
        public int WarningCount { get; set; }
        public int PassCount { get; set; }
        public int SkipCount { get; set; }
        public int InconclusiveCount { get; set; }
    }
}