﻿using System.Collections.Generic;

namespace Models.TestDataModel
{
    public class ProgramAccomplishmentReportModel
    {
        public IList<string> SelectRandomAccomplishmentTypes { get; set; }
        public IList<string> SelectRandomCaTypes { get; set; }
        public int Year { get; set; }
        public int Region { get; set; }
        public string State { get; set; }
    }
}