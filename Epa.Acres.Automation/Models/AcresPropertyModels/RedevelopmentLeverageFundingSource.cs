﻿using System;
using Epa.Acres.Core.Extensions;

namespace Models.AcresPropertyModels
{
    public class RedevelopmentLeverageFundingSource
    {
        private static readonly string[] SourceFundingOptions = new string[]
        {
            "Other Federal Funding",
            "State/Tribal Funding (non-section 128(a))",
            "Local Funding",
            "Private/Other Funding"
        };

        private static readonly double Fund = Math.Round(new Random().NextDouble(0d, 99999d), 2);

        public string SourceOfFunding = SourceFundingOptions?.SelectARandomElement();
        public string EntityProvidingFunding = $"AutoGen Entity";
        public string ActivityFunded = $"AutoGen Activity";
        public double AmountFundingExpended = Fund;
    }
}