﻿namespace Models.AcresPropertyModels
{
    public class AssessmentFundingTotalModel
    {
        public double EpaFundingTotal { get; set; }
        public double LeveragedFundingTotal { get; set; }
        public double TotalFunding { get; set; }
    }
}