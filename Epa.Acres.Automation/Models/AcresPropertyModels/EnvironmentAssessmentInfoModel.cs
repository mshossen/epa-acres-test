﻿using System;
using Epa.Acres.Core.Extensions;

namespace Models.AcresPropertyModels
{
    public class EnvironmentAssessmentInfoModel
    {
        private static readonly string[] Activites = new string[]
        {
            "Phase I Environmental Assessment",
            "Phase II Environmental Assessment",
            "Supplemental Assessment",
            "Cleanup Planning"
        };

        private static readonly string[] SourceOfFundings = new string[]
        {
            "US EPA - Brownfields Assessment Cooperative Agreement",
            "Other Federal Funding",
            "State/Tribal Funding (non-section 128(a))",
            "Local Funding",
            "Private/Other Funding"
        };

        private static readonly int Day = new Random().Next(1, 10);

        public string Activity = Activites?.SelectARandomElement();
        public string StartDate = DateTime.Now.AddDays(-1 * Day).ToString("MM/dd/yyyy");
        public string CompleationDate = DateTime.Now.ToString("MM/dd/yyyy");
        public string SourceOfFunding = SourceOfFundings?.SelectARandomElement();

        public string NameOfEntityProvidingFunds =>
            SourceOfFunding.Equals("US EPA - Brownfields Assessment Cooperative Agreement")
                ? "EPA"
                : $"AutoGen {SourceOfFunding}";

        public string ActivityFunded => SourceOfFunding.Equals("US EPA - Brownfields Assessment Cooperative Agreement")
            ? Activity
            : $"AutoGen {Activity}";

        public double AmountOfFundingExpended = Math.Round(new Random().NextDouble(1d, 10000d), 2);
    }
}