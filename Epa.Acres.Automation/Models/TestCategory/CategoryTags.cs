﻿namespace Models.TestCategory
{
    public class CategoryTags
    {
        public const string CarUser = "carUser";
        public const string Playwright = "playwrite";
        public const string Assessment = "assessment";
        public const string Cleanup = "cleanup";
        public const string AddCa = "addCa";
        public const string EpaUser = "epaUser";
        public const string Par = "par";
    }
}