﻿using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Data.Acres.Data;
using Epa.Acres.Data.Settings;
using NUnit.Framework;

[assembly: Parallelizable(ParallelScope.All)]
[assembly: LevelOfParallelism(5)]

namespace Epa.Acres.Data.Tests
{
    [SetUpFixture]
    public abstract class OracleDbBaseTest<TQueryClass> where TQueryClass : OracleQueryBase
    {
        protected readonly IReportingService TeamsNotification =
            ContainerConfig.ConfigBuilder().Resolve<IReportingService>();

        protected readonly TQueryClass DbQuery = InstanceCreator.GetDbQueryClass<TQueryClass>();

        [OneTimeTearDown]
        public void TearDownQueryConnection()
        {
            DbQuery.Dispose();
            TeamsNotification?.Dispose();
        }
    }
}