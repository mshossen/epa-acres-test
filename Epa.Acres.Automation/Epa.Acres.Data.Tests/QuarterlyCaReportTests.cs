﻿using System;
using System.Linq;
using Epa.Acres.Core.Enums;
using Epa.Acres.Data.Acres.Data;
using FluentAssertions;
using FluentAssertions.Execution;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.Data.Tests
{
    [TestFixture]
    public sealed class QuarterlyCaReportTests : OracleDbBaseTest<QuarterlyCaReportQuery>
    {
        [Test]
        public void GetQrTaskTest()
        {
            var value = DbQuery.GetProjectProgramTask("00TST122");
            Console.WriteLine(JsonConvert.SerializeObject(value));

            using (new AssertionScope())
            {
                value.Select(q => Enum.Parse<GtStatusCode>(q.GTSTATUS_CODE))
                    .Should().AllBeOfType<GtStatusCode>();
            }
        }
    }
}