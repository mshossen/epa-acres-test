﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Epa.Acres.Data.Acres.Data;
using Epa.Acres.Data.Acres.Model;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.Data.Tests
{
    [TestFixture]
    public sealed class GrantPropertyTests : OracleDbBaseTest<GrantPropertyQuery>
    {
        private static string DbCheckResults
        {
            get
            {
                const string dir = @"C:\LinTechProjects\DbCheckResults";
                Directory.CreateDirectory(dir);
                return dir;
            }
        }

        [Test]
        public void CheckGrantPropertyAccomplishmentTest()
        {
            var propertyAccomplishments = DbQuery.GetPropertyAccomplishmentWithCa("00A00234");

            TestContext.Out.Write(JsonConvert.SerializeObject(propertyAccomplishments, Formatting.Indented));
        }

        [Test]
        // [Ignore("Manually need to run monthly")]
        public void LatLongDataConversionCheck()
        {
            var properties = DbQuery.GetAllProperty();

            var failedToConvertProperties = properties.Where(property =>
                !float.TryParse(property.LONGITUDE_MEASURE, out _) ||
                !float.TryParse(property.LONGITUDE_MEASURE, out _)).ToList();

            if (!failedToConvertProperties.Any()) return;
            var file = @$"{DbCheckResults}\propertyDataCheck{DateTime.Now:yyyyMMddHHmmss}.txt";
            var propertyIds = failedToConvertProperties.Select(x => x.PROPERTY_ID.ToString()).ToArray();
            File.WriteAllText(file, string.Join(",", propertyIds));

            TestContext.Out.WriteLine(file);

            TeamsNotification
                .WithEmailSubject("Database Check for Property Lat/Long")
                .AddAttachment(file)
                .WithEmailBody(
                    LatLongReportingData(
                        failedToConvertProperties
                            .Where(i => propertyIds.Contains(i.PROPERTY_ID.ToString())).ToList()
                    )
                )
                .SendEmailReport();
        }

        private static string LatLongReportingData(IEnumerable<GrantPropertyDto> data)
        {
            var sb = new StringBuilder();
            sb.Append("<!DOCTYPE html>");
            sb.Append("<html>");
            sb.Append("	<head>");
            sb.Append("		<meta charset='UTF-8'>");
            sb.Append("			<title>Properties Lat/Long Check</title>");
            sb.Append("		</head>");
            sb.Append("		<body>");
            sb.Append("		<h1>Propertie(s) with empty/incorrect Lat/Long</h>");
            sb.Append("			<table border=1>");
            sb.Append("				<thead>");
            sb.Append("					<tr>");
            sb.Append("						<th>PROPERTY_ID</th>");
            sb.Append("						<th>PROPERTY_NAME</th>");
            sb.Append("						<th>LATITUDE_MEASURE</th>");
            sb.Append("						<th>LONGITUDE_MEASURE</th>");
            sb.Append("					</tr>");
            sb.Append("				</thead>");
            sb.Append("				<tbody>");

            foreach (var item in data)
            {
                sb.Append("					<tr>");
                sb.Append($"						<td>{item.PROPERTY_ID}</td>");
                sb.Append($"						<td>{item.PROPERTY_NAME}</td>");
                sb.Append($"						<td>{item.LATITUDE_MEASURE}</td>");
                sb.Append($"						<td>{item.LONGITUDE_MEASURE}</td>");
                sb.Append("					</tr>");
            }

            sb.Append("				</tbody>");
            sb.Append("			</table>");
            sb.Append("		</body>");
            sb.Append("	</html>");

            return sb.ToString();
        }
    }
}