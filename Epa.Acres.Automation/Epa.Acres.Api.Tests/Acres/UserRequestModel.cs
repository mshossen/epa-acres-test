﻿namespace Epa.Acres.Api.Tests.Acres
{
    public class UserRequestModel
    {
        public string loginType { get; set; }
        public string password { get; set; }
        public string userName { get; set; }
    }
}