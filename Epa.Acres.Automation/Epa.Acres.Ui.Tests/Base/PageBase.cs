﻿using System.Linq;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.Base
{
    public abstract class PageBase : Steps
    {
        protected readonly SessionController Session;

        protected PageBase(SessionController session)
        {
            Session = session;
        }

        protected void NavigateToAcresPage(string page, string section = null)
        {
            if (string.IsNullOrEmpty(page))
            {
                Session.MouseHoverClick($".//a[@id='navbarDropdown' and contains(.,'{section}')]");
            }
            else
            {
                Session.MouseHoverClick($".//a[@id='navbarDropdown' and contains(.,'{section}')]");

                Session.MouseHoverClick(Session.Driver
                    .FindElements(By.XPath(".//ul[@aria-labelledby='navbarDropdown']//a"))
                    .FirstOrDefault(t => t.Text.Contains(page)));
            }
        }
    }
}