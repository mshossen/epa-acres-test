﻿#@epaUser
Feature: User Defined ProgramAccomplishment Report

    @epaUser
    Scenario Outline: Run Accomplishment Report for Selected Year
        Given user navigates to Program Accomplishment Report (PAR) page from Reports
        When EPA user selects Fiscal <Year>
        And EPA user click's Run Report button
        Then EPA user confirm's that selected year is showing in report summary
        And confirm that Number of Assessments Completed matches summary total
        And confirm that Number of Cleanups Completed matches summary total
        And confirm that Dollars Leveraged matches summary total
        And confirm that Job Leveraged matches summary total

        Examples:
          | Year |
          | FY19 |
          | FY18 |