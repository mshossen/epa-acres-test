﻿@epaUser
Feature: Program Accomplishment Report

    @epaUser
    Scenario: Run Current Year Accomplishment Report
        Given user navigates to Program Accomplishment Report (PAR) page from Reports
        When EPA user click's Run Report button
        Then EPA user confirm's default report is showing current year
        And confirm that Number of Assessments Completed matches summary total
        And confirm that Number of Cleanups Completed matches summary total
        And confirm that Dollars Leveraged matches summary total
        And confirm that Job Leveraged matches summary total