﻿@carUser
Feature: Add Cooperative Agreement To CAR User Account
1. Car user selects Grant from Advance Search
2. Then adds to their list

    @carUser
    @addCA
    Scenario: CAR User adds a Cooperative Agreement to exiting list
        Given user Navigates to Advanced Search page
        Then user Clear selected filters
        And user Select Filters
          | Filter |
          | Region |
          | Status |
        And user select Regions
          | Region |
          | 7      |
          | 8      |
        Then user Select from Open Status
        When user click's Apply Filter button
        And user click's on View CA or TBAs
        Then user select's random CA from table
        Given user navigates to Add Cooperative Agreement page from Quick Start
        Then user populates CA number and select's state from previous selection
        Given user navigates to My Cooperative Agreements page from My Account
        Then user verify CA has been added to list