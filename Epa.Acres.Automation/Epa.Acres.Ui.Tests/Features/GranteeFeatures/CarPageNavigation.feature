﻿@carUser
Feature: CAR Page Navigation
Navigate to Acres page from dropdown when logged in as Cooperative Agreement Recepient

    @carUser
    Scenario: Enter Data For Existing Assessment Property
        Given user navigates to Edit Existing Property page from Quick Start
        When user click's on enter data for Assessment Grant Type
        And user click's Save and Continue to NEXT STEP
        Then user update Cleanup Activity Start Date in Environmental Cleanup Information
        And user update Cleanup Activity Completion Date
        Then user click's Add Funding button