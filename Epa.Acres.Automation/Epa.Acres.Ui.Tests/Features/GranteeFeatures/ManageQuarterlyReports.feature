﻿@carUser
@manageQR
Feature: Manage Quarterly Reports
1. Add Quarterly Reports for Cleanup Grants
2. Add Quarterly Reports for Assessment Grants
3. Add Quarterly Reports for BCRLF Grants

    @carUser
    @manageQR
    @cleanup
    Scenario: Create Quarterly Reports for Cleanup
        Given user navigates to Manage My Quarterly Reports page from Reports
        When user click's on Add Quaterly Report for Cleanup CA BF00TST122
        And user click on Project Progress tab
        Then user Add New Task to Project Progress Tab
        And user Add Project Progress Task
        And user select's Status
        When user click's Save and Continue button in Project Progress
        When user click on Funds Expended tab
        Then user provides Funds Expended for Cleanup
        When user click's Save and Continue button in Funds Expense
        When user click on Property Specific Information tab
        Then user populates Property Specific Information data
        When user click's Save and Continue button in Property Specific Information
        When user click on Narrative tab
        Then user populates Budget and Overall Project Status
        And user populates Cost Share and Leveraging Information
        And user populates Federal Cross-Cutters or Other Regulatory Requirements
        When user click's Save and Continue button in Narrative
        When user click on Review & Submit tab

    @carUser
    @manageQR
    @assessment
    Scenario: Create Quarterly Reports for Assessment
        Given user navigates to Manage My Quarterly Reports page from Reports
        When user click's on Add Quaterly Report for Assessment CA BF00TST124
    #And user click on Project Progress tab
    #Then user Add New Task to Project Progress Tab
    #And user Add Project Progress Task
    #And user select's Status
    #When user click's Save and Continue button in Project Progress
        When user click on Funds Expended tab
        Then user provides Funds Expended for Assessment
        When user click's Save and Continue button in Funds Expense

    #When user click on Property Specific Information tab
    #Then user populates Property Specific Information data
    #When user click's Save and Continue button in Property Specific Information
    #When user click on Narrative tab
    #Then user populates Budget and Overall Project Status
    #And user populates Cost Share and Leveraging Information
    #And user populates Federal Cross-Cutters or Other Regulatory Requirements
    #When user click's Save and Continue button in Narrative
    #When user click on Review & Submit tab

    @carUser
    @manageQR
    @bcrlf
    Scenario: Create Quarterly Reports for bcrlf
        Given user navigates to Manage My Quarterly Reports page from Reports
        When user click's on Add Quaterly Report for BCRLF CA BF00TST124
    #And user click on Project Progress tab
    #Then user Add New Task to Project Progress Tab
    #And user Add Project Progress Task
    #And user select's Status
    #When user click's Save and Continue button in Project Progress
        When user click on Funds Expended tab
        Then user provides Funds Expended for BCRLF
        When user click's Save and Continue button in Funds Expense
    #When user click on Property Specific Information tab
    #Then user populates Property Specific Information data
    #When user click's Save and Continue button in Property Specific Information
    #When user click on Narrative tab
    #Then user populates Budget and Overall Project Status
    #And user populates Cost Share and Leveraging Information
    #And user populates Federal Cross-Cutters or Other Regulatory Requirements
    #When user click's Save and Continue button in Narrative
    #When user click on Review & Submit tab