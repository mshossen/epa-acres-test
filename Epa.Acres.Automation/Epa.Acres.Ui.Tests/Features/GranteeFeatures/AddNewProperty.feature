﻿@carUser
Feature: Add New Property

    @carUser
    @addProperty
    Scenario: Create New Property for Existing CA
        Given user navigates to Add New Property page from Quick Start
        Then user select's from any existing Cooperative Agreement
        And clicks on Save and Continue to Next Step button
        When user clicks on ApplyFiler or Display Properties button
        And user clicks on Create new property button
        Then user confirms that CA Name and Id pre-selected
        And user check’s for funding type is disabled when grant fiscal year is grater than or equal to FY20
        And user select's a random funding type when fiscal year is before FY20
        When user populate's Property Name
        And user populate's Address
        And user populate's Zip Code
        And user populate's City
        And user select's state
        And user populate's property acres size
        When user Save and Continue to Next Step
        Then user confirm's created property information is correct