﻿@dataAdmin
Feature: State Tribal Contaminant Report
1. Login as Data Admin User
2. Take over user and fill out state and tribal contaminant report

    Background:
        Given user loged-in and is currently on the homepage
        Then takes over user account and navigates to State Tribal Contaminant site

    @dataAdmin
    @stvrpContaminants
    Scenario: Fills State Tribal Contaminant Data
        Given user click's on State/Tribal Report link
        And user is current on Cleaning Up Brownfields Under State Voluntary Response Programs
        And user narrow results to Idaho
        Then user clicks on Edit link under Actions column
        Then user click's Add Additional Contact and provide contect infromation
        And user click's on Save and Continue button
        And user click's on Program Overview tab
        When user click on Add Highlight button