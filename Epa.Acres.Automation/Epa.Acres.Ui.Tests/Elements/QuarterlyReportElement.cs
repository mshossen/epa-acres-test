﻿using OpenQA.Selenium;

namespace Epa.Acres.Ui.Tests.Elements
{
    public class QuarterlyReportElement
    {
        public IWebElement Action { get; set; }
        public IWebElement AnnouncementYear { get; set; }
        public IWebElement CaName { get; set; }

        public IWebElement CaNumber { get; set; }
        public IWebElement CaStatus { get; set; }
        public IWebElement CaType { get; set; }

        public IWebElement FundingTypes { get; set; }
        public IWebElement PreviousQuarterlyReports { get; set; }
        public IWebElement State { get; set; }
    }
}