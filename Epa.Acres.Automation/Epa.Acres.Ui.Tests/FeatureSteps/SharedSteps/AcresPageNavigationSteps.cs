﻿using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.FeatureSteps.SharedSteps
{
    [Binding]
    public sealed class AcresPageNavigationSteps : PageBase
    {
        public AcresPageNavigationSteps(SessionController session) : base(session)
        {
        }

        [Given(@"user navigates to (.*) page from (.*)")]
        public void GivenUserNavigatesPageFromDropDownMenu(string page, string section)
        {
            NavigateToAcresPage(page, section);
        }
    }
}