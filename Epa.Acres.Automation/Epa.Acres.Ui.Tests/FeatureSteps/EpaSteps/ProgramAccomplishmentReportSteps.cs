﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Extensions;
using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.FeatureSteps.EpaSteps
{
    [Binding]
    public sealed class ProgramAccomplishmentReportSteps : PageBase
    {
        private string _userSelectedFiscalYear;

        public ProgramAccomplishmentReportSteps(SessionController session) : base(session)
        {
        }

        [Then(@"confirm that Dollars Leveraged matches summary total")]
        public void ThenConfirmThatDollarsLeveragedMatchesSummaryTotal()
        {
            var calculatedTotal = GetSummaryTableValues(ParAccomplishmentSummary.DollarsLeveraged)
                .Select(v => double.Parse(v, NumberStyles.Currency));

            var expectedTotal =
                Math.Round(
                    double.Parse(GetTotalAccomplishmentValue(ParAccomplishmentSummary.DollarsLeveraged),
                        NumberStyles.Currency), 2);

            TestContext.Out.Write(JsonConvert.SerializeObject(calculatedTotal, Formatting.Indented));

            Math.Round(calculatedTotal.Sum(), 2)
                .Should().Be(expectedTotal);
        }

        [Then(@"confirm that Job Leveraged matches summary total")]
        public void ThenConfirmThatJobLeveragedMatchesSummaryTotal()
        {
            GetSummaryTableValues(ParAccomplishmentSummary.JobsLeveraged)
                .Select(v => int.Parse(v, NumberStyles.AllowThousands)).Sum()
                .Should().Be(int.Parse(GetTotalAccomplishmentValue(ParAccomplishmentSummary.JobsLeveraged),
                    NumberStyles.AllowThousands));
        }

        [Then(@"confirm that Number of Assessments Completed matches summary total")]
        public void ThenConfirmThatNumberOfAssessmentsCompletedMatchesSummaryTotal()
        {
            GetSummaryTableValues(ParAccomplishmentSummary.NumberOfAssessmentsCompleted)
                .Select(v => int.Parse(v, NumberStyles.AllowThousands)).Sum()
                .Should().Be(int.Parse(
                    GetTotalAccomplishmentValue(ParAccomplishmentSummary.NumberOfAssessmentsCompleted),
                    NumberStyles.AllowThousands));
        }

        [Then(@"confirm that Number of Cleanups Completed matches summary total")]
        public void ThenConfirmThatNumberOfCleanupsCompletedMatchesSummaryTotal()
        {
            GetSummaryTableValues(ParAccomplishmentSummary.NumberOfCleanupsCompleted)
                .Select(v => int.Parse(v, NumberStyles.AllowThousands)).Sum()
                .Should().Be(int.Parse(GetTotalAccomplishmentValue(ParAccomplishmentSummary.NumberOfCleanupsCompleted),
                    NumberStyles.AllowThousands));
        }

        [Then(@"EPA user confirm's default report is showing current year")]
        public void ThenEPAUserConfirmSDefaultReportIsShowingCurrentYear()
        {
            Session.WithXPath(".//table[@id='summaryTable']/tfoot/tr[2]/td[2]").Text
                .Should().Be(DateTime.Now.ToFiscalYear());
        }

        [Then(@"EPA user confirm's that selected year is showing in report summary")]
        public void ThenEPAUserConfirmSThatSelectedYearIsShowingInReportSummary()
        {
            Session.WithXPath(".//table[@id='summaryTable']/tfoot/tr[2]/td[2]").Text
                .Should().Be(_userSelectedFiscalYear);
        }

        [When(@"EPA user click's Run Report button")]
        public void ThenUserClickSRunReportButton()
        {
            Session.WithId("submitbuttonsave").Click();
        }

        [When(@"EPA user selects Fiscal (.*)")]
        public void WhenEPAUserSelectsFiscal(string fiscalYear)
        {
            _userSelectedFiscalYear = fiscalYear;

            if (!Session.WithXPath(".//span[@id='year-selected-text']").Text.Equals(fiscalYear))
            {
                Session.WithXPath(".//span[@id='year-selected-text']/parent::button").Click();

                foreach (var element in
                         Session.WithXPaths(".//ul[@id='multiselectUL-year']/li/a/label/input[@checked]"))
                    Session.MouseHoverClick(element);

                Session.MouseHoverClick($".//ul[@id='multiselectUL-year']/li/a/label/input[@value='{fiscalYear}']");
                Session.WithXPath(".//h2").Click();
            }
        }

        private IList<string> GetSummaryTableValues(ParAccomplishmentSummary summary)
        {
            return Session.WithXPaths($".//table[@id='summaryTableCount']/tbody/tr/td[{(int) summary}]")
                .Select(t => t.Text).ToList();
        }

        private string GetTotalAccomplishmentValue(ParAccomplishmentSummary summary)
        {
            return Session
                .WithXPath(
                    $".//table[@id='summaryTable']/tbody/tr/td[text()='{summary.GetDescription()}']/following-sibling::td")
                .Text;
        }
    }
}