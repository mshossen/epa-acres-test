﻿using System.Linq;
using Epa.Acres.Core.Models.BrownfiledStateVRP;
using Epa.Acres.Core.Processors;
using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.FeatureSteps.DataAdminSteps
{
    [Binding]
    public class StateTribalContaminantReportSteps : PageBase
    {
        private ContactInfoSvrpModel _svrpContactInfo;

        public StateTribalContaminantReportSteps(SessionController session) : base(session)
        {
        }

        private IWebElement LoginTakeOverBox => Session.WithXPath(
            ".//form[@action='/acres6/adminLoginTakeover']/div[contains(text(),'Login takeover:')]/div/input[@name='user_name']");

        //private IWebElement StateTribalReportLink => _session.WithXPath(".//a[@href='/acres6/jquery/stv/stvMain']");
        private IWebElement StateTribalReportLink =>
            Session.WithXPath(".//div[@class='flip-card']/div/div/div/ul/li/a[@href='/acres6/jquery/stv/stvMain']");

        [Given(@"user click's on State/Tribal Report link")]
        public void GivenUserClickSOnStateTribalReportLink()
        {
            StateTribalReportLink.Click();
        }

        [Given(@"user is current on Cleaning Up Brownfields Under State Voluntary Response Programs")]
        public void GivenUserIsCurrentOnCleaningUpBrownfieldsUnderStateVoluntaryResponsePrograms()
        {
            var contains = Session.WithCss("h2.acres-page-title").Text
                .Contains("Cleaning Up Brownfields Under State Voluntary Response Programs");
        }

        [Given(@"user loged-in and is currently on the homepage")]
        public void GivenUserLoged_InAndIsCurrentlyOnTheHomepage()
        {
            LoginTakeOverBox.Displayed.Should().BeTrue();
        }

        [Given(@"user narrow results to (.*)")]
        public void GivenUserNarrowResultsToState(string state)
        {
            Session.WithCss("#stvrpTable_filter>label>input").SendKeys(state);
        }

        [Then(@"takes over user account and navigates to State Tribal Contaminant site")]
        public void ThenTakesOverUserAccountAndNavigatesToStateTribalContaminantSite()
        {
            LoginTakeOverBox.SendKeys(Session.UserService.UserConfig.StvrpUser);
            Session.WithCss("form[action='/acres6/adminLoginTakeover']>div>div>input.btn.btn-primary").Click();

            StateTribalReportLink.Text.Should().Contain("State/Tribal Report");
        }

        [Then(@"user click's Add Additional Contact and provide contect infromation")]
        public void ThenUserClicksAddAdditionalContactAndProvideContectInfromation()
        {
            var deleteItems = Session.WithXPaths(".//table[@id='contactTable']/tbody/tr/td[7]/a[2]").Skip(1);

            foreach (var item in deleteItems) item.Click();

            _svrpContactInfo = MockSvrpContactInfoProcess.CreateContactInfo();
            //ScenarioContext.Current.Pending();
            Session.WithCss("#addContactBtn").Click();

            Session.WithXPath(".//table[@id='contactTable']/tbody/tr/td[1]/input[not(@value)]")
                .SendKeys(_svrpContactInfo.ContactName);

            Session.WithXPath(".//table[@id='contactTable']/tbody/tr/td[2]/input[not(@value)]")
                .SendKeys(_svrpContactInfo.Organization);

            Session.WithXPath(".//table[@id='contactTable']/tbody/tr/td[3]/input[not(@value)]")
                .SendKeys(_svrpContactInfo.Title);

            Session.WithXPath(".//table[@id='contactTable']/tbody/tr/td[4]/input[not(@value)]")
                .SendKeys(_svrpContactInfo.Email);

            Session.WithXPath(".//table[@id='contactTable']/tbody/tr/td[5]/input[not(@value)]")
                .SendKeys(_svrpContactInfo.PhoneNumber);

            Session.WithXPath(".//table[@id='contactTable']/tbody/tr/td[6]/input[not(@value)]")
                .SendKeys(_svrpContactInfo.ProgramWebsite);

            TestContext.Out.WriteLine(JsonConvert.SerializeObject(_svrpContactInfo, Formatting.Indented));
        }

        [Then(@"user clicks on Edit link under Actions column")]
        public void ThenUserClicksOnEditLinkUnderActionsColumn()
        {
            var actionsLink = Session.WithXPaths(".//table[@id='stvrpTable']/tbody/tr/td/a[contains(., 'Edit ')]");
            actionsLink.First().Click();
        }

        [Then(@"user click's on (.*) tab")]
        public void ThenUserClicksOnProgramOverviewTab(string currentSvrpTab)
        {
            Session.WithXPath(@$".//div/ul[@class='nav nav-tabs']/li/a[text()='{currentSvrpTab}']").Click();
        }

        [Then(@"user click's on Save and Continue button")]
        public void ThenUserClicksOnSaveAndContinueButton()
        {
            Session.WithId("btnSaveAndContinueContactInfo").Click();
        }

        [When(@"user click on Add Highlight button")]
        public void WhenUserClickOnAddHighlightButton()
        {
            Session.WithId("addHighlightBtn").Click();
        }
    }
}