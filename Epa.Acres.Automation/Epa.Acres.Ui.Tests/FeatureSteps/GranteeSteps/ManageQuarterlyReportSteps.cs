﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Models.Reports;
using Epa.Acres.Data.MockData;
using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Elements;
using Epa.Acres.Ui.Tests.Extensions;
using FluentAssertions;
using FluentAssertions.Execution;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.FeatureSteps.GranteeSteps
{
    [Binding]
    public sealed class ManageQuarterlyReportSteps : PageBase
    {
        private ProjectProgressModel _programTask;
        private Dictionary<string, double> _runtimeFundsData;

        public ManageQuarterlyReportSteps(SessionController session) : base(session)
        {
        }

        private IList<QuarterlyReportElement> ManageQuarterlyReports
        {
            get
            {
                var rows = Session.WithXPaths(".//table[@id='coopAgreementTable']/tbody/tr");

                return rows.Select(element => new QuarterlyReportElement
                    {
                        CaName = element.FindElement(By.XPath(".//td[1]")),
                        CaStatus = element.FindElement(By.XPath(".//td[2]")),
                        CaNumber = element.FindElement(By.XPath(".//td[3]")),
                        AnnouncementYear = element.FindElement(By.XPath(".//td[4]")),
                        CaType = element.FindElement(By.XPath(".//td[5]")),
                        State = element.FindElement(By.XPath(".//td[6]")),
                        FundingTypes = element.FindElement(By.XPath(".//td[7]")),
                        PreviousQuarterlyReports = element.FindElement(By.XPath(".//td[8]"))
                        //Action = IsQrActionLinkVisible(element.FindElement(By.XPath(".//td[9]/a")))
                    })
                    .ToList();
            }
        }

        private Dictionary<string, PropertySpecific> PropertySpecificInfo { get; } =
            new Dictionary<string, PropertySpecific>
            {
                {"HAZFUND_", PropertySpecific.TDouble},
                {"PETROLFUND_", PropertySpecific.TDouble},
                {"QEPSERVICE_", PropertySpecific.TTime},
                {"COMRELPLAN_", PropertySpecific.TTime},
                {"INFOREPO_", PropertySpecific.TTime},
                {"30DCOMM_", PropertySpecific.TTime},
                {"PUBMEET_", PropertySpecific.TTime},
                {"COMMENDS_", PropertySpecific.TTime},
                {"ABCA_", PropertySpecific.TTime},
                {"SSQAPP_", PropertySpecific.TTime}
            };

        private static Random Random => new Random();

        #region When

        [When("user click's on Add Quaterly Report for (.*) CA (.*)")]
        public void WhenUserClickOnAddQuaterlyReportforCa(string caType, string caNumber)
        {
            var editQr =
                $".//table[@id='coopAgreementTable']/tbody/tr/td[position()=3 and text()='{caNumber}']/following-sibling::td[text()='{caType}']/following-sibling::td[4]/a";

            Session.WithXPath(editQr).Click();
        }

        [When(@"user click on (.*) tab")]
        public void WhenUserClickOnTab(string tabs)
        {
            var tab = Session.WithXPaths(".//a[@data-toggle='tab']")
                .FirstOrDefault(t => t.Text.ToLower().Contains(tabs.ToLower()));

            try
            {
                if (tab.FindElement(By.XPath(".//parent::li")).GetAttribute("class").EndsWith(" active")) return;
                tab.Click();
            }
            catch (ElementClickInterceptedException)
            {
                Session.ScrollToElement(tab, true);
                if (tab.FindElement(By.XPath(".//parent::li")).GetAttribute("class").EndsWith(" active")) return;
                tab.Click();
            }
        }

        [When(@"user click's Save and Continue button in Funds Expense")]
        public void WhenUserClickSSaveAndContinueButtonInFundsExpense()
        {
            Session.WithXPath(".//form[@id='frmFinance']//button[text()='Save and Continue']").Click();
        }

        [When(@"user click's Save and Continue button in Narrative")]
        public void WhenUserClickSSaveAndContinueButtonInNarrative()
        {
            Session.WithXPath(".//form[@id='frmNarratives']//button[text()='Save and Continue']").Click();
        }

        [When(@"user click's Save and Continue button in Project Progress")]
        public void WhenUserClicksSaveAndContinueButtonInProjectProgress()
        {
            Session.WithXPath(".//form[@id='frmProjProg']//button[text()='Save and Continue']").Click();
        }

        [When(@"user click's Save and Continue button in Property Specific Information")]
        public void WhenUserClickSSaveAndContinueButtonInPropertySpecificInformation()
        {
            Session.WithXPath(".//form[@id='frmPropData']//button[text()='Save and Continue']").Click();
        }

        #endregion When

        #region Then

        [Then(@"user Add New Task to Project Progress Tab")]
        public void ThenUserAddNewTaskToProjectProgressTab()
        {
            Session.WithXPath(".//a[text()='+ Add New Task']").Click();
        }

        [Then(@"user Add Project Progress Task")]
        public void ThenUserAddProjectProgressTask()
        {
            _programTask = QuarterlyReportMock.ProgressTasks[Random.Next(QuarterlyReportMock.ProgressTasks.Count)];

            TestContext.Out.WriteLine(
                $"Select random tasks \n{JsonConvert.SerializeObject(_programTask, Formatting.Indented)}");

            Session.WithId($"qrtask_name_new_1").SendKeys(_programTask.Tasks);
            EditQuarterSummaryProgress(_programTask.QuarterProgressSummary);
        }

        [Then(@"user populates Budget and Overall Project Status")]
        public void ThenUserPopulatesBudgetAndOverallProjectStatus()
        {
            var budgetData =
                QuarterlyReportMock.BudgetAndOverallProjectStatus[
                    Random.Next(QuarterlyReportMock.BudgetAndOverallProjectStatus.Count)];
            TestContext.Out.WriteLine(budgetData);

            Session.WithXPath(".//button[@data-target='#qrn_BDGTSTATUS_modalDiv']").Click();
            Thread.Sleep(500);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(500);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(500);
            ClearModalTextArea();
            Session.Actions.SendKeys(budgetData).Build().Perform();
            Session.MouseHoverClick(Session.WithId("qrn_BDGTSTATUS_modalSaveButton", scroll: false));
        }

        [Then(@"user populates Cost Share and Leveraging Information")]
        public void ThenUserPopulatesCostShareAndLeveragingInformation()
        {
            var budgetData =
                QuarterlyReportMock.CostShareAndLeveragingInformation[
                    Random.Next(QuarterlyReportMock.CostShareAndLeveragingInformation.Count)];
            TestContext.Out.WriteLine(budgetData);

            Session.WithXPath(".//button[@data-target='#qrn_SCHFCC_modalDiv']").Click();
            Thread.Sleep(500);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(500);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(500);
            ClearModalTextArea();
            Session.Actions.SendKeys(budgetData).Build().Perform();
            Session.MouseHoverClick(Session.WithId("qrn_SCHFCC_modalSaveButton", scroll: false));
        }

        [Then(@"user populates Federal Cross-Cutters or Other Regulatory Requirements")]
        public void ThenUserPopulatesFederalCrossCuttersOrOtherRegulatoryRequirements()
        {
            var budgetData =
                QuarterlyReportMock.FederalCrossCutters[Random.Next(QuarterlyReportMock.FederalCrossCutters.Count)];
            TestContext.Out.WriteLine(budgetData);

            Session.WithXPath(".//button[@data-target='#qrn_SCHPROJ_modalDiv']").Click();
            Thread.Sleep(500);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(500);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(500);
            ClearModalTextArea();
            Session.Actions.SendKeys(budgetData).Build().Perform();
            Session.MouseHoverClick(Session.WithId("qrn_SCHPROJ_modalSaveButton", scroll: false));
        }

        [Then(@"user populates Property Specific Information data")]
        public void ThenUserPopulatesPropertySpecificInformationData()
        {
            var testData = new Dictionary<string, string>();
            var elementIds = Session.WithXPaths(".//form[@id='frmPropData']//tbody/tr/td/input");

            foreach (var propertyInfo in PropertySpecificInfo)
            {
                var inputFields = elementIds.Where(e =>
                    e.GetAttribute("id").ToLower().StartsWith(propertyInfo.Key.ToLower()));

                foreach (var item in inputFields)
                {
                    item.Clear();
                    var value = CreatePropertyData(propertyInfo.Value);
                    item.SendKeys(value);

                    testData.Add(item.GetAttribute("name"), value);
                }
            }

            TestContext.Out.Write(JsonConvert.SerializeObject(testData, Formatting.Indented));
        }

        [Then(@"user provides Funds Expended for (.*)")]
        public void ThenUserProvidesFundsExpended(string grantType)
        {
            _runtimeFundsData = new Dictionary<string, double>();
            var funds = Session.WithXPaths(".//form[@id='frmFinance']//tbody/tr/td/input");

            foreach (var fund in funds)
            {
                var value = QuarterlyReportMock.RandomFinanceData;
                fund.Clear();
                Task.Delay(250).Wait();
                fund.SendKeys(value.ToString());
                Session.Actions.SendKeys(Keys.Tab).Perform();
                _runtimeFundsData.Add(fund.GetAttribute("name"), value);
            }

            TestContext.Out.WriteLine(JsonConvert.SerializeObject(_runtimeFundsData, Formatting.Indented));

            if (grantType.ToLower().Equals("cleanup"))
                using (new AssertionScope())
                {
                    CalculateTotalRemaining("_PERSONNEL").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_PERSONNEL_TOTREMAIN").Text.Trim(),
                            NumberStyles.Currency));

                    CalculateTotalRemaining("_FRINGEBEN").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_FRINGEBEN_TOTREMAIN").Text.Trim(),
                            NumberStyles.Currency));

                    CalculateTotalRemaining("_TRAVEL").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_TRAVEL_TOTREMAIN").Text.Trim(), NumberStyles.Currency));

                    CalculateTotalRemaining("_EQUIP").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_EQUIP_TOTREMAIN").Text.Trim(), NumberStyles.Currency));

                    CalculateTotalRemaining("_SUPPLIES").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_SUPPLIES_TOTREMAIN").Text.Trim(), NumberStyles.Currency));

                    CalculateTotalRemaining("_CONTRACT").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_CONTRACT_TOTREMAIN").Text.Trim(), NumberStyles.Currency));

                    CalculateTotalRemaining("_OTHER").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_OTHER_TOTREMAIN").Text.Trim(), NumberStyles.Currency));

                    CalculateTotalRemaining("_COSTSHARE").TotalRemaining.Should()
                        .Be(double.Parse(Session.WithId("td_1_COSTSHARE_TOTREMAIN").Text.Trim(),
                            NumberStyles.Currency));
                }
        }

        [Then(@"user select's Status")]
        public void ThenUserSelectStatus()
        {
            Session.DropDownSelect(
                    By.XPath($".//input[@value='{_programTask.Tasks}']/parent::td/following-sibling::td[2]/select"))
                .SelectByText(_programTask.Status);
        }

        #endregion Then

        #region Private Methods

        private QrFundsExpendedModel CalculateTotalRemaining(string matchingKey)
        {
            var category = _runtimeFundsData.Where(k => k.Key.EndsWith(matchingKey));

            return new QrFundsExpendedModel
            {
                InitialApprovedBudget = category.Where(k => k.Key.Contains("_INITBUDGET")).Select(k => k.Value)
                    .FirstOrDefault(),
                CurrentApprovedBudget = category.Where(k => k.Key.Contains("_CURRBUDGET")).Select(k => k.Value)
                    .FirstOrDefault(),
                CostIncurredCurrentQuarter = category.Where(k => k.Key.Contains("_COSTSQTR")).Select(k => k.Value)
                    .FirstOrDefault()
            };
        }

        private void ClearModalTextArea()
        {
            for (var i = 0; i < 1000; i++) Session.Actions.SendKeys(Keys.Delete).Build().Perform();

            Thread.Sleep(500);
        }

        private string CreatePropertyData(PropertySpecific dataType)
        {
            return dataType switch
            {
                PropertySpecific.TDouble => QuarterlyReportMock.RandomFinanceData.ToString(),
                PropertySpecific.TTime => QuarterlyReportMock.RandomDate,
                _ => string.Empty
            };
        }

        private void EditQuarterSummaryProgress(string summary)
        {
            Session.WithXPath($".//div[@id='qrtask_desc_1']/following-sibling::button").Click();
            Thread.Sleep(1000);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(1000);
            Session.Actions.SendKeys(Keys.Tab).Perform();
            Thread.Sleep(1000);
            Session.Actions.SendKeys(summary).Build().Perform();
            Session.MouseHoverClick(Session.WithId("qrtask_desc_1_modalSaveButton", scroll: false));
        }

        #endregion Private Methods

        //private IWebElement IsQrActionLinkVisible(IWebElement element)
        //{
        //  var wait = new DefaultWait<IWebElement>(element)
        //  {
        //    Message = "Element Retry",
        //    PollingInterval = TimeSpan.FromMilliseconds(250),
        //    Timeout = TimeSpan.FromSeconds(5)
        //  };
        //  //     wait.IgnoreExceptionTypes(new[] { typeof(NoSuchElementException), typeof(ElementNotVisibleException) });

        //  return wait.Until(e =>
        //   {
        //     try
        //     {
        //       return e;
        //     }
        //     catch (NoSuchElementException)
        //     {
        //       return null;
        //     }
        //   });
        //}
    }
}