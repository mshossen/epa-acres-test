﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Models;
using Epa.Acres.Core.Models.TestData;
using Epa.Acres.Core.Processors;
using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using FluentAssertions;
using FluentAssertions.Execution;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.FeatureSteps.GranteeSteps
{
    [Binding]
    public sealed class AddNewPropertySteps : PageBase
    {
        private int _announcementYear;
        private string _propertyCreatedMessage;
        private int _year;

        public AddNewPropertySteps(SessionController session) : base(session)
        {
        }

        private IWebElement City => Session.WithId("city");

        private IEnumerable<IWebElement> ContinueNextButtons =>
            Session.WithActiveElements(By.XPath(".//button[contains(.,'Save and Continue to NEXT STEP')]"));

        private IEnumerable<IWebElement> CreatNewPropertyButtons =>
            Session.WithXPaths(".//a[contains(.,'Create new property')]");

        private IEnumerable<IWebElement> FundingTypes
        {
            get
            {
                try
                {
                    return Session.Driver.FindElements(By.XPath(".//input[@id='haz_pet']"));
                }
                catch (NoSuchElementException element)
                {
                    TestContext.Out.WriteLine(element.Message);
                    return null;
                }
            }
        }

        private bool IsDuplicateWarningDisplayed
        {
            get
            {
                try
                {
                    return Session.Driver.FindElement(By.XPath(".//div[@id='duplicateCheckModal']")).Displayed ||
                           Session.Driver.FindElement(By.XPath(".//div[@id='duplicateCheckModal']")).Enabled;
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
        }

        private IList<MockPropertyModel> MockProperties { get; set; }
        private MockPropertyDataProcessor MockPropertyData => new MockPropertyDataProcessor();
        private IWebElement NewPropertyId => Session.WithId(".//p[@id='propId']");
        private SelectElement PreSelectedGrant => Session.DropDownSelect(By.XPath(".//select[@id='grantId']"));

        private IEnumerable<IWebElement> PropertyRows =>
            Session.WithActiveElements(By.XPath(".//table[@id='coopAgreementTable']/tbody/tr"), 0.30d);

        private IWebElement SaveAndContinue => Session.WithXPath(".//input[@id='continue']");
        private CaPropertyModel SelectCaProperty { get; set; }
        private MockPropertyModel SelectRandomProperty { get; set; }

        [Then(@"clicks on Save and Continue to Next Step button")]
        public void ThenClicksOnSaveAndContinueToNextStepButton()
        {
            var index = new Random().Next(ContinueNextButtons.Count());
            ContinueNextButtons.ElementAt(index).Click();
        }

        [Then("user confirms that CA Name and Id pre-selected")]
        public void ThenUserConfirmsThatCaNameAndIdPreSelected()
        {
            Thread.Sleep(2000);

            if (!PreSelectedGrant.SelectedOption.Text.Contains(
                    $"{SelectCaProperty.CaName} {SelectCaProperty.CaNumber}"))
            {
                TestContext.Out.WriteLine(
                    $"Ca Name with Id: {SelectCaProperty.CaName} {SelectCaProperty.CaNumber} was not pre selected");
                PreSelectedGrant.SelectByValue(SelectCaProperty.GrantId.ToString());
            }

            using (new AssertionScope())
            {
                PreSelectedGrant.SelectedOption.Text
                    .Should().Contain($"{SelectCaProperty.CaNumber}");
            }

            MockProperties = new MockPropertyDataProcessor().GetPropertiesByState(SelectCaProperty.State);
            SelectRandomProperty = MockProperties.ElementAtOrDefault(new Random().Next(0, MockProperties.Count() - 1));
        }

        [Then(@"user select's from any existing Cooperative Agreement")]
        public void ThenUserSelectFromAnyExistingCooperativeAgreement()
        {
            var selectARandomProperty = PropertyRows.ElementAt(new Random().Next(0, PropertyRows.Count() - 1));

            SelectCaProperty = new CaPropertyModel
            {
                GrantId = int.Parse(selectARandomProperty.FindElement(By.XPath("//td[1]/input")).GetAttribute("value")),
                CaName = selectARandomProperty.FindElement(By.XPath("//td[2]/a")).Text,
                AssociatedProperties = selectARandomProperty.FindElements(By.XPath(".//td[3]/div/a")).Where(
                    t => !t.Text.Contains("Enter data")).Select(t => t.Text).ToList(),
                CaStatus = selectARandomProperty.FindElement(By.XPath("//td[4]")).Text.Trim(' '),
                CaNumber = selectARandomProperty.FindElement(By.XPath("//td[5]")).Text,
                CaType = selectARandomProperty.FindElement(By.XPath("//td[6]")).Text,
                AnnouncementYear = selectARandomProperty.FindElement(By.XPath("//td[7]")).Text,
                State = selectARandomProperty.FindElement(By.XPath("//td[8]")).Text,
                FundingType = selectARandomProperty.FindElement(By.XPath("//td[9]")).Text,
                AwardAmount = double.Parse(
                    !string.IsNullOrWhiteSpace(selectARandomProperty.FindElement(By.XPath("//td[10]")).Text)
                        ? selectARandomProperty.FindElement(By.XPath("//td[10]")).Text.Trim(' ')
                        : "$0.00", NumberStyles.Currency)
            };

            TestContext.Out.Write(JsonConvert.SerializeObject(SelectCaProperty, Formatting.Indented));

            SelectCa(SelectCaProperty.GrantId);
        }

        [Then(@"user select's a random funding type when fiscal year is before FY20")]
        public void ThenUserSelectSARandomFundingTypeWhenFiscalYearIsBeforeFY20()
        {
            if (_announcementYear < _year && !SelectCaProperty.CaType.Contains("Section 128(a) State/Tribal"))
                FundingTypes.ElementAtOrDefault(new Random().Next(FundingTypes.Count()))?.Click();
        }

        [Then("user check’s for funding type is disabled when grant fiscal year is grater than or equal to (.*)")]
        public void UserCheckForFundingTypeIsDisabledWhenGrantFiscalYearIsGraterThanOrEqualTo(string fy)
        {
            _year = int.Parse(new RegexHelper(@"\d+", fy).GetMatchedPatter(0));

            _announcementYear =
                int.Parse(new RegexHelper(@"\d+", SelectCaProperty.AnnouncementYear).GetMatchedPatter(0));

            if (_announcementYear >= _year && !SelectCaProperty.CaType.Contains("Section 128(a) State/Tribal"))
                FundingTypes.All(d => !d.Enabled).Should().BeTrue();

            if (_announcementYear < _year && !SelectCaProperty.CaType.Contains("Section 128(a) State/Tribal"))
                FundingTypes.All(d => d.Enabled).Should().BeTrue();
        }

        [When("user populate's Address")]
        public void UserPopulateAddress()
        {
            Session.WithId("address1").SendKeys(SelectRandomProperty.Address);
        }

        [When("user populate's Property Name")]
        public void UserPopulatePropertyName()
        {
            Session.WithId("property_name").SendKeys(SelectRandomProperty.PropertyName);
        }

        [When("user populate's Zip Code")]
        public void UserPopulateZipCode()
        {
            Session.WithId("zip_code").SendKeys(SelectRandomProperty.ZipCode);
        }

        [When(@"user clicks on ApplyFiler or Display Properties button")]
        public void WhenUserClicksOnApplyFilerOrDisplayPropertiesButton()
        {
            Session.WithXPath(".//button[@id='page-apply-filter']").Click();
        }

        [When(@"user clicks on Create new property button")]
        public void WhenUserClicksOnCreateNewPropertyButton()
        {
            CreatNewPropertyButtons.ElementAt(new Random().Next(0, CreatNewPropertyButtons.Count() - 1)).Click();
        }

        [Then("user confirm's created property information is correct")]
        public void WhenUserConfirmsCreatedPropertyInformationIsCorrect()
        {
            if (!string.IsNullOrWhiteSpace(_propertyCreatedMessage))
            {
                TestContext.Out.WriteLine(_propertyCreatedMessage);
                return;
            }

            Session.NewPropertyLog.CreateNewProperty(new NewPropertyModel
            {
                SessionId = Session.SessionId,
                CaNumber = SelectCaProperty.CaNumber,
                CaState = Session.WithXPath(".//p[@id='propState']").Text,
                CaStateAbbreviation = SelectCaProperty.State,
                CaName = SelectCaProperty.CaName,
                CaYear = SelectCaProperty.AnnouncementYear,
                CaStatus = SelectCaProperty.CaStatus,
                CaType = SelectCaProperty.CaType,
                CaAwardedAmount = (decimal) SelectCaProperty.AwardAmount,
                PropertyJson = JsonConvert.SerializeObject(SelectRandomProperty),
                PropertyId = Session.Driver.FindElement(By.XPath("//*[@id='propId']")).Text,
                CreatedDate = DateTime.Now,
                IsNewProperty = string.IsNullOrWhiteSpace(_propertyCreatedMessage),
                IsApprovedWorkPackage = !string.IsNullOrWhiteSpace(_propertyCreatedMessage)
                    ? true
                    : false
            });

            using (new AssertionScope())
            {
                Session.WithXPath(".//p[@id='propName']").Text
                    .Should().Be(SelectRandomProperty.PropertyName);

                Session.WithXPath(".//p[@id='propState']").Text
                    .Should().Be(SelectRandomProperty.State);
            }
        }

        [When("user populate's property acres size")]
        public void WhenUserPopulatePropertyAcresSize()
        {
            Session.WithId("property_size").SendKeys(SelectRandomProperty.Acres.ToString(CultureInfo.InvariantCulture));
        }

        [When("user populate's City")]
        public void WhenUserPopulatesCity()
        {
            City.SendKeys(Keys.Home);
            City.SendKeys(Keys.Control + Keys.Delete);
            City.SendKeys(SelectRandomProperty.City);
        }

        [When(@"user Save and Continue to Next Step")]
        public void WhenUserSaveAndContinueToNextStep()
        {
            SaveAndContinue.Click();
            Task.Delay(1000).Wait();

            if (!IsDuplicateWarningDisplayed) return;

            _propertyCreatedMessage = Session.Driver
                .FindElement(By.XPath("//div[@class='modal-body']/p[@id='dupWarningDynamic']/p[3]")).Text;
            Session.Driver.FindElement(By.XPath("//div[@class='modal-footer']/a[text()='No, Cancel']")).Click();
        }

        [When("user select's state")]
        public void WhenUserSelectsState()
        {
            Session.DropDownSelect(By.Id("state_id")).SelectByText(SelectRandomProperty.State);
        }

        private void SelectCa(int grantId)
        {
            Session.Driver.FindElement(By.XPath($".//input[@id='grantId' and @value='{grantId}']")).Click();
        }
    }
}