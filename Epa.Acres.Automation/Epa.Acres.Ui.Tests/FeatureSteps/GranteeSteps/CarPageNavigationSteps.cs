﻿using System;
using System.Linq;
using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.FeatureSteps.GranteeSteps
{
    [Binding]
    public sealed class CarPageNavigationSteps : PageBase
    {
        public CarPageNavigationSteps(SessionController session) : base(session)
        {
        }

        [Then(@"user click's Add Funding button")]
        public void ThenUserClickSAddFundingButton()
        {
            Session.WithId("cleanupFunding_add").Click();
        }

        [Then(@"user update Cleanup Activity Completion Date")]
        public void ThenUserUpdateCleanupActivityCompletionDate()
        {
            Session.WithId("cleanup_activity_completion_date").SendKeys(DateTime.Now.ToString("MM/dd/yyyy"));
        }

        [Then(@"user update Cleanup Activity Start Date in Environmental Cleanup Information")]
        public void ThenUserUpdateCleanupActivityStartDateInEnvironmentalCleanupInformation()
        {
            Session.WithId("cleanup_activity_start_date").SendKeys("04/08/2021");
        }

        [When(@"user click's Save and Continue to NEXT STEP")]
        public void WhenUserClickSaveAndContinueToNextSteP()
        {
            Session.WithXPath(".//input[@id='continue' and 'Save and Continue to NEXT STEP']").Click();
        }

        [When(@"user click's on enter data for (.*) Grant Type")]
        public void WhenUserClickSOnEnterDataForAssessmentGrantType(string grantType)
        {
            Session.WithXPath(".//div[@id='propertiesTable_filter']/label/input").SendKeys(grantType);

            var enterData =
                Session.WithXPaths(
                    $".//table[@id='propertiesTable']/tbody/tr/td[contains(., 'Open') and position()=4]/following-sibling::td[position()=3 and contains(.,'{grantType}')]/following-sibling::td/a");

            enterData[new Random().Next(enterData.Count())].Click();
        }
    }
}