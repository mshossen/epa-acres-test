﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Models;
using Epa.Acres.Core.Models.SpecflowStepModel;
using Epa.Acres.Data.Acres.Data;
using Epa.Acres.Data.Acres.Model;
using Epa.Acres.Ui.Tests.Base;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using FluentAssertions;
using FluentAssertions.Execution;
using Newtonsoft.Json;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Epa.Acres.Ui.Tests.FeatureSteps.GranteeSteps
{
    [Binding]
    public sealed class AddCooperativeAgreementToCarUserAccountSteps : PageBase
    {
        private readonly RefStateQuery _refState = new RefStateQuery();
        private CooperativeAgreementModel _caModel;
        private string _caNumber;

        private RefStateDto _state;

        public AddCooperativeAgreementToCarUserAccountSteps(SessionController session) : base(session)
        {
        }

        private IEnumerable<IWebElement> CaRows =>
            Session.WithXPaths(".//table[@id='coopAgreementTable']/tbody/tr");

        private IWebElement SelectFilters =>
            Session.WithXPath(".//a[@id='navbarDropdown' and contains(.,'Select Filters')]");

        [Given(@"user Navigates to Advanced Search page")]
        public void GivenUserNavigatesToAdvancedSearchPage()
        {
            Session.WithXPath(".//a[@href='/acres6/search/showSearchMain']").Click();
        }

        [Then(@"user Clear selected filters")]
        public void ThenUserClearSelectedFilters()
        {
            SelectFilters.Click();
            Session.WithXPath(".//a[@onclick='clearFilters();']").Click();
        }

        [Then(@"user populates CA number and select's state from previous selection")]
        public void ThenUserPopulatesCaNumberAndSelectStateFromPreviousSelection()
        {
            _caNumber = new RegexHelper(@"(?<=^..)(.*)", _caModel.CaNumber).GetMatchedPatter();

            _state = _refState.GetStateByAbbreviation(_caModel.State);

            Session.WithId("coop_agreement_number").SendKeys(_caNumber.ToString());
            Session.DropDownSelect(By.Id("state_id")).SelectByText(_state.LABEL_STATE_LONG);
            Session.WithXPath(".//button[contains(.,'Next')]").Click();
            if (IsAssociatedCooperativeAgreement()) return;

            Console.WriteLine("Confirm add ca");
            Session.WithXPath(".//button[text()='Confirm Cooperative Agreement']").Click();
        }

        [Then(@"user Select Filters")]
        public void ThenUserSelectFilters(Table table)
        {
            var advancedSearch = table.CreateSet<AdvancedSearch>();
            SelectFilters.Click();

            var advancedSearches = advancedSearch as AdvancedSearch[] ?? advancedSearch.ToArray();

            foreach (var item in advancedSearches)
                Session.MouseHoverClick($".//li[@class='advSearchItem' and contains(.,'{item.Filter}')]/input");

            var regions = advancedSearches.Select(r => r.Region).Distinct();
        }

        [Then(@"user Select from (.*) Status")]
        public void ThenUserSelectFromOpenStatus(string status)
        {
            Session.WithXPath(".//div[@id='statusItem']//button").Click();

            var preSelections = Session
                .WithXPaths(".//select[@id='grantStatusTypeLst']/following-sibling::div/ul/li/a/label/input")
                .Where(s => s.Selected).ToList();

            foreach (var remove in preSelections) remove.Click();

            var selectStatus =
                Session.WithXPath(
                    $".//select[@id='grantStatusTypeLst']/following-sibling::div/ul/li/a/label[contains(.,'{status}')]/input");

            if (selectStatus.Selected) return;
            selectStatus.Click();
        }

        [Then(@"user select Regions")]
        public void ThenUserSelectRegions(Table table)
        {
            var regions = table.CreateSet<AdvancedSearch>();
            Session.WithXPath(".//div[@id='regionItem']//button").Click();

            var preSelections = Session
                .WithXPaths(".//select[@id='regionIdLst']/following-sibling::div/ul/li/a/label/input", scroll: false)
                .Where(s => s.Selected).ToList();

            foreach (var remove in preSelections) remove.Click();

            var setRegions =
                Session.WithXPaths(".//select[@id='regionIdLst']/following-sibling::div/ul/li/a/label/input",
                    scroll: false);

            foreach (var item in regions.Select(r => r.Region).ToList())
            {
                var region = setRegions.FirstOrDefault(a => a.GetAttribute("value").EndsWith(item));
                if (region.Selected) return;
                region.Click();
            }
        }

        [Then(@"user verify CA has been added to list")]
        public void ThenUserVerifyCAHasBeenAddedToList()
        {
            var myCooperative = new List<CooperativeAgreementModel>();

            foreach (var item in CaRows)
                myCooperative.Add(new CooperativeAgreementModel
                {
                    CaName = item.FindElement(By.XPath(".//td[1]/a")).Text,
                    CaStatus = item.FindElement(By.XPath(".//td[3]")).Text,
                    CaNumber = item.FindElement(By.XPath(".//td[4]")).Text,
                    CaType = item.FindElement(By.XPath(".//td[5]")).Text,
                    AnnouncementYear = item.FindElement(By.XPath(".//td[6]")).Text,
                    State = item.FindElement(By.XPath(".//td[7]")).Text
                });

            var expected = myCooperative.FirstOrDefault(ca => ca.CaNumber.Contains(_caNumber));

            using (new AssertionScope())
            {
                expected.CaNumber.Should().Be(_caModel.CaNumber);
                expected.CaStatus.Should().Be(_caModel.CaStatus);
                expected.State.Should().Be(_caModel.State);
                expected.CaName.Should().Be(_caModel.CaName);
                expected.CaType.Should().Be(_caModel.CaType);
            }
        }

        [Then("user select's random CA from table")]
        public void UserSelectRandomCaFromTable()
        {
            var counter = 0;

            while (counter <= 5)
            {
                Task.Delay(1000).Wait();

                if (Session.WithXPaths(".//table[@id='searchResultsTableCA']/tbody/tr").Count() > 1) break;

                counter++;
            }

            var caDataRow = Session.WithXPaths(".//table[@id='searchResultsTableCA']/tbody/tr");
            var random = new Random().Next(0, caDataRow.Count());

            _caModel = new CooperativeAgreementModel
            {
                CaName = Session.WithXPath($".//table[@id='searchResultsTableCA']/tbody/tr[{random}]/td[1]/a").Text,
                CaStatus = Session.WithXPath($".//table[@id='searchResultsTableCA']/tbody/tr[{random}]/td[2]").Text,
                CaNumber = Session.WithXPath($".//table[@id='searchResultsTableCA']/tbody/tr[{random}]/td[3]").Text,
                State = Session.WithXPath($".//table[@id='searchResultsTableCA']/tbody/tr[{random}]/td[5]").Text,
                AnnouncementYear = Session.WithXPath($".//table[@id='searchResultsTableCA']/tbody/tr[{random}]/td[6]")
                    .Text,
                CaType = Session.WithXPath($".//table[@id='searchResultsTableCA']/tbody/tr[{random}]/td[7]").Text
            };

            Console.WriteLine(JsonConvert.SerializeObject(_caModel));
        }

        [When(@"user click's on View CA or TBAs")]
        public void WhenUserClickOnViewCAOrTBAs()
        {
            Session.WithXPath(".//a[@aria-controls='caSearchResults']").Click();
        }

        [When(@"user click's Apply Filter button")]
        public void WhenUserClickSApplyFilterButton()
        {
            Task.Run(() => Session.WithCss("#page-apply-filter").Click())
                .Wait(TimeSpan.FromSeconds(3));
        }

        private bool IsAssociatedCooperativeAgreement()
        {
            return Session.IsVisibleElement(By.XPath(
                ".//div[@role='alert' and contains(.,'You are already associated to this cooperative agreement')]"));
        }
    }
}