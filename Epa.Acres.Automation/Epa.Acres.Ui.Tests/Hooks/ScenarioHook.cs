﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using BoDi;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Models.TestResultsDtos;
using Epa.Acres.Core.ScenarioTag;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.Hooks
{
    [Binding]
    public sealed class ScenarioHook
    {
        private readonly SessionController _session;

        public ScenarioHook(IObjectContainer container)
        {
            _session = container.Resolve<SessionController>();
        }

        [BeforeScenario(
            new[]
            {
                FeatureTags.CarUser, FeatureTags.AddProperty, FeatureTags.AddCa, FeatureTags.Assessment,
                FeatureTags.Bcrlf, FeatureTags.Cleanup
            }, Order = 1)]
        //[Scope(Tag = "carUser")]
        public void LogInAsCarUser(FeatureContext context, ScenarioContext scenario)
        {
            _session.WithBrowser(BrowserType.Chrome)
                .CreateBrowserInstance()
                .Visit(_session.UserService.UserConfig.AcresUrl);

            _session.WithId("userName")
                .SendKeys(_session.UserService.UserConfig.CarUsers.First(e => e.Region.Equals(1010)).UserName);
            Thread.Sleep(500);
            _session.WithId("password").SendKeys(_session.UserService.UserConfig.Password);
            Thread.Sleep(500);
            _session.WithXPath(".//input[@value='login']").Click();

            if (scenario.ScenarioInfo?.Tags != null)
                _session.Features.InsertFeatureInfo(new FeatureInfoDto
                {
                    Title = context.FeatureInfo.Title,
                    Tags = string.Join(",", scenario.ScenarioInfo?.Tags),
                    Description = context.FeatureInfo?.Description,
                    ProgrammingLanguage = context.FeatureInfo.GenerationTargetLanguage.ToString()
                });
        }

        [BeforeScenario(new[] {FeatureTags.DataAdmin, FeatureTags.StvrpContaminants}, Order = 2)]
        public void LogInAsDataAdminUser(FeatureContext context, ScenarioContext scenario)
        {
            _session.WithBrowser(BrowserType.Chrome)
                .CreateBrowserInstance()
                .Visit(_session.UserService.UserConfig.AcresUrl);

            _session.WithId("userName").SendKeys(_session.UserService.UserConfig.AdminUser);
            Thread.Sleep(500);
            _session.WithId("password").SendKeys(_session.UserService.UserConfig.Password);
            Thread.Sleep(500);
            _session.WithXPath(".//input[@value='login']").Click();

            if (scenario.ScenarioInfo?.Tags != null)
                _session.Features.InsertFeatureInfo(new FeatureInfoDto
                {
                    Title = context.FeatureInfo.Title,
                    Tags = string.Join(",", scenario.ScenarioInfo?.Tags),
                    Description = context.FeatureInfo?.Description,
                    ProgrammingLanguage = context.FeatureInfo.GenerationTargetLanguage.ToString()
                });
        }

        [BeforeScenario(new[] {FeatureTags.EpaUser}, Order = 2)]
        // [Scope(Tag = "epaUser")]
        public void LogInAsEpaUser(FeatureContext context, ScenarioContext scenario)
        {
            _session.WithBrowser(BrowserType.Chrome)
                .CreateBrowserInstance()
                .Visit(_session.UserService.UserConfig.AcresUrl);

            _session.WithId("userName")
                .SendKeys(_session.UserService.UserConfig.EpaUsers.First(e => e.Region.Equals(1010)).UserName);
            Thread.Sleep(500);
            _session.WithId("password").SendKeys(_session.UserService.UserConfig.Password);
            Thread.Sleep(500);
            _session.WithXPath(".//input[@value='login']").Click();

            if (scenario.ScenarioInfo?.Tags != null)
                _session.Features.InsertFeatureInfo(new FeatureInfoDto
                {
                    Title = context.FeatureInfo.Title,
                    Tags = string.Join(",", scenario.ScenarioInfo?.Tags),
                    Description = context.FeatureInfo?.Description,
                    ProgrammingLanguage = context.FeatureInfo.GenerationTargetLanguage.ToString()
                });
        }

        [AfterScenario]
        public void TearDownBrowser(FeatureContext context, ScenarioContext scenario)
        {
            var featureInfo = _session.Features.GetFeatureTitle(context.FeatureInfo.Title);

            if (scenario.TestError is {InnerException: { }})
                _session.TeamsNotification
                    .WithEmailSubject()
                    .WithEmailBody(ReportBuilder(featureInfo, scenario.TestError.InnerException.ToString()))
                    .SendEmailReport()
                    .Dispose();

            _session?.Dispose();
            _session.OracleSql.CloseDbConnection();
        }

        private string ReportBuilder(FeatureInfoDto featureInfo, string testError = "")
        {
            var sb = new StringBuilder();
            sb.Append(@"<html>");
            sb.Append("<head>");

            sb.Append(
                "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">");

            sb.Append(
                "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">");

            sb.Append(
                "<link rel=\"stylesheet\" type=\"text / css\" href=\"https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css\">");
            sb.Append("<style>");
            //sb.Append("table {border-collapse: collapse; width: 100%;}");
            sb.Append("table { table-layout: fixed; width: 100%; }");
            sb.Append("table tr { border-bottom:1px solid #e9e9e9; }");
            //sb.Append("table thead td, th {border-left: 1px solid #f2f2f2; border-right: 1px solid #d5d5d5; background: #ddd url(".. / images / sprites4.png") repeat-x scroll 0 100% ; font-weight: bold; text-align:left;}");
            sb.Append("table tr td, th { border:1px solid #D5D5D5; padding:5px;}");
            sb.Append("table tr:hover { background:#fcfcfc;}");
            sb.Append("table tr ul.actions {margin: 0;}");
            sb.Append("table tr ul.actions li {display: inline; margin-right: 5px;}");
            sb.Append("img.resize { max-width: 35%; display: block; margin-left: auto; margin-right: auto; }");
            sb.Append("pre {");
            sb.Append("  background-color: #eee;");
            sb.Append("  border: 1px solid #999;");
            sb.Append("  display: block;");
            sb.Append("  padding: 20px;");
            sb.Append("}");
            sb.Append("</style>");
            sb.Append("</head>");
            sb.Append(@"<body>");
            sb.Append("<table class=\"table\">");
            sb.Append("<thead class=\"thead - dark\"><tr><th scope=\"col\"  title=\"Field #1\">Id</th>");
            sb.Append("<th scope=\"col\" title=\"Field #2\">Title</th>");
            sb.Append("<th scope=\"col\" title=\"Field #3\">Description</th>");
            sb.Append("<th scope=\"col\" title=\"Field #4\">Tags</th>");
            sb.Append("</tr></thead>");
            sb.Append("<tbody><tr>");
            sb.Append($"<td>{featureInfo.Id}</td>");
            sb.Append($"<td>{featureInfo.Title}</td>");
            sb.Append($"<td>{featureInfo.Description?.Replace("\t", "")}</td>");
            sb.Append($"<td>{featureInfo.Tags}</td>");
            sb.Append("</tr>");
            sb.Append("</tbody></table>");

            sb.Append(
                $"<br /> <p><strong>Total Execution Time:</strong> <time>{TimeSpan.FromMilliseconds(_session.ScenarioInfoDto.Select(t => t.StepCompleationTime).Sum()).ToString("c")}</time></p>");

            sb.Append("<table cellspacing=\"1\" cellpadding=\"0\" border=\"1.5\">");
            sb.Append("<thead>");
            sb.Append("<tr>");

            sb.Append("<th scope=\"col\">Status</th>");
            sb.Append("<th scope=\"col\">Step Definition Type</th>");
            sb.Append("<th scope=\"col\">Step Definition</th>");
            sb.Append("<th scope=\"col\">Step Compleation Time</th>");

            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            foreach (var item in _session.ScenarioInfoDto)
            {
                sb.Append($"<tr scope=\"row\">");
                sb.Append($"<td>{item.Status}</td>");
                sb.Append($"<td>{item.StepDefinitionType}</td>");
                sb.Append("<td>");
                sb.Append($"<div>{item.StepDefinition}</div>");

                if (!string.IsNullOrWhiteSpace(item.ScreenShot))
                    sb.Append($"<img src=\"data:image/png;base64,{item.ScreenShot}\" />");
                sb.Append("</td>");
                sb.Append($"<td>{TimeSpan.FromMilliseconds(item.StepCompleationTime):c}</td>");
                sb.Append($"</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            if (!string.IsNullOrWhiteSpace(testError))
            {
                sb.Append("<br />");
                sb.Append($"<pre>{testError}</pre>");
            }

            sb.Append("</body></html>");

            return sb.ToString();
        }
    }
}