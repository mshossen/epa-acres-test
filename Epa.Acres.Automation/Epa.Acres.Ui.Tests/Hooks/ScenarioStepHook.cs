﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using BoDi;
using Epa.Acres.Core.Models;
using Epa.Acres.Core.Models.TestResultsDtos;
using Epa.Acres.Ui.Tests.Controllers;
using Epa.Acres.Ui.Tests.Extensions;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Epa.Acres.Ui.Tests.Hooks
{
    [Binding]
    public class ScenarioStepHook
    {
        private readonly SessionController _session;
        private Stopwatch _watch;

        public ScenarioStepHook(IObjectContainer objectContainer)
        {
            _session = objectContainer.Resolve<SessionController>();
        }

        [AfterStep]
        public void AfterStep(ScenarioContext context, FeatureContext featureContext)
        {
            _watch.Stop();
            var completionTime = (int) _watch.Elapsed.TotalMilliseconds;

            var scenarioInfoDto = new ScenarioInfoDto
            {
                TestPlanId = _session.SessionId,
                ScenarioTitle = context.ScenarioInfo.Title,
                ExecutedDate = DateTime.Now,
                StepDefinitionType = context.StepContext.StepInfo.StepDefinitionType.ToString(),
                StepDefinition = context.StepContext.StepInfo.Text,
                StepCompleationTime = completionTime,
                Status = context.TestError is null
                    ? "Passed"
                    : "Failed",
                ErrorMessage = context.TestError?.Message,
                ScreenShot = context.TestError != null
                    ? _session.TakeScreenShotAsBase64String(ProjectSettings.ScreenShotDirectory)
                    : null
            };
            _session.ScenarioInfoDto.Add(scenarioInfoDto);

            _session.ScenarioLog.SaveScenarioInfo(scenarioInfoDto
                , featureContext.FeatureInfo.Title);

            if (context.TestError is null) return;

            var allScreenShotFiles = new DirectoryInfo(ProjectSettings.ScreenShotDirectory).GetFiles();

            TestContext.AddTestAttachment(allScreenShotFiles.OrderByDescending(f => f.LastWriteTime).First()
                .ToString());
        }

        [BeforeStep]
        public void BeforeStep(ScenarioContext context, FeatureContext featureContext)
        {
            _watch = new Stopwatch();
            _watch.Start();
        }
    }
}