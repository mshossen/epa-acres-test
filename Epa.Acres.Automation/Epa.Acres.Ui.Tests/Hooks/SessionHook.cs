﻿using System;
using System.IO;
using BoDi;
using Epa.Acres.Core.Models;
using Epa.Acres.Ui.Tests.Controllers;
using NUnit.Framework;
using TechTalk.SpecFlow;

[assembly: Parallelizable(ParallelScope.Fixtures)]
[assembly: LevelOfParallelism(7)]

namespace Epa.Acres.Ui.Tests.Hooks
{
    [Binding]
    public sealed class SessionHook
    {
        [BeforeTestRun]
        public static void OneTimeSetup(SessionController session, IObjectContainer container)
        {
        }

        [AfterTestRun]
        public static void OneTimeTearDown()
        {
            var files = Directory.GetFiles(ProjectSettings.ScreenShotDirectory + @"\");
            var timeNow = DateTime.Now;

            foreach (var file in files)
            {
                var lastAccessed = File.GetLastAccessTime(file);

                if (lastAccessed <= timeNow.AddDays(-7)) File.Delete(file);
            }
        }
    }
}