﻿using System;
using System.Collections.Generic;
using System.IO;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models;
using Epa.Acres.Core.Models.TestResultsDtos;
using Epa.Acres.Ui.Tests.Extensions;
using Epa.Acres.Ui.Tests.Settings;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace Epa.Acres.Ui.Tests.Controllers
{
    public class SessionController
    {
        private BrowserType _browser;

        protected SessionController()
        {
            ProjectSettings.AutoCreateDirectories();
            
            UserService = ContainerConfig.ConfigBuilder().Resolve<IUserService>();
            SqLiteLog = ContainerConfig.ConfigBuilder().Resolve<ISqLiteDataAccess>();
            Features = ContainerConfig.ConfigBuilder().Resolve<IFeatureInfoProcessor>();
            TestPlan = ContainerConfig.ConfigBuilder().Resolve<ITestPlanProcessor>();
            ScenarioLog = ContainerConfig.ConfigBuilder().Resolve<IScenarioInfoProcessor>();
            NewPropertyLog = ContainerConfig.ConfigBuilder().Resolve<INewPropertyProcessor>();
            OracleSql = ContainerConfig.ConfigBuilder().Resolve<IOracleDataAccess>();
            TeamsNotification = ContainerConfig.ConfigBuilder().Resolve<IReportingService>();
        }

        public Actions Actions => new Actions(Driver);
        public IWebDriver Driver { get; private set; }
        public IFeatureInfoProcessor Features { get; }
        public INewPropertyProcessor NewPropertyLog { get; }
        public IOracleDataAccess OracleSql { get; }

        public FileReader Reader => new FileReader()
        {
            FileExtension = FileExtension.txt
        };

        public List<ScenarioInfoDto> ScenarioInfoDto { get; set; } = new List<ScenarioInfoDto>();
        public IScenarioInfoProcessor ScenarioLog { get; }
        public string SessionId { get; private set; }
        private ISqLiteDataAccess SqLiteLog { get; }
        public IReportingService TeamsNotification { get; }
        private ITestPlanProcessor TestPlan { get; }
        public IUserService UserService { get; }

        public SessionController CreateBrowserInstance()
        {
            if (Driver is { }) return this;

            switch (_browser)
            {
                case BrowserType.Chrome:
                    new DriverManager().SetUpDriver(new ChromeConfig());

                    Driver = new ChromeDriver(ChromeCustomSettings.ChromeService(),
                        ChromeCustomSettings.ChromeOption());
                    break;

                case BrowserType.Firefox:
                    new DriverManager().SetUpDriver(new FirefoxConfig());

                    Driver = new FirefoxDriver(FirefoxCustomSettings.FirefoxService(),
                        FirefoxCustomSettings.FirefoxOption());
                    break;

                case BrowserType.Edge:
                    break;

                default:
                    throw new WebDriverException($"Browser {_browser} is not initiated!!");
            }

            this.MaximizeBrowser();
            var cap = (RemoteWebDriver) Driver;
            if (cap != null) SessionId = cap.SessionId.ToString();
            var capability = ((RemoteWebDriver) Driver)?.Capabilities.ToString();
            if (capability == null) return this;
            var testPlan = JsonConvert.DeserializeObject<TestPlanDto>(capability);

            if (testPlan != null)
                TestPlan.SaveTestPlan(new TestPlanDto
                {
                    TestPlanId = SessionId,
                    MachineName = Environment.MachineName,
                    UserName = Environment.UserName,
                    BuildNumber = "6",
                    AcceptInsecureCerts = testPlan is {AcceptInsecureCerts: true},
                    BrowserName = testPlan.BrowserName,
                    BrowserVersion = testPlan.BrowserVersion,
                    PageLoadStrategy = testPlan.PageLoadStrategy,
                    PlatformName = testPlan.PlatformName,
                    PlatformVersion = testPlan?.PlatformVersion ?? Environment.OSVersion.VersionString
                });

            return this;
        }

        public void Dispose()
        {
            Driver?.Close();
            Driver?.Quit();
        }

        public SessionController WithBrowser(BrowserType browser)
        {
            _browser = browser;
            return this;
        }
    }
}