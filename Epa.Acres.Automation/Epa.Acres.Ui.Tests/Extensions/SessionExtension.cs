﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using Epa.Acres.Core.Models;
using Epa.Acres.Ui.Tests.Controllers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace Epa.Acres.Ui.Tests.Extensions
{
    public static class SessionExtension
    {
        public static void AcceptAlert(this SessionController session, double seconds = 5, double milliSeconds = 250)
        {
            try
            {
                if (!session.HasAlertMessage(seconds, milliSeconds)) return;
                session.Driver.SwitchTo().Alert().Accept();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void DismissAlert(this SessionController session, double seconds = 5, double milliSeconds = 250)
        {
            try
            {
                if (!session.HasAlertMessage(seconds, milliSeconds)) return;
                session.Driver.SwitchTo().Alert().Dismiss();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static SelectElement DropDownSelect(this SessionController session, By locator, double minute = 1,
            double milliSeconds = 250, bool scroll = true)
        {
            return new SelectElement(session.WithActiveElement(locator, minute, milliSeconds, scroll));
        }

        public static void ElementTimeOut(this SessionController session)
        {
            session.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        public static string GetAlertMessage(this SessionController session, double seconds = 5,
            double milliSeconds = 250)
        {
            try
            {
                return !session.HasAlertMessage(seconds, milliSeconds)
                    ? string.Empty
                    : session.Driver.SwitchTo().Alert().Text;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static void GoBack(this SessionController session)
        {
            session.Driver?.Navigate().Back();
        }

        public static void GoForward(this SessionController session)
        {
            session.Driver?.Navigate().Forward();
        }

        private static bool HasAlertMessage(this SessionController session, double seconds = 5,
            double milliSeconds = 250)
        {
            var wait = new DefaultWait<IWebDriver>(session.Driver)
            {
                Timeout = TimeSpan.FromSeconds(seconds),
                PollingInterval = TimeSpan.FromMilliseconds(milliSeconds),
                Message = $"Checked if alert displayed for {seconds} second(s)"
            };

            return wait.Until(s =>
            {
                try
                {
                    if (s.SwitchTo().Alert() != null) return true;
                }
                catch (NoAlertPresentException)
                {
                    return false;
                }

                return false;
            });
        }

        public static bool IsVisibleElement(this SessionController session, By locator)
        {
            try
            {
                return session.Driver.FindElement(locator).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public static void MaximizeBrowser(this SessionController session)
        {
            session.Driver?.Manage().Window.Maximize();
        }

        public static void MouseHover(this SessionController session, string xpath)
        {
            new Actions(session.Driver).MoveToElement(session.WithXPath(xpath, scroll: false)).Build().Perform();
        }

        public static void MouseHover(this SessionController session, By locator)
        {
            new Actions(session.Driver).MoveToElement(session.WithActiveElement(locator, scroll: false)).Build()
                .Perform();
        }

        public static void MouseHoverClick(this SessionController session, string xpath)
        {
            new Actions(session.Driver).MoveToElement(session.WithXPath(xpath, scroll: false)).Click().Build()
                .Perform();
        }

        public static void MouseHoverClick(this SessionController session, IWebElement element)
        {
            var action = new Actions(session.Driver);
            action.MoveToElement(element).Perform();
            action.Click().Build().Perform();
        }

        public static void MouseHoverClick(this SessionController session, By locator)
        {
            new Actions(session.Driver).MoveToElement(session.WithActiveElement(locator, scroll: false)).Click().Build()
                .Perform();
        }

        public static void PageRefresh(this SessionController session)
        {
            session.Driver?.Navigate().Refresh();
        }

        public static void PageTimeOut(this SessionController session)
        {
            session.Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
        }

        public static void ScrollToElement(this SessionController session, IWebElement element, bool scroll = false,
            int offsetX = 0, int offsetY = 0)
        {
            if (!scroll) return;

            var actions = new Actions(session.Driver);

            if ((offsetX | offsetY) > 0)
            {
                actions.MoveToElement(element, offsetX, offsetY).Perform();
                return;
            }

            actions.MoveToElement(element).Perform();
        }

        public static void SwitchToDefaultContent(this SessionController session)
        {
            session.Driver?.SwitchTo().DefaultContent();
        }

        public static void SwitchToIFrame(this SessionController session, IWebElement element)
        {
            Thread.Sleep(250);
            session?.Driver.SwitchTo().Frame(element);
            Thread.Sleep(250);
        }

        public static void SwitchToParent(this SessionController session)
        {
            var windows = session.Driver?.WindowHandles;

            if (windows == null) return;

            for (var i = windows.Count - 1; i > 0;)
            {
                session.Driver?.Close();
                i -= 1;
                Thread.Sleep(2000);
                session.Driver?.SwitchTo().Window(windows[i]);
            }

            session.Driver?.SwitchTo().Window(windows[0]);
        }

        public static void SwitchToWindow(this SessionController session, int index = 0)
        {
            Thread.Sleep(1500);
            var windows = session.Driver?.WindowHandles;

            if (windows != null && windows.Count - 1 < index)
                throw new NoSuchWindowException($"Invalid Browser Window Index Count {index}");

            if (windows != null) session.Driver?.SwitchTo().Window(windows[index]);
            Thread.Sleep(1000);
            session.MaximizeBrowser();
        }

        public static string TakeScreenShot(this SessionController session,
            string path = ProjectSettings.ScreenShotDirectory)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            var random = new Random().Next(250, 1000);
            Thread.Sleep(random);
            var screenShot = ((ITakesScreenshot) session.Driver).GetScreenshot();
            var png = @$"{path}\Error-{session.SessionId}-{DateTime.Now:yyyyMMddHHmmsss}.png";

            screenShot.SaveAsFile(png, ScreenshotImageFormat.Png);
            return png;
        }

        public static string TakeScreenShotAsBase64String(this SessionController session,
            string path = ProjectSettings.ScreenShotDirectory)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            var random = new Random().Next(250, 1000);
            Thread.Sleep(random);
            var screenShot = ((ITakesScreenshot) session.Driver).GetScreenshot();
            var png = @$"{path}\Error-{session.SessionId}-{DateTime.Now:yyyyMMddHHmmsss}.png";

            screenShot.SaveAsFile(png, ScreenshotImageFormat.Png);
            return screenShot.AsBase64EncodedString;
        }

        public static void Visit(this SessionController session, string url)
        {
            session.Driver?.Navigate().GoToUrl(url);
        }

        private static IWebElement WithActiveElement(this SessionController session, By locator, double minute = 1,
            double milliSeconds = 250, bool scroll = true, int xCoordinate = 0, int yCoordinate = 0)
        {
            var wait = new DefaultWait<IWebDriver>(session.Driver)
            {
                Timeout = TimeSpan.FromMinutes(minute),
                PollingInterval = TimeSpan.FromMilliseconds(milliSeconds),
                Message = $"Unable to locate element {locator}, timed out after minute(s): {minute}"
            };
            wait.IgnoreExceptionTypes(new[] {typeof(NoSuchElementException), typeof(ElementNotVisibleException)});

            return wait.Until(e =>
            {
                var element = e.FindElement(locator);

                if (!element.Displayed && !element.Enabled) throw new NoSuchElementException(wait.Message);
                session.ScrollToElement(element, scroll, xCoordinate, yCoordinate);

                ((IJavaScriptExecutor) e).ExecuteScript("arguments[0].style.background='#33FFBD'", element);
                return element;
            });
        }

        public static ReadOnlyCollection<IWebElement> WithActiveElements(this SessionController session, By locator,
            double minute = 1, double milliSeconds = 250, bool scroll = true, int xCoordinate = 0, int yCoordinate = 0)
        {
            var wait = new DefaultWait<IWebDriver>(session.Driver)
            {
                Timeout = TimeSpan.FromMinutes(minute),
                PollingInterval = TimeSpan.FromMilliseconds(milliSeconds),
                Message = $"Unable to locate element {locator}, timed out after minute(s): {minute}"
            };
            wait.IgnoreExceptionTypes(new[] {typeof(NoSuchElementException), typeof(ElementNotVisibleException)});

            return wait.Until(e =>
            {
                var elements = e.FindElements(locator);

                if (elements.Any(x => x.Displayed || x.Enabled) || elements.Count > 0)
                {
                    session.ScrollToElement(elements.FirstOrDefault(), scroll, xCoordinate, yCoordinate);
                    return elements;
                }

                throw new NoSuchElementException(wait.Message);
            });
        }

        public static IWebElement WithCss(this SessionController session, string css, double minute = 1,
            double milliSeconds = 250, bool scroll = true, int xCoordinate = 0, int yCoordinate = 0)
        {
            return session.WithActiveElement(By.CssSelector(css), minute, milliSeconds, scroll, xCoordinate,
                yCoordinate);
        }

        public static ReadOnlyCollection<IWebElement> WithCssSelectors(this SessionController session,
            string cssSelectors, double minute = 1, double milliSeconds = 250, bool scroll = true, int xCoordinate = 0,
            int yCoordinate = 0)
        {
            return session.WithActiveElements(By.CssSelector(cssSelectors), minute, milliSeconds, scroll, xCoordinate,
                yCoordinate);
        }

        public static IWebElement WithId(this SessionController session, string id, double minute = 1,
            double milliSeconds = 250, bool scroll = true, int xCoordinate = 0, int yCoordinate = 0)
        {
            return session.WithActiveElement(By.Id(id), minute, milliSeconds, scroll, xCoordinate, yCoordinate);
        }

        public static IWebElement WithXPath(this SessionController session, string xpath, double minute = 1,
            double milliSeconds = 250, bool scroll = true, int xCoordinate = 0, int yCoordinate = 0)
        {
            return session.WithActiveElement(By.XPath(xpath), minute, milliSeconds, scroll, xCoordinate, yCoordinate);
        }

        public static ReadOnlyCollection<IWebElement> WithXPaths(this SessionController session, string xpath,
            double minute = 1, double milliSeconds = 250, bool scroll = true, int xCoordinate = 0, int yCoordinate = 0)
        {
            return session.WithActiveElements(By.XPath(xpath), minute, milliSeconds, scroll, xCoordinate, yCoordinate);
        }
    }
}