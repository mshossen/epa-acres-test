﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Epa.Acres.Ui.Tests.Settings
{
    public static class FirefoxCustomSettings
    {
        public static FirefoxOptions FirefoxOption()
        {
            return new FirefoxOptions
            {
                LogLevel = FirefoxDriverLogLevel.Default,
                AcceptInsecureCertificates = true,
                PageLoadStrategy = PageLoadStrategy.Eager
            };
        }

        public static FirefoxDriverService FirefoxService()
        {
            var service = FirefoxDriverService.CreateDefaultService();
            // service.Port = 50091;
            return service;
        }
    }
}