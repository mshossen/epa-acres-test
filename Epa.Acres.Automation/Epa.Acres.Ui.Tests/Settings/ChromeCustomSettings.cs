﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Epa.Acres.Ui.Tests.Settings
{
    public class ChromeCustomSettings
    {
        public static ChromeOptions ChromeOption()
        {
            var options = new ChromeOptions
            {
                AcceptInsecureCertificates = true,
                PageLoadStrategy = PageLoadStrategy.Eager
            };

            //options.AddArgument("no-sandbox");
            //options.AddArgument("--allowed-ips 127.0.0.1");
            //options.AddArgument("--whitelisted-ips 0.0.0.0");

            return options;
        }

        public static ChromeDriverService ChromeService()
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.LogPath = $@"C:\LinTechProjects\Log\Chrome{DateTime.Now:yyyyMMddHHmmss}.log";
            // service.Port = 59180;
            //service.EnableVerboseLogging = false;
            return service;
        }
    }
}