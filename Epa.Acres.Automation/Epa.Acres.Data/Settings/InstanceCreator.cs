﻿using System;
using System.Collections.Generic;
using Epa.Acres.Data.Acres.Data;

namespace Epa.Acres.Data.Settings
{
    /// <summary>
    ///   Creates instance for Oracle Query classes Registers base classes then registers the sub classes
    /// </summary>
    public static class InstanceCreator
    {
        private static readonly Dictionary<Type, OracleQueryBase> CreateSubClassDictionary =
            new Dictionary<Type, OracleQueryBase>();

        private static readonly object InternalDictionaryLock = new object();

        public static T GetDbQueryClass<T>() where T : OracleQueryBase
        {
            lock (InternalDictionaryLock)
            {
                return InternalGetQueryClass<T>();
            }
        }

        private static T InternalGetQueryClass<T>() where T : OracleQueryBase
        {
            var key = typeof(T);
            if (CreateSubClassDictionary.TryGetValue(key, out var result)) return (T) result;

            var newQuery = (T) Activator.CreateInstance(typeof(T));
            CreateSubClassDictionary.Add(key, newQuery);

            return newQuery;
        }
    }
}