﻿using System.Linq;
using Epa.Acres.Core.Helper;
using Epa.Acres.Data.Acres.Model;

namespace Epa.Acres.Data.Acres.Data
{
    public class GrantsQuery : OracleQueryBase
    {
        public GrantsQuery()
        {
        }

        public GrantsDto GetGrantWithCaNumber(string caNumber)
        {
            var ca = caNumber;
            if (ca.Length > 8 && ca.Length.Equals(10)) ca = new RegexHelper(@"^..(.*)", caNumber).GetMatchedPatter();

            var grant = OracleDb.LoadDataRows<GrantsDto, dynamic>(
                "Select * From GRANTS WHERE coop_agreement_number = :CaNumber", new {CaNumber = ca});
            return grant.First();
        }

        // public List<GrantPropertyDto>
    }
}