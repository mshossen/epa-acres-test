﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Epa.Acres.Data.Acres.Model;

namespace Epa.Acres.Data.Acres.Data
{
    public class GrantPropertyQuery : OracleQueryBase
    {
        private readonly GrantsQuery _grant;

        public GrantPropertyQuery()
        {
            _grant = new GrantsQuery();
        }

        public List<GrantPropertyDto> GetAllProperty()
        {
            return OracleDb.LoadDataAsync<GrantPropertyDto>(
                    @"Select property_id, property_name, latitude_measure, longitude_measure from GRANT_PROPERTY where flag_lat_lng_override != 'Y' or (flag_lat_lng_override is null or flag_lat_lng_override = 'N') order by flag_lat_lng_override ASC")
                .Result;
        }

        public GrantPropertyFinanceAccomplishmentDto GetPropertyAccomplishmentWithCa(string caNumber)
        {
            var grantPropertyFinanceAccomplishments = new GrantPropertyFinanceAccomplishmentDto();

            var grantId = _grant.GetGrantWithCaNumber(caNumber).GRANT_ID;

            grantPropertyFinanceAccomplishments.GpaFinancial = OracleDb.LoadDataRows<GpaFinancialDto, dynamic>(
                "Select gpaf.gpa_id, gpaf.gpf_source_type_id, (Select label_gpf_source_type From REF_GPF_SOURCE_TYPE where gpf_source_type_id = gpaf.gpf_source_type_id) AS label_gpf_source_type, gpaf.funding_entity_name, gpaf.gpf_amount, gpaf.gpf_date, gpaf.activity_funded from GPA_FINANCIAL gpaf inner join GRANT_PROPERTY_ACCOMPLISHMENT gpa on gpa.gpa_id = gpaf.gpa_id WHERE gpa.grant_id = :gId ",
                new {gId = grantId});

            var gpFinanceSql = $"Select * from GRANT_PROPERTY_ACCOMPLISHMENT where grant_id  = {grantId}";

            grantPropertyFinanceAccomplishments.GrantPropertyAccomplishments =
                OracleDb.LoadDataAsync<GrantPropertyAccomplishmentDto>(gpFinanceSql).Result;
            ;

            // grantPropertyFinanceAccomplishments.GrantPropertyAccomplishments.AddRange(gpAccomlishtment); grantPropertyFinanceAccomplishments.GpaFinancial.AddRange(gpaFinancials.Result);

            return grantPropertyFinanceAccomplishments;
        }

        public async Task<int> GetTotalProperties()
        {
            return await OracleDb.GetTableRowCount("GRANT_PROPERTY");
        }
    }
}