﻿using System;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;

namespace Epa.Acres.Data.Acres.Data
{
    public abstract class OracleQueryBase : IDisposable
    {
        protected OracleQueryBase()
        {
            OracleDb = ContainerConfig.ConfigBuilder().Resolve<IOracleDataAccess>();
        }

        protected IOracleDataAccess OracleDb { get; private set; }

        public void Dispose()
        {
            OracleDb.CloseDbConnection();
        }
    }
}