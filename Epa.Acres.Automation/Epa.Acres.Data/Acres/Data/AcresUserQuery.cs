﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Data.Acres.Model;

namespace Epa.Acres.Data.Acres.Data
{
    public class AcresUserQuery : IDisposable
    {
        private readonly IOracleDataAccess _oracleDb;

        public AcresUserQuery()
        {
            _oracleDb = ContainerConfig.ConfigBuilder().Resolve<IOracleDataAccess>();
        }

        public void Dispose()
        {
            _oracleDb.CloseDbConnection();
        }

        public IList<UsersDto> GetActiveUsers(string userName)
        {
            return _oracleDb
                .LoadDataAsync<UsersDto, dynamic>(
                    $@"SELECT
                          *
                      FROM(
                      Select * from users
                      where password != NULL or Length(trim(password)) !=0
                      and flag_active_user = 'Y'
                      order BY dbms_random.random
                      )
                      where rownum < 19
                      and password = (Select password from users where user_name =:name" + ")", new {name = userName}
                ).Result.ToList();
        }

        public UsersDto GetByUserName(string username)
        {
            return _oracleDb
                .LoadDataAsync<UsersDto, dynamic>("Select * from USERS WHERE USER_NAME = :name", new {name = username})
                .Result.FirstOrDefault();
        }
    }
}