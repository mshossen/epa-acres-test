﻿using System.Collections.Generic;
using System.Linq;
using Epa.Acres.Data.Acres.Model;

namespace Epa.Acres.Data.Acres.Data
{
    public class RefStateQuery : OracleQueryBase
    {
        public RefStateQuery()
        {
        }

        public RefStateDto GetStateByAbbreviation(string abbreviation)
        {
            return GetAllRefState().FirstOrDefault(s => s.LABEL_STATE.Equals(abbreviation));
        }

        public IList<RefStateDto> GetStatesByRegionId(int regionId)
        {
            return GetAllRefState().Where(s => s.REGION_ID.Equals(regionId)).ToList();
        }

        public IList<RefStateDto> GetStatesByUserName(string username)
        {
            var user = new AcresUserQuery().GetByUserName(username);
            var regionId = GetAllRefState().FirstOrDefault(s => s.STATE_ID.Equals(user.STATE_ID))!.REGION_ID;

            return GetStatesByRegionId(regionId);
        }

        private IEnumerable<RefStateDto> GetAllRefState()
        {
            return OracleDb.LoadDataAsync<RefStateDto>(@"select * from ref_state").Result;
        }
    }
}