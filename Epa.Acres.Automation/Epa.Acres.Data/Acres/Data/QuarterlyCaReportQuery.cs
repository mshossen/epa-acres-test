﻿using System.Collections.Generic;
using System.Linq;
using Epa.Acres.Data.Acres.Model;

namespace Epa.Acres.Data.Acres.Data
{
    public class QuarterlyCaReportQuery : OracleQueryBase
    {
        private readonly GrantsQuery _grant;

        public QuarterlyCaReportQuery()
        {
            _grant = new GrantsQuery();
        }

        public List<QrTaskDto> GetProjectProgramTask(string caNumber)
        {
            var grant = _grant.GetGrantWithCaNumber(caNumber);
            var qrReport = GetQrReportWithGrantId(grant.GRANT_ID);

            var qrTask = OracleDb.LoadDataRows<QrTaskDto, dynamic>(@"SELECT * FROM QR_TASKS where QR_ID = :qrId",
                new {qrId = qrReport.QR_ID});

            return qrTask;
        }

        public QrReportsDto GetQrReportWithGrantId(int id)
        {
            return OracleDb
                .LoadDataAsync<QrReportsDto, dynamic>(@"Select * From qr_reports where grant_id = :grantId",
                    new {grantId = id}).Result.FirstOrDefault();
        }
    }
}