﻿namespace Epa.Acres.Data.Acres.Model
{
    public class RefStateDto
    {
        public int FIPS_ST { get; set; }
        public string LABEL_STATE { get; set; }
        public string LABEL_STATE_LONG { get; set; }
        public int NUMBER_OF_DISTRICTS { get; set; }
        public int REGION_ID { get; set; }
        public int STATE_ID { get; set; }
    }
}