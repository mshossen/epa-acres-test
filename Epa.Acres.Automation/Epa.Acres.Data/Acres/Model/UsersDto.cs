﻿namespace Epa.Acres.Data.Acres.Model
{
    public class UsersDto
    {
        public string ACRES_TOKEN { get; set; }
        public string CITY { get; set; }
        public string First_Name { get; set; }
        public string FLAG_ACTIVE_USER { get; set; }
        public string Last_Name { get; set; }
        public string Password { get; set; }
        public int STATE_ID { get; set; }
        public int USER_ID { get; set; }
        public string USER_Name { get; set; }
        public string ZIP_CODE { get; set; }
    }
}