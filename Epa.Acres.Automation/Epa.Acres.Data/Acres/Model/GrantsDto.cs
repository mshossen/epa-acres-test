﻿using System;

namespace Epa.Acres.Data.Acres.Model
{
    public class GrantsDto
    {
        public DateTime ADD_DATE { get; set; }
        public int ALL_OTHER_EXPENDED { get; set; }
        public int ALL_OTHER_OBLIGATED { get; set; }
        public string ALL_REPORTS_RECEIVED { get; set; }
        public DateTime ANNOUNCE_DATE { get; set; }
        public int AREA_POPULATION { get; set; }
        public int AWARD_AMOUNT_CUMULATIVE { get; set; }
        public string AWARD_CLASS_CODE { get; set; }
        public DateTime AWARD_DATE { get; set; }
        public int AWARD_ID { get; set; }
        public int BE_INSURANCE_EXPENDED { get; set; }
        public int BE_INSURANCE_OBLIGATED { get; set; }
        public int BH_HEALTH_EXPENDED { get; set; }
        public int BH_HEALTH_OBLIGATED { get; set; }
        public int BH_INST_CONTROLS_EXPENDED { get; set; }
        public int BH_INST_CONTROLS_OBLIGATED { get; set; }
        public int BH_LOCAL_GOV_EXPENDED { get; set; }
        public int BH_LOCAL_GOV_OBLIGATED { get; set; }
        public int BS_RLF_SUBGRANTS_EXPENDED { get; set; }
        public int BS_RLF_SUBGRANTS_OBLIGATED { get; set; }
        public DateTime CLOSEOUT_DATE { get; set; }
        public int CLOSEOUT_EXPLAIN_ID { get; set; }
        public string CLOSEOUT_EXPLAIN_OTHER { get; set; }
        public string CLOSEOUT_REPORT_RECEIVED { get; set; }
        public DateTime CLOSEOUT_REPORT_RECEIVED_DATE { get; set; }
        public int CLOSING_PROJECT_OFFICER_ID { get; set; }
        public string COOP_AGREEMENT_NUMBER { get; set; }
        public string COOP_AGREEMENT_SUMMARY { get; set; }
        public string COST_SHARE_HARDSHIP_WAIVER { get; set; }
        public DateTime CURRENT_PROJECT_PERIOD_END { get; set; }
        public string DCN { get; set; }
        public string DCN_SUPPLEMENTAL { get; set; }
        public string DUNS { get; set; }
        public int ENTITY_TYPE_ID { get; set; }
        public string FLAG_CLOSEOUT_YN { get; set; }
        public string FLAG_EPA_UPDATE { get; set; }
        public string FLAG_PPG { get; set; }
        public string FLAG_PREPOP_AUTH { get; set; }
        public int FUND_LIMIT_WAIVER_AMOUNT { get; set; }
        public string FUND_LIMIT_WAIVER_APPROVED { get; set; }
        public int GRANT_ID { get; set; }
        public string GRANT_PROJECT_NAME { get; set; }
        public int GRANT_STATUS_TYPE_ID { get; set; }
        public string GRANT_TYPE_ID { get; set; }
        public DateTime INVENTORY_COMPLETION_DATE { get; set; }
        public string INVENTORY_PLANNED { get; set; }
        public DateTime INVENTORY_START_DATE { get; set; }
        public string LARGER_CLEANUP { get; set; }
        public string LESSONS_LEARNED { get; set; }
        public string MISSING_REPORTS { get; set; }
        public DateTime MOD_DATE { get; set; }
        public string MOD_USER { get; set; }
        public string OTHER_ENTITY_DESCRIPTION { get; set; }
        public string OTHER_GRANT_TYPE { get; set; }
        public int P_AREA_MINORITY { get; set; }
        public int P_AREA_POVERTY { get; set; }
        public int P_AREA_UNEMPLOYMENT { get; set; }
        public string P_CA_RECIEVED_HQ { get; set; }
        public string P_LEGISLATIVE_AUTHORITY { get; set; }
        public string P_RECIPIENT_ADDRESS1 { get; set; }
        public string P_RECIPIENT_ADDRESS2 { get; set; }
        public string P_RECIPIENT_CITY { get; set; }
        public string P_RECIPIENT_COUNTY { get; set; }
        public string P_RECIPIENT_ORGANIZATION { get; set; }
        public string P_RECIPIENT_ZIP_CODE { get; set; }
        public int P_TARGET_MINORITY { get; set; }
        public int P_TARGET_POVERTY { get; set; }
        public int P_TARGET_UNEMPLOYMENT { get; set; }
        public string P_URL { get; set; }
        public DateTime P_WORKPLAN_DATE { get; set; }
        public string P_WORKPLAN_RECIEVED { get; set; }
        public DateTime PREPOP_AUTH_DATE { get; set; }
        public string PROGRAM_CODE { get; set; }
        public DateTime PROJECT_PERIOD_END { get; set; }
        public DateTime PROJECT_PERIOD_START { get; set; }
        public string RECIPIENT_NAME { get; set; }
        public string RLF_COALITION_PARTNERS { get; set; }
        public string SHOWCASE_INDICATOR { get; set; }
        public int STATE_ID { get; set; }
        public int TARGET_POPULATION { get; set; }
    }
}