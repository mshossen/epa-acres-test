﻿using System.Collections.Generic;

namespace Epa.Acres.Data.Acres.Model
{
    /// <summary>
    ///   get or set values for GRANT_PROPERTY_ACCOMPLISHMENT table and GPA_FINANCIAL
    /// </summary>
    public class GrantPropertyFinanceAccomplishmentDto
    {
        public List<GpaFinancialDto> GpaFinancial { get; set; }
        public List<GrantPropertyAccomplishmentDto> GrantPropertyAccomplishments { get; set; }
    }
}