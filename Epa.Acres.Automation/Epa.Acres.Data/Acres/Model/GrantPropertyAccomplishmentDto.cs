﻿using System;

namespace Epa.Acres.Data.Acres.Model
{
    /// <summary>
    ///   mapping to GRANT_PROPERTY_ACCOMPLISHMENT
    /// </summary>
    public class GrantPropertyAccomplishmentDto
    {
        public DateTime? ADD_DATE { get; set; }
        public DateTime? ENVPROFCERT_ISSUE_DATE { get; set; }
        public DateTime? FCA_DATE { get; set; }
        public DateTime? FCA_DATE_DRAFT { get; set; }
        public string FCA_FY { get; set; }
        public string FCA_FY_DRAFT { get; set; }
        public string FLAG_COUNT_ACCOMP_DRAFT { get; set; }
        public string FLAG_COUNT_ACCOMPLISHMENT { get; set; }
        public string FLAG_DELETE { get; set; }
        public string FLAG_ENVPROFCERT_DOC { get; set; }
        public string FLAG_NFA_DOC { get; set; }
        public string FLAG_OTHER_FORMS_OF_DOC { get; set; }
        public decimal? GPA_AMOUNT { get; set; }
        public DateTime? GPA_COMPLETION_DATE { get; set; }
        public int GPA_ID { get; set; }
        public string GPA_NOTE { get; set; }
        public DateTime? GPA_START_DATE { get; set; }
        public int GPA_TYPE_ID { get; set; }
        public int GRANT_ID { get; set; }
        public DateTime? MOD_DATE { get; set; }
        public string MOD_USER { get; set; }
        public string OTHER_FORMS_OF_DOC { get; set; }
        public int PROPERTY_ID { get; set; }
    }
}