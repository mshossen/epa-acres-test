﻿using System;

namespace Epa.Acres.Data.Acres.Model
{
    /// <summary>
    ///   Mapping to GPA_FINANCIAL tabel
    /// </summary>
    public class GpaFinancialDto
    {
        public string ACTIVITY_FUNDED { get; set; }
        public string FUNDING_ENTITY_NAME { get; set; }
        public int GPA_ID { get; set; }
        public double GPF_AMOUNT { get; set; }
        public DateTime? GPF_DATE { get; set; }
        public int GPF_SOURCE_TYPE_ID { get; set; }
        public string LABEL_GPF_SOURCE_TYPE { get; set; }
    }
}