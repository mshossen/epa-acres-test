﻿namespace Epa.Acres.Data.Acres.Model
{
    public class QrTaskDto
    {
        public string DESCRIPTION { get; set; }
        public string GRTASK_CODE { get; set; }
        public string GTSTATUS_CODE { get; set; }
        public int QR_ID { get; set; }
        public int QRTASK_ID { get; set; }
        public string QRTASK_NAME { get; set; }
        public int SORT_ORDER { get; set; }
    }
}