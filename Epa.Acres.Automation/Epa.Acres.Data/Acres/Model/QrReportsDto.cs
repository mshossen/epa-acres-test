﻿using System;
using Epa.Acres.Core.Enums;

namespace Epa.Acres.Data.Acres.Model
{
    public class QrReportsDto
    {
        public DateTime ACCEPTED_BY_PO_DATE { get; set; }
        public int APPROVED_BY { get; set; }
        public string CAR_COMMENTS { get; set; }
        public int CREATED_BY { get; set; }
        public string FLAG_TASK_BASED_FIN { get; set; }
        public int GRANT_ID { get; set; }
        public string PO_COMMENTS { get; set; }
        public int QR_ID { get; set; }
        public QrStatusCode QR_STATUS_CODE { get; set; }
        public string QRTYPE_CODE { get; set; }
        public int REPORT_QUARTER { get; set; }
        public int REPORT_YEAR { get; set; }
        public DateTime SUBMIT_TO_EPA_DATE { get; set; }
        public int SUBMITTED_BY { get; set; }
    }
}