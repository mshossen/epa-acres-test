﻿using System;

namespace Epa.Acres.Data.Acres.Model
{
    [Serializable]
    public class GrantPropertyDto
    {
        public string LATITUDE_MEASURE { get; set; }
        public string LONGITUDE_MEASURE { get; set; }
        public int PROPERTY_ID { get; set; }
        public string PROPERTY_NAME { get; set; }
    }
}