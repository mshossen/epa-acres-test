﻿using System;

namespace Epa.Acres.Data.Acres.Model
{
    public class QrPropertyDataDto
    {
        public int PROPERTY_ID { get; set; }
        public int QR_ID { get; set; }
        public string QRPD_CODE { get; set; }
        public DateTime VALUE_DATE { get; set; }
        public int VALUE_NUMERIC { get; set; }
        public string VALUE_VARCHAR2 { get; set; }
    }
}