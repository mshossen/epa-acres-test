﻿using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;

namespace Epa.Acres.Data.Reports
{
    public class TestPlanData
    {
        private ISqLiteDataAccess _sqLite;

        public TestPlanData()
        {
            _sqLite = ContainerConfig.ConfigBuilder().Resolve<ISqLiteDataAccess>();
        }
    }
}