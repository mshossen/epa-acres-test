﻿using System;
using System.Collections.Generic;
using System.Linq;
using Epa.Acres.Core.Enums;
using Epa.Acres.Core.Extensions;
using Epa.Acres.Core.Models.Reports;

namespace Epa.Acres.Data.MockData
{
    /// <summary>
    ///   Mock Data https://medium.com/@nidhishps/generate-fake-data-for-testing-in-c-sharp-2825593072a3
    /// </summary>
    public class QuarterlyReportMock
    {
        public static List<string> BudgetAndOverallProjectStatus { get; } = new List<string>
        {
            RandomString(250),
            RandomString(100),
            RandomString(50),
            RandomString(10),
            RandomString(40),
            RandomString(30),
            RandomString(200),
            RandomString(300)
        };

        public static List<string> CostShareAndLeveragingInformation { get; } = new List<string>
        {
            RandomString(250),
            RandomString(330),
            RandomString(100),
            RandomString(200),
            RandomString(150)
        };

        public static List<string> FederalCrossCutters { get; } = new List<string>
        {
            RandomString(250),
            RandomString(50),
            RandomString(120),
            RandomString(200),
            RandomString(238)
        };

        public static IList<ProjectProgressModel> ProgressTasks =>
            new List<ProjectProgressModel>
            {
                new ProjectProgressModel
                {
                    Id = 1,
                    Tasks = "Community Building",
                    QuarterProgressSummary =
                        "Projects that intentionally bring people together to simply get to know one another. Examples include the Morris Community Meal.",
                    Status = ProgressStatus.Completed.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 2,
                    Tasks = "Community Education",
                    QuarterProgressSummary =
                        "Projects that provide instructional services or curricula, or serve to educate the public about a social issue (in a non-partisan way). Examples include the TREC program, Community ESL classes, and Gather in the Park.",
                    Status = ProgressStatus.InProgress.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 3,
                    Tasks = "Community Organizing",
                    QuarterProgressSummary =
                        "Projects that bring people together with the goal of solving a community issue. Please note that OCE cannot work on partisan issues, but can contribute to creating solutions for non-partisan, local issues. An example is assistance in establishing the Latino Parent Advisory Board for the local school.",
                    Status = ProgressStatus.InProgress.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 4,
                    Tasks = "Deliberative Dialogue",
                    QuarterProgressSummary =
                        "Projects that intentionally bring people together to build understanding across differences.",
                    Status = ProgressStatus.NotStarted.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 5,
                    Tasks = "Direct Service",
                    QuarterProgressSummary =
                        "Projects that provide a service or product to an individual, group, or the community as a whole. Examples include filling a volunteer shift at a local organization, creating social media tools for an organization, or creating a community mural.",
                    Status = ProgressStatus.Completed.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 6,
                    Tasks = "Economic Development",
                    QuarterProgressSummary =
                        "Projects that work on developing the regional economy in a sustainable way. Note that OCE does not partner with for-profit entities except when doing so would benefit the community as a whole and not the for-profit only organization. Examples include feasibility studies for new businesses and projects that attract people to small town business disricts.",
                    Status = ProgressStatus.NotStarted.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 7,
                    Tasks = "Engaged Research",
                    QuarterProgressSummary =
                        "Research that directly benefits the community by clarifying the causes of a community challenge, mapping a community's assets, or contributing to solutions to current challenges and also fits a faculty member's research agenda. In the best case scenario, faculty with research expertise work alongside community members and students on such projects. An example would be the MIEI community needing assessment.",
                    Status = ProgressStatus.InProgress.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 8,
                    Tasks = "Institutional Engagement",
                    QuarterProgressSummary =
                        "University resources intentionally offered without undue barriers to the community. OCE can play a role in envisioning institutional engagement efforts. Examples include making Briggs library cards available for community members, making campus events accessible, and choosing to use local and sustainable businesses to supply services or goods.",
                    Status = ProgressStatus.NotStarted.GetDescription()
                },
                new ProjectProgressModel
                {
                    Id = 9,
                    Tasks = "Sponsor Partnerships",
                    QuarterProgressSummary =
                        "We don't protect the environment on our own.  We work with businesses, non-profit organizations, and state and local governments through dozens of partnerships. A few examples include conserving water and energy, minimizing greenhouse gases, re-using solid waste, and getting a handle on pesticide risks. In return, we share information and publicly recognize our partners.",
                    Status = ProgressStatus.NotStarted.GetDescription()
                }
                //new ProjectProgressModel
                //{
                //  Id = 10,
                //  Tasks = "Test With an 'phes",
                //  QuarterProgressSummary = "To ensure that system's behavior is working with apostrophe save. So that we can ensure that there is nothing broken.",
                //  Status = ProgressStatus.NotStarted.GetDescription()
                //},
                //new ProjectProgressModel
                //{
                //  Id = 11,
                //  Tasks = "Test with a % sign",
                //  QuarterProgressSummary = "To ensure that system's behavior is working with % sign on save. So that we can ensure that there is nothing broken.",
                //  Status = ProgressStatus.NotStarted.GetDescription()
                //},
                //new ProjectProgressModel
                //{
                //  Id = 12,
                //  Tasks = "Test with a \"",
                //  QuarterProgressSummary = "To ensure that system’s behavior is working with \" sign on save. So that we can ensure that there is nothing broken.",
                //  Status = ProgressStatus.InProgress.GetDescription()
                //},
                //new ProjectProgressModel
                //{
                //  Id = 13,
                //  Tasks = "Test with (",
                //  QuarterProgressSummary = "To ensure that system’s behavior is working with ( sign on save. So that we can ensure that there is nothing broken.",
                //  Status = ProgressStatus.Completed.GetDescription()
                //}
            };

        public static string RandomDate
        {
            get
            {
                var start = new DateTime(2009, 1, 1);
                var range = (DateTime.Today - start).Days;
                return start.AddDays(new Random().Next(range)).ToString("MM/dd/yyyy");
            }
        }

        public static double RandomFinanceData
        {
            get
            {
                var random = new Random();
                var financeMax = 1000000;
                //var financeMin = -1000000;
                var financeMin = 0;

                return Math.Round(random.NextDouble() * random.Next(financeMin, financeMax), 2);
            }
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.; ";

            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[new Random().Next(s.Length)])
                .ToArray());
        }
    }
}