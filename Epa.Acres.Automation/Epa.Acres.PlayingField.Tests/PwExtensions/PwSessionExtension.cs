﻿using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.Settings;
using Microsoft.Playwright;

namespace Epa.Acres.PlayingField.Tests.PwExtensions
{
    public static class PwSessionExtension
    {
        private const float MaxTimeout = 30000;

        public static async Task<SessionContext> LocatorAsync(this SessionContext sessionContext, string locator)
        {
            var element = sessionContext.Page.Locator(locator);

            await element.WaitForAsync(!(await element.IsEnabledAsync()
                                         || await element.IsVisibleAsync())
                ? new LocatorWaitForOptions
                {
                    Timeout = MaxTimeout,
                    State = WaitForSelectorState.Visible | WaitForSelectorState.Attached
                }
                : null);

            sessionContext.Locator = element;
            //System.Console.WriteLine(JsonConvert.SerializeObject(new
            //{
            //	//IsChecked = await element?.IsCheckedAsync(),
            //	//IsEnabled = await element?.IsEnabledAsync(),
            //	//IsVisble = await element?.IsVisibleAsync(),
            //	//InnerHtml = await element?.InnerHTMLAsync(),
            //	//InnerText = await element?.InnerTextAsync(),
            //	//InputValue = await element?.InputValueAsync(),
            //	//IsHidden = await element?.IsHiddenAsync(),

            //	Element = sessionContext?.Locator.ToString(),
            //	ElementClass = sessionContext.Locator.GetAttributeAsync("class")?.Result,
            //	ElementName = sessionContext.Locator.GetAttributeAsync("name")?.Result,
            //	ElementType = sessionContext.Locator.GetAttributeAsync("type")?.Result,
            //	ElementId = sessionContext.Locator.GetAttributeAsync("id")?.Result,
            //	ElementAllInnerText = sessionContext.Locator.AllInnerTextsAsync().Result,
            //	ElementAllInnerContent = sessionContext.Locator.AllTextContentsAsync().Result,
            //	ElementScreenShot = sessionContext.Locator.ScreenshotAsync(new LocatorScreenshotOptions
            //	{
            //		Path = @$"C:\LinTechProjects\PlaywrightReport\screenshot\{sessionContext?.Locator.ElementHandleAsync().Id.ToString().Replace("@", "_")}.jpeg",
            //		Type = ScreenshotType.Jpeg
            //	}).Result

            //}, Formatting.Indented));
            return sessionContext;
        }

        public static async Task FillWithText(this SessionContext sessionContext, string text)
        {
            await sessionContext.Locator.FillAsync(text, new LocatorFillOptions
            {
                Force = true,
                Timeout = MaxTimeout
            });
        }

        public static async Task LeftClickAsync(this SessionContext sessionContext)
        {
            await sessionContext.Locator.ClickAsync(new LocatorClickOptions
            {
                Button = MouseButton.Left,
                Force = true,
                Timeout = MaxTimeout
            });
        }
    }
}