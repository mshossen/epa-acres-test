﻿using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.Pages;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;
using FluentAssertions;
using Models.TestCategory;
using NUnit.Framework;

namespace Epa.Acres.PlayingField.Tests.Assessments.CarTests
{
    [TestFixtureSource(typeof(BrowserRunner))]
    public sealed class AddCaToCarAccountTests : CarBase<AddCaToCarAccountPage>
    {
        public AddCaToCarAccountTests(string browser) : base(browser)
        {
        }

        [Test(Description = "Navigate to Add Cooperative Agreement Page")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.AddCa)]
        [Order(0)]
        public async Task NavigateToAddCaPage()
        {
            await CurrentPage.GotoPage();
        }


        [Test(Description = "Add Cooperative Agreement to CAR account")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.AddCa)]
        [Order(1)]
        public async Task AddCaToCarAccount()
        {
            var h2Text = await Context.LocatorAsync("//h2");

            h2Text.Locator.InnerTextAsync().Result.Should().Be("Add Cooperative Agreement");
        }
    }
}