﻿using System.Globalization;
using System.Threading.Tasks;
using Epa.Acres.Core.Extensions;
using Epa.Acres.Core.Helper;
using Epa.Acres.Core.Models;
using Epa.Acres.Core.Processors;
using Epa.Acres.PlayingField.Tests.Pages;
using Epa.Acres.PlayingField.Tests.Pages.GlobalPages;
using Epa.Acres.PlayingField.Tests.Settings;
using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.Playwright;
using Models.AcresPropertyModels;
using Models.RestApiModel;
using Models.TestCategory;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.PlayingField.Tests.Assessments.CarTests.PropertyTests
{
    [TestFixtureSource(typeof(BrowserRunner))]
    public sealed class CreateNewAssessmentPropertyTests : CarBase<AddNewPropertyPage>
    {
        private string _caType;
        private MockPropertyModel _propertiesInfo;
        private EnvironmentAssessmentInfoModel _environmentInfo;

        private Grant Grant { get; set; }
        private PpfEnvironmentalActivitiesPage PpfEnvironmentalActivitiesPage { get; set; }

        public CreateNewAssessmentPropertyTests(string browser) : base(browser)
        {
        }

        [Test(Description = "CAR user navigates to Add Property Page")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(0)]
        public async Task TestStep001_NavigateToAddPropertyPage()
        {
            await CurrentPage.GotoPage();

            Context.Page.Url
                .Should()
                .EndWith("viewMode=addProperty");
        }

        [TestCase("Assessment", Description = "CAR user narrow's CA type to Assessment")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(1)]
        public async Task TestStep002_NarrowCASearchResultToAssessment(string catype)
        {
            _caType = catype;

            await CurrentPage.NarrowResults(catype);
            var caTypes = CurrentPage.GetCaTypeFromNarrowResult();

            caTypes.Should().AllBe(catype);
        }

        [TestCase("Open", Description = "Select a random Open Status CA")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(2)]
        public async Task TestStep003_SelectARandomOpenCA(string caStatus)
        {
            await CurrentPage.SelectRandomCa(caStatus);
        }

        [Test(Description = "Click on apply filter and click on create New Property button")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(3)]
        public async Task TestStep004_ApplyFilterAndCreateNewProperty()
        {
            Grant = CurrentPage.ClickApplyFilter().Result;
            await CurrentPage.ClickCreateNewProperty();

            var grantSchema = JsonConvert.SerializeObject(Grant, Formatting.Indented);

            await TestContext.Out.WriteLineAsync(grantSchema);

            using (new AssertionScope())
            {
                Grant.RefGrantType.LabelGfsDisplay
                    .Should().Be(_caType);

                JsonConvert.SerializeObject(Grant.RefState)
                    .IsValidJson<RefState>().Should().BeTrue();
            }
        }

        [Test(Description = "Fillin property information")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(4)]
        public async Task TestStep005_FillInPropertyInfromation()
        {
            _propertiesInfo = new MockPropertyDataProcessor().SelectARandomProperty(Grant.RefState.LabelState);

            await TestContext.Out.WriteLineAsync(JsonConvert.SerializeObject(_propertiesInfo, Formatting.Indented));

            await CurrentPage.FillPropertyInfo(_propertiesInfo,
                new TimestampToDateHelper(Grant.AwardDate).Year);

            System.Console.WriteLine(Context.Page.Url);
        }

        [Test(Description = "Click Save and Continue To Next Step PPF II: Environment Information")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(5)]
        public async Task TestStep006_ClickSaveAndContinueToNextStep()
        {
            PpfEnvironmentalActivitiesPage = await CurrentPage.PpfContinueNextStep(Grant);
        }

        [Test(Description = "Then I enter PPF Environment Assessment Information")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(6)]
        public async Task TestStep007_EnterPpfEnvironmentAssessmentInformation()
        {
            _environmentInfo = new EnvironmentAssessmentInfoModel();
            await TestContext.Out.WriteLineAsync(JsonConvert.SerializeObject(_environmentInfo, Formatting.Indented));

            await PpfEnvironmentalActivitiesPage.AddAssessmentActivity(_environmentInfo);

            var assessFundingTotal = PpfEnvironmentalActivitiesPage.AssessmentFundingTotal();

            var epaFund =
                _environmentInfo.SourceOfFunding.Equals("US EPA - Brownfields Assessment Cooperative Agreement") &&
                _environmentInfo.AmountOfFundingExpended < 0.00d
                    ? -1 * _environmentInfo.AmountOfFundingExpended
                    : _environmentInfo.AmountOfFundingExpended;

            // Assertion
            using (new AssertionScope())
            {
                assessFundingTotal.TotalFunding
                    .Should()
                    .Be(_environmentInfo.AmountOfFundingExpended < 0.00d
                        ? -1 * _environmentInfo.AmountOfFundingExpended
                        : _environmentInfo.AmountOfFundingExpended);
            }
        }

        [Test(Description = "Then I confirm that Cleanup is Necessary")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(7)]
        public async Task TestStep008_ThenIConfirmThatCleanupIsNecessary()
        {
            await PpfEnvironmentalActivitiesPage.IsCleanupNecessary();
        }

        [Test(Description = "Click All that Applied for Class of Contaminants")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(8)]
        public async Task TestStep009_ClickAllThatAppliedForClassOfContaminants()
        {
            await PpfEnvironmentalActivitiesPage.ClickAllAppliedContaminants();
        }

        [Test(Description = "Click All Affected Media")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(9)]
        public async Task TestStep010_ClickAllAffectedMedia()
        {
            await PpfEnvironmentalActivitiesPage.ClickAllMediaAffected();
        }

        [Test(Description = "Click Save and Continue NEXT STEP to PPF Part III: Additional Property Information")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(10)]
        public async Task TestStep011_ClickSaveAndContinueToTraditionalPropertyInformation()
        {
            await CurrentPage.PpfContinueNextStep(Grant);

            var saveAlert = await CurrentPage.CheckEnvironmentActivitiesInformationSaved();
            await saveAlert.WaitForElementStateAsync(ElementState.Visible);

            using (new AssertionScope())
            {
                saveAlert.InnerTextAsync().Result.Should()
                    .Contain("Successfully updated Environmental Activities Information.");

                saveAlert.IsVisibleAsync().Result.Should().BeTrue();
            }
        }

        [Test(Description = "Property Redevelopment start and completion dates are entered")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(11)]
        public async Task TestStep012_Confirm_RedevelopmentStartAndCompletionDates_AreSaved()
        {
            var rlfSource = new RedevelopmentLeverageFundingSource();
            await TestContext.Out.WriteLineAsync(JsonConvert.SerializeObject(rlfSource, Formatting.Indented));

            await PpfEnvironmentalActivitiesPage.FillRedevelopmentStartAndCompletionDates(_environmentInfo.StartDate,
                _environmentInfo.CompleationDate);

            await PpfEnvironmentalActivitiesPage.AddRedevelopmentLeverageFunding(rlfSource);

            var leverageFundingTotal = await PpfEnvironmentalActivitiesPage.LeverageFundingTotals();

            using (new AssertionScope())
            {
                double.Parse(leverageFundingTotal, NumberStyles.Currency)
                    .Should().Be(rlfSource.AmountFundingExpended);
            }
        }

        [Test(Description = "Click Save and Continue to NEXT STEP to Review & Submit")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(12)]
        public async Task TestStep013_Confirm_WorkPackageReviewAndSubmitPage_IsShown()
        {
            await CurrentPage.PpfContinueNextStep(Grant);
            //await Context.Page.WaitForURLAsync($"**/acres6/jquery/ppf/ppfReviewAndSubmit?action=&grantId={Grant.GrantId}**", new PageWaitForURLOptions
            //{
            //  Timeout = 5000,
            //  WaitUntil = WaitUntilState.Load
            //});
        }

        [Test(Description = "Review and Submit Assessment Work Package")]
        [Category(CategoryTags.CarUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Assessment)]
        [Order(13)]
        public async Task TestStep014_ReviewAndSubmitWorkPackage()
        {
            await CurrentPage.ReviewAndSubmit();

            var propertyStatus = await CurrentPage.WorkPackageStatus(_propertiesInfo.PropertyName, Grant.GrantId);

            using (new AssertionScope())
            {
                propertyStatus
                    .Should().Be("Submitted");
            }
        }
    }
}