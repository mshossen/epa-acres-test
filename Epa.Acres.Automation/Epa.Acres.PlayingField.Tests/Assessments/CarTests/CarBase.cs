﻿using System.Linq;
using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.Pages;
using Epa.Acres.PlayingField.Tests.Pages.GlobalPages;
using Epa.Acres.PlayingField.Tests.Settings;
using NUnit.Framework;

namespace Epa.Acres.PlayingField.Tests.Assessments.CarTests
{
    public abstract class CarBase<TP> : BaseTest<TP> where TP : BasePage
    {
        public CarBase(string browser) : base(browser)
        {
            CurrentPage = PageFactory.BuildPage<TP>(Context);
        }

        [OneTimeSetUp]
        [Order(1)]
        public async Task LoginAsCarUser()
        {
            await PageFactory.BuildPage<LoginPage>(Context)
                .Login(Context.UserService.UserConfig.CarUsers.First(e => e.Region.Equals(1010)).UserName,
                    Context.UserService.UserConfig.Password);
            //await CurrentPage.GotoPage();
        }
    }
}