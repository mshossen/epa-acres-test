﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;
using Microsoft.Playwright;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

[assembly: LevelOfParallelism(7)]
[assembly: Parallelizable(ParallelScope.Fixtures)]

namespace Epa.Acres.PlayingField.Tests.Assessments
{
    [SetUpFixture]
    public abstract class BaseTest<TPage>
    {
        protected readonly SessionContext Context = new SessionContext();
        private static readonly string TimeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");

        protected TPage CurrentPage;
        private Stopwatch _watch;
        private string _path;

        public BaseTest(string browser)
        {
            Context.RunOnBrowser = browser;
        }

        [OneTimeSetUp]
        [Order(0)]
        public async Task BrowserSetup()
        {
            Context.Playwright = await Playwright.CreateAsync();

            switch (Context.RunOnBrowser.ToLower())
            {
                case "chrome":
                case "msedge":
                    Context.Browser = await Context
                        .Playwright
                        .Chromium
                        .LaunchAsync(new BrowserTypeLaunchOptions
                        {
                            Headless = false,
                            Channel = Context.RunOnBrowser.ToLower(),
                            // Args = new[] {
                            // //  "--start-fullscreen",  
                            //  // "--start-maximized" 
                            ////  "--window-size=1080,920"
                            // }
                            SlowMo = 99
                        });

                    Context.BrowserContext = await Context
                        .Browser
                        .NewContextAsync(
                            new BrowserNewContextOptions
                            {
                                IgnoreHTTPSErrors = true
                            }
                        );

                    Context.Page = Context.BrowserContext.NewPageAsync().Result;
                    break;
                case "firefox":
                    Context.Browser = await Context
                        .Playwright
                        .Firefox
                        .LaunchAsync(
                            new BrowserTypeLaunchOptions
                            {
                                Headless = false,
                                SlowMo = 99
                            });

                    Context.BrowserContext = await Context
                        .Browser
                        .NewContextAsync(
                            new BrowserNewContextOptions {IgnoreHTTPSErrors = true}
                        );

                    //Context.Page = await

                    Context.Page = await Context.Browser.NewPageAsync(new BrowserNewPageOptions
                    {
                        IgnoreHTTPSErrors = true,
                        RecordVideoDir = Context.SaveReportPath()
                    });
                    break;
            }

            await Context.BrowserContext.Tracing.StartAsync(new TracingStartOptions
            {
                Screenshots = true,
                Snapshots = true
            });
        }

        [OneTimeTearDown]
        public async Task OneTimeTearDown()
        {
            await Context.BrowserContext.Tracing.StopAsync(
                new TracingStopOptions
                {
                    Path = Context.SaveReportPath() +
                           @$"\{Context.TestId}-{Context.TestMethod}-{Guid.NewGuid().ToString()[..8]}.zip"
                });

            await Context.LocatorAsync("//a[@href='/acres6/processLogout']").Result.LeftClickAsync();
            //		await Context.BrowserContext?.CloseAsync();
            //		await Context?.Page.CloseAsync();
            await Context.Browser?.CloseAsync()!;
        }

        [SetUp]
        [Order(0)]
        public void SetExecutionTimer()
        {
            _watch = new Stopwatch();
            _watch.Start();
        }

        [TearDown]
        [Order(0)]
        public void TearDown()
        {
            _watch.Stop();
            var currentContext = TestContext.CurrentContext;
            //string path;
            // Console.WriteLine(JsonConvert.SerializeObject(currentContext, Formatting.Indented));

            Context.TestMethod = currentContext.Test.MethodName;
            Context.TestId = currentContext.Test.ID;

            //  Console.WriteLine(JsonConvert.SerializeObject(currentContext.Test.Properties["Description"]));

            TestContext.Out.WriteLine(
                $"ID: {currentContext.Test.ID} | Execution Time: {_watch.Elapsed} |Test Status: {currentContext.Result.Outcome.Status} | Test Method: {Context.TestMethod} | Description: {currentContext.Test.Properties["Description"].First()}");
            if (currentContext.Result.Outcome.Status != TestStatus.Failed) return;
            _path = GetScreenShotAsync(currentContext.Test.Name, currentContext.Result.Outcome.Status);
            TestContext.AddTestAttachment(_path);
        }

        private string GetScreenShotAsync(string testName, TestStatus status)
        {
            var path = @$"C:\LinTechProjects\PlaywrightReport\screenshot\{TimeStamp}";
            if (status != TestStatus.Failed) return string.Empty;
            Directory.CreateDirectory(path);

            var name = $@"{path}\{testName}.jpeg";
            File.WriteAllBytes(name, Context.Page.ScreenshotAsync().Result);
            Thread.Sleep(500);
            Console.WriteLine(name);
            return name;
        }
    }
}