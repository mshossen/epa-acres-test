﻿using System.Threading.Tasks;
using DataGeneration.Par;
using Epa.Acres.PlayingField.Tests.Pages.EpaPages;
using Epa.Acres.PlayingField.Tests.Settings;
using FluentAssertions;
using Models.TestCategory;
using Models.TestDataModel;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.PlayingField.Tests.Assessments.EpaTests
{
    [TestFixtureSource(typeof(BrowserRunner))]
    internal class ProgramAccomplishmentReportTests : EpaBase<ProgramAccomplishmentReportPage>
    {
        private static readonly ProgramAccomplishmentReportModel
            AccomplishmentReport = ParData.CreateRegionTenParData();

        public ProgramAccomplishmentReportTests(string browser) : base(browser)
        {
        }

        [Test(Description = "Navigate to Add Cooperative Agreement Page")]
        [Category(CategoryTags.EpaUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Par)]
        [Order(0)]
        public async Task TestStep001_ProgramAccomplishmentReportPage_IsVisible()
        {
            await CurrentPage.GotoPage();
            var isValidPage = await CurrentPage.IsProgramAccomplishmentReport();

            isValidPage.Should().BeTrue();
        }

        [Test(Description = "Select random Accomplishment type(s)")]
        [Category(CategoryTags.EpaUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Par)]
        [Order(1)]
        public async Task TestStep002_SelectRandomAccomplishmentType()
        {
            System.Console.WriteLine(JsonConvert.SerializeObject(AccomplishmentReport));
            await CurrentPage.SelectAccomplishmentType(AccomplishmentReport);
        }

        [Test(Description = "Select from available Cooperative Agreement Type(s)")]
        [Category(CategoryTags.EpaUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Par)]
        [Order(2)]
        public async Task TestStep003_SelectCaType()
        {
            await CurrentPage.SelectCaType(AccomplishmentReport);
        }


        [Test(Description = "Select a random FY within the last 3 years")]
        [Category(CategoryTags.EpaUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Par)]
        [Order(3)]
        public async Task TestStep004_SelectFiscalYear()
        {
            await CurrentPage.SelectFiscalYear(AccomplishmentReport.Year);
        }

        [Test(Description = "Select region ten state")]
        [Category(CategoryTags.EpaUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Par)]
        [Order(4)]
        public async Task TaskStep005_SelectLocationByRegion()
        {
            await CurrentPage.SelectLocationRegionAndState(AccomplishmentReport.Region, AccomplishmentReport.State);
        }

        [Test(Description = "Run Par Report")]
        [Category(CategoryTags.EpaUser)]
        [Category(CategoryTags.Playwright)]
        [Category(CategoryTags.Par)]
        [Order(5)]
        public async Task TestStep006_RunParReport()
        {
            await CurrentPage.RunReport();
        }
    }
}