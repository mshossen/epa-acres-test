﻿using System;
using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;

namespace Epa.Acres.PlayingField.Tests.Pages.GlobalPages
{
    public class LoginPage : BasePage
    {
        public LoginPage(SessionContext context) : base(context)
        {
        }

        public async Task<HomePage> Login(string username, string password)
        {
            await Context.Page.GotoAsync(Context.UserService.UserConfig.AcresUrl);
            await Context.Page.WaitForTimeoutAsync(new Random().Next(250, 2500));

            await Context.LocatorAsync("#userName").Result.FillWithText(username);
            await Context.LocatorAsync("#password").Result.FillWithText(password);
            await Context.LocatorAsync("//input[@value='login']").Result.LeftClickAsync();
            return new HomePage(Context);
        }
    }
}