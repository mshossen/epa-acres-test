﻿using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.Settings;

namespace Epa.Acres.PlayingField.Tests.Pages.GlobalPages
{
    public sealed class HomePage : BasePage
    {
        public HomePage(SessionContext context) : base(context)
        {
        }

        public async Task GotAddPropertyNav()
        {
            await Context.Page.HoverAsync("#menu_admin_viewAdminModule");
        }
    }
}