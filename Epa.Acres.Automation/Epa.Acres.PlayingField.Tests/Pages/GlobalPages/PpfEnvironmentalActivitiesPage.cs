﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Epa.Acres.Core.Extensions;
using Epa.Acres.Core.Helper;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;
using Microsoft.Playwright;
using Models.AcresPropertyModels;

namespace Epa.Acres.PlayingField.Tests.Pages.GlobalPages
{
    public sealed class PpfEnvironmentalActivitiesPage : AddNewPropertyPage
    {
        private IReadOnlyList<IElementHandle> _redevelopmentDays;

        private IReadOnlyList<IElementHandle> CalenderDays { get; set; }

        public PpfEnvironmentalActivitiesPage(SessionContext context) : base(context)
        {
        }

        public async Task<PpfEnvironmentalActivitiesPage> AddAssessmentActivity(
            EnvironmentAssessmentInfoModel environmentAssessmentInfo)
        {
            await ClickAddAssessmentActivity();
            await SelectAnActivityType(environmentAssessmentInfo.Activity);
            await FillStartDate(environmentAssessmentInfo.StartDate);
            await FillCompletionDate(environmentAssessmentInfo.CompleationDate);
            await SelectSourceOfFunding(environmentAssessmentInfo.SourceOfFunding);

            if (!environmentAssessmentInfo.NameOfEntityProvidingFunds.ToLower().Contains("epa") &&
                !environmentAssessmentInfo.SourceOfFunding.ToLower().Contains("epa"))
            {
                await Context.LocatorAsync("#funding_entity_name").Result
                    .FillWithText(environmentAssessmentInfo.NameOfEntityProvidingFunds);

                await Context.LocatorAsync("#activity_funded").Result
                    .FillWithText(environmentAssessmentInfo.ActivityFunded);
            }

            var gpfAmount = await Context.Page.QuerySelectorAsync("#gpf_amount");

            if (gpfAmount == null)
                throw new NoNullAllowedException();

            await gpfAmount.FillAsync(
                environmentAssessmentInfo.AmountOfFundingExpended.ToString(CultureInfo.InvariantCulture),
                new ElementHandleFillOptions
                {
                    Force = true
                });

            await Context.LocatorAsync("#saveActivity").Result.LeftClickAsync();

            await Context.Page.WaitForTimeoutAsync(500);

            if (environmentAssessmentInfo.AmountOfFundingExpended < 0.00d)
                Context.Page.Dialog += async (_, dialog) =>
                {
                    if (dialog.Message.Equals(
                            "Please provide a numeric value greater than 0 for Amount of Funding") ||
                        dialog.Message.Equals("Please enter Assessment Activity Start Date"))
                    {
                        await dialog.AcceptAsync();
                        await FillStartDate(environmentAssessmentInfo.StartDate);
                        await FillCompletionDate(environmentAssessmentInfo.CompleationDate);
                        await gpfAmount.EvaluateAsync("e => e.value =\"\"");

                        await Context.Page.ClickAsync("#saveActivity", new PageClickOptions
                        {
                            Force = true
                        });
                    }
                    else
                    {
                        throw new Exception(
                            $"Message is showing: {dialog.Message} \n instead of numeric value grater than 0");
                    }
                };

            return this;
        }

        public AssessmentFundingTotalModel AssessmentFundingTotal()
        {
            return new AssessmentFundingTotalModel
            {
                EpaFundingTotal = double.Parse(TotalEpaFunding().Result.InnerTextAsync().Result, NumberStyles.Currency),
                LeveragedFundingTotal = double.Parse(TotalLeveragedFunding().Result.InnerTextAsync().Result,
                    NumberStyles.Currency),
                TotalFunding = double.Parse(TotalAssessmentFunding().Result.InnerTextAsync().Result,
                    NumberStyles.Currency)
            };
        }

        private async Task<IElementHandle> TotalEpaFunding()
        {
            return await Context.Page.QuerySelectorAsync("#TotalAssessEPAFunding");
        }

        private async Task<IElementHandle> TotalLeveragedFunding()
        {
            return await Context.Page.QuerySelectorAsync("#TotalAssessLevgFunding");
        }

        private async Task<IElementHandle> TotalAssessmentFunding()
        {
            return await Context.Page.QuerySelectorAsync("#TotalAssessFunding");
        }

        private async Task ClickAddAssessmentActivity()
        {
            await Context.Page.WaitForURLAsync("**/acres6/jquery/ppf/ppfEnvironmentalActivities?**", UrlWaitOptions());
            await Context.Page.ClickAsync("#assessment_activity_add");
        }

        private async Task SelectAnActivityType(string activity)
        {
            await Context.Page.SelectOptionAsync("#gpa_type_id", new SelectOptionValue
            {
                Label = activity
            }, new PageSelectOptionOptions {Timeout = 500, NoWaitAfter = false});
        }

        private async Task FillStartDate(string startDate)
        {
            var startEpoch = new TimestampToDateHelper(startDate).LongEpoch;

            await Context.LocatorAsync("#gpa_start_date").Result.FillWithText(startDate);

            CalenderDays = await Context.Page.QuerySelectorAllAsync($"table>tbody>tr>td[data-date='{startEpoch}']");

            if (CalenderDays.Count <= 0)
            {
                var previous =
                    await Context.LocatorAsync("//div[@class='datepicker-days']/table/thead/tr/th[@class='prev']");
                await previous.Locator.HoverAsync();

                await previous.Locator.ClickAsync(new LocatorClickOptions
                {
                    Button = MouseButton.Left,
                    ClickCount = 1,
                    Force = true
                });

                CalenderDays = await Context.Page.QuerySelectorAllAsync($"table>tbody>tr>td[data-date='{startEpoch}']");

                var previousMonth = CalenderDays.LastOrDefault();
                if (previousMonth == null) return;
                await previousMonth.HoverAsync();
                await previousMonth.ClickAsync();

                return;
            }

            var earliestDay = CalenderDays.FirstOrDefault();

            if (earliestDay != null)
            {
                await earliestDay.HoverAsync();
                await earliestDay.ClickAsync();
            }
        }

        private async Task FillCompletionDate(string completionDate)
        {
            await Context.Page.FillAsync("#gpa_completion_date",
                completionDate,
                new PageFillOptions
                {
                    Force = true,
                    Strict = true
                });

            var calenderDays =
                await Context.Page.QuerySelectorAllAsync(
                    $"table>tbody>tr>td[data-date='{new TimestampToDateHelper(completionDate).LongEpoch}']");


            var earliestDay = calenderDays.LastOrDefault();

            if (earliestDay != null)
            {
                await earliestDay.HoverAsync();
                await earliestDay.ClickAsync();
            }
        }

        public async Task IsCleanupNecessary()
        {
            var cleanUpRadioButtons = await Context.Page.QuerySelectorAllAsync("#cleanupRadio");

            await cleanUpRadioButtons[0].ClickAsync(new ElementHandleClickOptions
            {
                Force = true
            });
        }

        public async Task ClickAllAppliedContaminants()
        {
            // Get all the Class of Contaminant column
            var contaminants = await Context.Page.QuerySelectorAllAsync("(//table)[3]/tbody/tr/td[1]");

            // Select all the name for Class of Contaminant and skip the last 3
            var names = contaminants
                .SkipLast(3)
                .Select(c => c.InnerTextAsync().Result)
                .ToList();

            foreach (var name in names)
            {
                // Get all the checkbox associated with contaminant name
                var selectContaminants =
                    await Context.Page.QuerySelectorAllAsync(
                        $"(//table)[3]/tbody/tr/td[text()='{name}']/following-sibling::*/input");

                // Pick random checkbox to click on from list
                var contaminantRfc = selectContaminants.PickRandom(new Random().Next(0, selectContaminants.Count));

                foreach (var item in contaminantRfc)
                    await item.ClickAsync(new ElementHandleClickOptions
                    {
                        Force = true,
                        Button = MouseButton.Left
                    });
            }
        }

        public async Task ClickAllMediaAffected()
        {
            // Gets all name of the media affected
            var mediaNameElements = await Context.Page.QuerySelectorAllAsync("(//table)[4]/tbody/tr/td[1]");

            var mediaNames = mediaNameElements.Select(m => m.InnerTextAsync().Result).ToList();

            foreach (var media in mediaNames)
            {
                var affectedAndCleanUpCheckboxes =
                    await Context.Page.QuerySelectorAllAsync(
                        $"(//table)[4]/tbody/tr/td[text()='{media}']/following-sibling::*/input");

                var randomMediaCheckboxes =
                    affectedAndCleanUpCheckboxes.PickRandom(new Random().Next(0, affectedAndCleanUpCheckboxes.Count));

                foreach (var mediaBox in randomMediaCheckboxes)
                    await mediaBox.ClickAsync(new ElementHandleClickOptions
                    {
                        Force = true,
                        Button = MouseButton.Left
                    });
            }
        }

        public async Task FillRedevelopmentStartAndCompletionDates(string startDate, string endDate)
        {
            var startEpoch = new TimestampToDateHelper(startDate).LongEpoch;
            var elementHandle = Context.Page.QuerySelectorAsync("#redev_start_date").Result;

            if (elementHandle != null)
            {
                await elementHandle.FocusAsync();

                _redevelopmentDays =
                    await Context.Page.QuerySelectorAllAsync($"table>tbody>tr>td[data-date='{startEpoch}']");

                if (_redevelopmentDays.Count <= 0)
                {
                    var previous =
                        await Context.LocatorAsync("//div[@class='datepicker-days']/table/thead/tr/th[@class='prev']");
                    await previous.Locator.HoverAsync();

                    await previous.Locator.ClickAsync(new LocatorClickOptions
                    {
                        Button = MouseButton.Left,
                        ClickCount = 1,
                        Force = true
                    });

                    _redevelopmentDays =
                        await Context.Page.QuerySelectorAllAsync($"table>tbody>tr>td[data-date='{startEpoch}']");

                    var previousMonth = _redevelopmentDays.LastOrDefault();

                    if (previousMonth != null)
                    {
                        await previousMonth.HoverAsync();
                        await previousMonth.ClickAsync();
                    }
                }
                else
                {
                    await _redevelopmentDays.FirstOrDefault()?.HoverAsync()!;
                    await _redevelopmentDays.FirstOrDefault()?.ClickAsync()!;
                    await Context.Page.Keyboard.PressAsync("Enter");
                }

                var result = Context.Page.QuerySelectorAsync("#redev_completion_date").Result;

                if (result != null)
                    await result.FocusAsync();
            }

            var redevelopmentCompletionDays =
                await Context.Page.QuerySelectorAllAsync(
                    $"table>tbody>tr>td[data-date='{new TimestampToDateHelper(endDate).LongEpoch}']");
            await redevelopmentCompletionDays.LastOrDefault()?.HoverAsync()!;
            await redevelopmentCompletionDays.LastOrDefault()?.ClickAsync()!;
            await Context.Page.Keyboard.PressAsync("Enter");
        }

        public async Task AddRedevelopmentLeverageFunding(RedevelopmentLeverageFundingSource redevelopmentLeverage)
        {
            await Context.LocatorAsync("#redevFunding_add").Result.LeftClickAsync();

            await Context.Page.SelectOptionAsync("#gpf_source_type_id", new SelectOptionValue
            {
                Label = redevelopmentLeverage.SourceOfFunding
            }, new PageSelectOptionOptions
            {
                Force = true,
                Timeout = 5000
            });

            await Context.LocatorAsync("#funding_entity_name").Result
                .FillWithText(redevelopmentLeverage.EntityProvidingFunding);

            await Context.LocatorAsync("#activity_funded").Result.FillWithText(redevelopmentLeverage.ActivityFunded);

            await Context.LocatorAsync("#gpf_amount").Result
                .FillWithText(redevelopmentLeverage.AmountFundingExpended.ToString(CultureInfo.InvariantCulture));

            await Context.LocatorAsync("#saveNew").Result.LeftClickAsync();

            await Context.Page.WaitForTimeoutAsync(4000);
        }

        public async Task<string> LeverageFundingTotals()
        {
            var totalReDevFunding = await Context.Page.QuerySelectorAsync("#TotalRedevLevgFunding");

            await totalReDevFunding?.WaitForElementStateAsync(ElementState.Stable,
                new ElementHandleWaitForElementStateOptions
                {
                    Timeout = 5000
                })!;
            return totalReDevFunding?.InnerTextAsync().Result;
        }

        private async Task SelectSourceOfFunding(string sourceFundingType)
        {
            await Context.Page.SelectOptionAsync("#gpf_source_type_id", new SelectOptionValue
            {
                Label = sourceFundingType
            }, new PageSelectOptionOptions {Timeout = 500, NoWaitAfter = false});
        }
    }
}