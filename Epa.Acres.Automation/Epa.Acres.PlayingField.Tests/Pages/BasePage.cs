﻿using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.Settings;
using Microsoft.Playwright;

namespace Epa.Acres.PlayingField.Tests.Pages
{
    public abstract class BasePage
    {
        protected readonly SessionContext Context;

        private PageClickOptions SetPageClickOption { get; } = new PageClickOptions
        {
            Timeout = 30000,
            Button = MouseButton.Left,
            Force = true
        };

        public BasePage(SessionContext context)
        {
            Context = context;
        }

        public async Task<AddNewPropertyPage> AddNewProperty()
        {
            await Context.Page.HoverAsync(
                "//a[@id='navbarDropdown']/following-sibling::ul/li/a[text()='Add New Property']");

            return new AddNewPropertyPage(Context);
        }

        private async Task GoToHomePage()
        {
            await Context.Page.ClickAsync("//a[@href='/acres6/showHomepage']");
        }

        public virtual async Task GotoPage()
        {
            await GoToHomePage();
        }

        protected async void QuickStartClick()
        {
            await Context.Page.ClickAsync("//a[@id='navbarDropdown' and contains(text(),'Quick Start')]");
        }

        protected async void ReportsClick()
        {
            await Context.Page.ClickAsync("//a[@id='navbarDropdown' and contains(text(),'Reports')]",
                SetPageClickOption);
        }

        protected static PageWaitForURLOptions UrlWaitOptions(float timeout = 5000,
            WaitUntilState browserState = WaitUntilState.DOMContentLoaded)
        {
            return new PageWaitForURLOptions {Timeout = timeout, WaitUntil = browserState};
        }
    }
}