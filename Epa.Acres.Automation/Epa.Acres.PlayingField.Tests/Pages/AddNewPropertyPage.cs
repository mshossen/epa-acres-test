﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Epa.Acres.Core.Extensions;
using Epa.Acres.Core.Models;
using Epa.Acres.PlayingField.Tests.Pages.GlobalPages;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;
using Microsoft.Playwright;
using Models.RestApiModel;
using Newtonsoft.Json;

namespace Epa.Acres.PlayingField.Tests.Pages
{
    public class AddNewPropertyPage : BasePage
    {
        private const string PpfEnvironmentActiveUrl = "**/acres6/jquery/ppf/ppfEnvironmentalActivities/**";

        /// <summary>
        /// Save Continue button on Step 1 of property creation random
        /// </summary>
        private Task ClickSaveContinueToNextStep()
        {
            return Context.Page.QuerySelectorAllAsync("button.btn.btn-sm.btn-success")
                .Result.SelectARandomElement().ClickAsync();
        }

        public AddNewPropertyPage(SessionContext context) : base(context)
        {
        }

        public async Task<string> AddAssessmentActivity()
        {
            var response = await Context.Page.WaitForResponseAsync(PpfEnvironmentActiveUrl,
                new PageWaitForResponseOptions {Timeout = 5000});

            return response.TextAsync().Result;
        }

        /// <summary>
        /// AC6-1192 - must select at least one from the filter before apply filter
        /// </summary>
        /// <returns></returns>
        public async Task<Grant> ClickApplyFilter()
        {
            var response = Context.Page.WaitForResponseAsync("**/acres6/grant/getGrant/*",
                new PageWaitForResponseOptions
                {
                    Timeout = 10000
                });

            await Context.Page.WaitForURLAsync("**/ppf/ppfPropertySearch**");
            await Context.Page.WaitForTimeoutAsync(500);

            var keyword = await Context.Page.QuerySelectorAsync("#keyword");
            keyword?.FocusAsync().Wait(500);
            const string word = "boise";
            var wordArray = word.ToArray();

            foreach (var t in wordArray) await Context.Page.Keyboard.PressAsync(t.ToString());

            await Context.Page.ClickAsync($"#page-apply-filter", new PageClickOptions
            {
                Button = MouseButton.Left,
                Force = true,
                Timeout = 10000
            });

            var grant = await response.Result.TextAsync();

            return JsonConvert.DeserializeObject<Grant>(grant);
        }

        public async Task<AddNewPropertyPage> ClickCreateNewProperty()
        {
            var addNewButtons = await Context.Page.QuerySelectorAllAsync("a.page-add-new-btn.btn.btn.btn-success");

            await addNewButtons.SelectARandomElement().ClickAsync(new ElementHandleClickOptions
            {
                Timeout = 5000
            });
            return this;
        }

        public async Task<PpfEnvironmentalActivitiesPage> PpfContinueNextStep(Grant grant)
        {
            await Context.Page.ClickAsync("#continue", new PageClickOptions
            {
                Force = true,
                Button = MouseButton.Left,
                Timeout = 10000
            });

            await Context.Page.WaitForURLAsync($"**/acres6/jquery/ppf/**grantId={grant.GrantId}**", UrlWaitOptions());

            return new PpfEnvironmentalActivitiesPage(Context);
        }

        public async Task<AddNewPropertyPage> FillPropertyInfo(MockPropertyModel property, int? year = null)
        {
            await Context.Page.WaitForURLAsync("**/acres6/jquery/ppf/ppfGeneralInfo?grantId=*&action=*",
                UrlWaitOptions());

            // older grants for FY2020 or below will still have the option to select Hazadous or Petroleum
            // or both options
            if (year <= 2020)
            {
                var hazPets = await Context.Page.QuerySelectorAllAsync("//label[@for='haz_pet']/input");

                await hazPets[new Random().Next(hazPets.Count)].ClickAsync();
            }

            await Context.LocatorAsync("#property_name").Result.FillWithText($"{property.PropertyName}");
            await Context.LocatorAsync("#address1").Result.FillWithText($"{property.Address}");
            await Context.LocatorAsync("#zip_code").Result.FillWithText(property.ZipCode);

            await Context.LocatorAsync("#city").Result.FillWithText(property.City);
            await Context.Page.SelectOptionAsync("#state_id", new SelectOptionValue {Label = property.State});

            await Context.LocatorAsync("input[name=\"property_size\"]").Result
                .FillWithText(property.Acres.ToString(CultureInfo.InvariantCulture));

            return this;
        }

        public IEnumerable<string> GetCaTypeFromNarrowResult()
        {
            return Context.Page.QuerySelectorAllAsync("//table[@id='coopAgreementTable']/tbody/tr/td[5]").Result
                .Select(t => t.InnerTextAsync().Result).ToList();
        }

        public override async Task GotoPage()
        {
            QuickStartClick();
            await Context.LocatorAsync("//li/a[contains(text(),'Add New Property')]").Result.LeftClickAsync();
            await Context.Page.WaitForURLAsync("**/acres6/jquery/*/showCAList?viewMode=addProperty", UrlWaitOptions());
        }

        public async Task<AddNewPropertyPage> NarrowResults(string search)
        {
            await Context.LocatorAsync("//input[@aria-controls='coopAgreementTable']").Result.FillWithText(search);

            return this;
        }

        public async Task<AddNewPropertyPage> SelectRandomCa(string caStatus = "Open")
        {
            var selectCAs = await Context.Page.QuerySelectorAllAsync(
                $"//table[@id='coopAgreementTable']/tbody/tr/td[contains(text(), '{caStatus}')]/preceding-sibling::td/input[@id='grantId']");

            await selectCAs.SelectARandomElement().ClickAsync();
            await ClickSaveContinueToNextStep();

            return this;
        }

        public async Task<IElementHandle> CheckEnvironmentActivitiesInformationSaved()
        {
            return await Context.Page.QuerySelectorAsync(
                "//div[contains(., 'Successfully updated Environmental Activities Information.') and @role]");
        }

        public async Task ReviewAndSubmit()
        {
            await Context.LocatorAsync("#submitApproval").Result.LeftClickAsync();
        }

        public async Task<string> WorkPackageStatus(string propertyName, int grantId)
        {
            var wpStatus = await Context.Page.QuerySelectorAsync(
                $"//table[@id='propertyTable']/tbody/tr/td/a[text()='{propertyName.Replace("'", "\'")}']/parent::td/following-sibling::td[2]");

            await Context.Page.WaitForURLAsync($"**/acres6/jquery/carmain/showCA?grantId={grantId}**",
                new PageWaitForURLOptions
                {
                    Timeout = 7000,
                    WaitUntil = WaitUntilState.Load
                });
            await wpStatus?.WaitForElementStateAsync(ElementState.Stable)!;
            return wpStatus?.InnerTextAsync().Result;
        }
    }
}