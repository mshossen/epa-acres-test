﻿using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;

namespace Epa.Acres.PlayingField.Tests.Pages
{
    public class AddCaToCarAccountPage : BasePage
    {
        public AddCaToCarAccountPage(SessionContext context) : base(context)
        {
        }

        public override async Task GotoPage()
        {
            QuickStartClick();
            await Context.LocatorAsync("//li/a[contains(text(),'Add Cooperative Agreement')]").Result.LeftClickAsync();
            await Context.Page.WaitForURLAsync("**/acres6/jquery/carmain/showAddCA", UrlWaitOptions());
        }
    }
}