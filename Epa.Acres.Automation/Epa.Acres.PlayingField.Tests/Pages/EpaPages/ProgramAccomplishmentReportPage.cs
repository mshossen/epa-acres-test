﻿using System.Linq;
using System.Threading.Tasks;
using Epa.Acres.PlayingField.Tests.PwExtensions;
using Epa.Acres.PlayingField.Tests.Settings;
using Microsoft.Playwright;
using Models.TestDataModel;

namespace Epa.Acres.PlayingField.Tests.Pages.EpaPages
{
    internal sealed class ProgramAccomplishmentReportPage : BasePage
    {
        public ProgramAccomplishmentReportPage(SessionContext context) : base(context)
        {
        }

        public override async Task GotoPage()
        {
            ReportsClick();

            await Context
                .LocatorAsync(
                    "//li/a[@class='dropdown-item' and contains(text(),'Program Accomplishment Report (PAR)')]").Result
                .LeftClickAsync();
        }

        public async Task<bool> IsProgramAccomplishmentReport()
        {
            await Context.Page.WaitForURLAsync("**/acres6/reports/showPARReport", UrlWaitOptions());
            var h2Locator = await Context.LocatorAsync("h2.acres-page-title");
            //	System.Console.WriteLine(h2locator.Locator.InnerTextAsync().Result);
            return await h2Locator.Locator.IsVisibleAsync();
        }

        public async Task SelectAccomplishmentType(ProgramAccomplishmentReportModel programAccomplishment)
        {
            var accompType = Context.LocatorAsync("#accomptype-selected-text")
                .Result.Locator;


            await accompType.WaitForAsync(new LocatorWaitForOptions
            {
                State = WaitForSelectorState.Attached,
                Timeout = 10000
            });

            await accompType.ClickAsync();

            var selectAccomplishments = programAccomplishment.SelectRandomAccomplishmentTypes;


            for (var i = 0; i < selectAccomplishments.Count(); i++)
            {
                var element = await Context.LocatorAsync(
                    $"//ul[@id='multiselectUL-accomptype']/li/a/label/input[ancestor::label/text()[contains(.,'{selectAccomplishments.ElementAt(i)}')]]");

                if (!element.Locator.IsCheckedAsync().Result) await element.LeftClickAsync();
            }
        }

        public async Task SelectCaType(ProgramAccomplishmentReportModel programAccomplishment)
        {
            await Context.LocatorAsync("//body").Result.LeftClickAsync();

            await Context.LocatorAsync("#catype-selected-button").Result.LeftClickAsync();

            var caDataTypes = programAccomplishment.SelectRandomCaTypes;

            for (var i = 0; i < caDataTypes.Count(); i++)
            {
                var caTypes = await Context.LocatorAsync(
                    $"//ul[@id='multiselectUL-catype']/li/a/label/input[ancestor::label/text()[contains(.,'{caDataTypes.ElementAt(i)}')]]");

                if (!caTypes.Locator.IsCheckedAsync().Result) await caTypes.LeftClickAsync();
            }
        }

        internal async Task SelectLocationRegionAndState(int region, string state)
        {
            var selectLocation = await Context.LocatorAsync("#location-selected-button");
            await selectLocation.LeftClickAsync();

            var clearAll = await Context.LocatorAsync("#clearAllRegionsLink");
            await clearAll.LeftClickAsync();

            var regionLabel = await Context.LocatorAsync($"//b[text()='Region {region}']/ancestor::label/ancestor::a");
            await regionLabel.LeftClickAsync();

            var stateLabel =
                await Context.LocatorAsync($"//input[@name='state' and ancestor::label/text()[contains(.,'{state}')]]");
            await stateLabel.LeftClickAsync();
        }

        internal async Task RunReport()
        {
            await Context.LocatorAsync("#submitbuttonsave").Result.LeftClickAsync();
        }

        internal async Task SelectFiscalYear(int year)
        {
            await Context.LocatorAsync("//body").Result.LeftClickAsync();
            await Context.LocatorAsync("//button[@id='year-selected-button']").Result.LeftClickAsync();
            //Console.WriteLine(year);
            var selectYear = await Context.LocatorAsync($"//input[@value='FY{year}']");

            if (!selectYear.Locator.IsCheckedAsync().Result) await selectYear.LeftClickAsync();
        }
    }
}