﻿using System;
using System.Collections.Generic;
using Epa.Acres.PlayingField.Tests.Pages;

namespace Epa.Acres.PlayingField.Tests.Settings
{
    public static class PageFactory
    {
        private static readonly Dictionary<(Type, SessionContext), BasePage> InternalPageDictionary =
            new Dictionary<(Type, SessionContext), BasePage>();

        private static readonly object LockDictionary = new object();

        public static TPage BuildPage<TPage>(SessionContext context) where TPage : BasePage
        {
            lock (LockDictionary)
            {
                return RegisterPage<TPage>(context);
            }
        }

        private static TPage RegisterPage<TPage>(SessionContext context) where TPage : BasePage
        {
            var key = (typeof(TPage), context);
            if (InternalPageDictionary.TryGetValue(key, out var page)) return (TPage) page;

            var registerPage = (TPage) Activator.CreateInstance(typeof(TPage), context);
            InternalPageDictionary.Add(key, registerPage);
            return registerPage;
        }
    }
}