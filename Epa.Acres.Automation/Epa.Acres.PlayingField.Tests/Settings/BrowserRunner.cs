﻿using System.Collections;

namespace Epa.Acres.PlayingField.Tests.Settings
{
    public class BrowserRunner : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            var browsers = new string[]
            {
                "chrome"
                //"firefox"
            };

            foreach (var browser in browsers) yield return browser;
        }
    }
}