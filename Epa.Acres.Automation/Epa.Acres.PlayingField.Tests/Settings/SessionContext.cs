﻿using System;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Microsoft.Playwright;

namespace Epa.Acres.PlayingField.Tests.Settings
{
    public class SessionContext
    {
        public string RunOnBrowser;

        //public IReportingService TeamsNotification { get; }
        public SessionContext()
        {
            UserService = ContainerConfig.ConfigBuilder().Resolve<IUserService>();
            //SQLiteLog = ContainerConfig.ConfigBuilder().Resolve<ISQLiteDataAccess>();
            //Features = ContainerConfig.ConfigBuilder().Resolve<IFeatureInfoProcessor>();
            //TestPlan = ContainerConfig.ConfigBuilder().Resolve<ITestPlanProcessor>();
            //ScenarioLog = ContainerConfig.ConfigBuilder().Resolve<IScenarioInfoProcessor>();
            //NewPropertyLog = ContainerConfig.ConfigBuilder().Resolve<INewPropertyProcessor>();
            //OracleSql = ContainerConfig.ConfigBuilder().Resolve<IOracleDataAccess>();
            //TeamsNotification = ContainerConfig.ConfigBuilder().Resolve<IReportingService>();
        }

        public IBrowser Browser { get; set; }
        public ILocator Locator { get; set; }
        public IBrowserContext BrowserContext { get; set; }

        //public ISQLiteDataAccess SQLiteLog { get; }
        //public IFeatureInfoProcessor Features { get; }
        //public ITestPlanProcessor TestPlan { get; }
        //public IScenarioInfoProcessor ScenarioLog { get; }
        //public INewPropertyProcessor NewPropertyLog { get; }
        //public IOracleDataAccess OracleSql { get; }
        public IPage Page { get; set; }

        public IPlaywright Playwright { get; set; }

        public IUserService UserService { get; }
        public string TestMethod { get; internal set; }
        public string TestId { get; internal set; }

        public string SaveReportPath()
        {
            return
                @$"C:\LinTechProjects\PlaywrightReport\{RunOnBrowser}\{DateTime.Now.Year}\{DateTime.Now:MMMM}\{DateTime.Now.Day}";
        }
    }
}