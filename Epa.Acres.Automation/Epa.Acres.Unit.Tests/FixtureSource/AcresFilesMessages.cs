﻿using System.Collections;

namespace Epa.Acres.Unit.Tests.FixtureSource
{
    public class AcresFilesMessages : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] {"System.Out.PrintLn"};
            yield return new object[] {"Console.log"};
        }
    }
}