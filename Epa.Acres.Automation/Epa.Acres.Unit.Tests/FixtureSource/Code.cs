﻿namespace Epa.Acres.Unit.Tests.FixtureSource
{
    public class Code
    {
        public int Line { get; set; }
        public string Text { get; set; }
    }
}