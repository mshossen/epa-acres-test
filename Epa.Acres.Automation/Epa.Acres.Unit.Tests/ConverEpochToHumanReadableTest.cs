﻿using Epa.Acres.Core.Helper;
using FluentAssertions;
using NUnit.Framework;

namespace Epa.Acres.Unit.Tests
{
    public class ConverEpochToHumanReadableTest
    {
        [TestCase(1635638400000, 2021)]
        [TestCase(1567137600000, 2019)]
        public void ConverEpochToUtcNowTest(long epoch, int year)
        {
            new TimestampToDateHelper(epoch)
                .Year
                .Should()
                .Be(year);
        }

        [Test]
        public void ConvertStringToEpoch()
        {
            var dateFormat = "10/31/2021";

            new TimestampToDateHelper(dateFormat)
                .LongEpoch
                .Should()
                .Be(1635638400000);
        }
    }
}