﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Epa.Acres.Unit.Tests.FixtureSource;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.Unit.Tests
{
    [TestFixtureSource(typeof(AcresFilesMessages))]
    [Ignore("manually run test")]
    [Parallelizable]
    public class CheckPrintLineMessageAcresProjectFileTests
    {
        private readonly string _code;

        private readonly List<CodeCleanup> cleanup = new List<CodeCleanup>();

        public CheckPrintLineMessageAcresProjectFileTests(string code)
        {
            _code = code;
        }

        [Test]
        public void CheckForPrintLineTest()
        {
            var files = Directory.GetFiles(@"C:\LinTechProjects\acres6\src", ".", SearchOption.AllDirectories);

            var sFiles = files.Where(f => f.EndsWith(".java") || f.EndsWith(".jsp"));

            foreach (var file in sFiles)
            {
                var fileConents = File.ReadAllLines(file);

                if (fileConents.Any(c => c.ToLower().Contains(_code.ToLower())))
                {
                    var codeCleanup = new CodeCleanup
                    {
                        FileName = file.Split('\\').Last(),
                        Codes = new List<Code>()
                    };

                    var line = 1;

                    foreach (var content in fileConents)
                    {
                        if (content.ToLower().Contains(_code.ToLower()))
                        {
                            var c = new Code
                            {
                                Line = line,
                                Text = content
                            };

                            codeCleanup.Codes.Add(c);
                        }

                        line += 1;
                    }

                    cleanup.Add(codeCleanup);
                }
            }

            TestContext.Out.Write(JsonConvert.SerializeObject(cleanup, Formatting.Indented));
        }
    }
}