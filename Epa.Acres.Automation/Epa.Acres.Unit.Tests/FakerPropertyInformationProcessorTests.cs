﻿using System;
using Epa.Acres.Core.DataSource;
using Epa.Acres.Core.Processors;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.Unit.Tests
{
    public class FakerPropertyInformationProcessorTests
    {
        [Test]
        public void CreatePropertyAddressByStateTest()
        {
            var property = new FakerPropertyInformationData();
            var address = FakerPropertyInformationData.CreateProperty("ID");
            Console.WriteLine(JsonConvert.SerializeObject(address, Formatting.Indented));
        }

        [Test]
        public void FakeProprtyDataTest()
        {
            var faker = new FakerPropertyInformationData();

            Console.WriteLine(JsonConvert.SerializeObject(FakerPropertyInformationData.CreateProperties(),
                Formatting.Indented));
        }

        [Test]
        public void SvrpCleanupTest()
        {
            Console.WriteLine(JsonConvert.SerializeObject(MockSvrpContactInfoProcess.CreateContactInfo(),
                Formatting.Indented));
        }
    }
}