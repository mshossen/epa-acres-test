﻿using System;
using System.IO;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models;
using FluentAssertions;
using FluentAssertions.Execution;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Epa.Acres.Unit.Tests
{
    [TestFixture]
    internal class UserSecretsTests
    {
        private static IUserService UserService => ContainerConfig.ConfigBuilder().Resolve<IUserService>();

        [Test]
        public void ConfirmUserSecrets_IsNotNull()
        {
            using (new AssertionScope())
            {
                UserService.UserConfig.AcresUrl.Should().NotBeNullOrEmpty();
                UserService.UserConfig.Password.Should().NotBeNullOrEmpty();

                UserService.UserConfig.CarUsers.Should().HaveCount(x => x >= 1)
                    .And.NotBeEmpty();
            }
        }

        [Test]
        public void ProjectLocation()
        {
            var location = typeof(UserSecretsTests).Assembly.Location.ToString();
            //nameof(Epa.Acres.Unit.Tests).Assembly
            Console.WriteLine(Directory.GetParent(location).Parent.Parent.Parent.ToString());
        }

        [Test]
        public void ConvertToJsonTest()
        {
            Console.WriteLine(JsonConvert.SerializeObject(new UserConfig()));
        }
    }
}