﻿using System;
using System.IO;
using System.Threading;
using Epa.Acres.Core.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace Epa.Acres.LoadPerformance.Tests
{
    public static class SeleniumExtension
    {
        public static IWebElement GetActiveElement(this IWebDriver driver, By locator, double seconds = 180,
            double milliSeconds = 250)
        {
            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(seconds),
                PollingInterval = TimeSpan.FromMilliseconds(milliSeconds),
                Message = $"Unable to locate element {locator}, timed out after seconds(s): {seconds}"
            };

            wait.IgnoreExceptionTypes(new[]
            {
                typeof(NoSuchElementException), typeof(ElementNotVisibleException),
                typeof(StaleElementReferenceException)
            });

            return wait.Until(e =>
            {
                var element = e.FindElement(locator);

                if (element.Displayed || element.Enabled) return element;

                throw new NoSuchElementException(wait.Message);
            });
        }

        public static void MouseHover(this IWebDriver driver, IWebElement element)
        {
            new Actions(driver).MoveToElement(element).Build().Perform();
        }

        public static void MouseHover(this IWebDriver driver, By locator)
        {
            new Actions(driver).MoveToElement(driver.GetActiveElement(locator)).Build().Perform();
        }

        public static void MouseHoverClick(this IWebDriver driver, IWebElement element)
        {
            var action = new Actions(driver);
            action.MoveToElement(element).Perform();
            action.Click().Build().Perform();
        }

        public static string TakeScreenShot(this IWebDriver driver, string customfilename = "",
            string path = ProjectSettings.ParallelScreenShotDir)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            var radom = new Random().Next(250, 1000);
            Thread.Sleep(radom);

            customfilename = !string.IsNullOrWhiteSpace(customfilename)
                ? customfilename
                : DateTime.Now.ToString("yyyyMMddHHmmsss");

            var screenShot = ((ITakesScreenshot) driver).GetScreenshot();
            var png = @$"{path}\{((RemoteWebDriver) driver).SessionId}-{customfilename}.png";

            screenShot.SaveAsFile(png, ScreenshotImageFormat.Png);
            return png;
        }
    }
}