﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Core.Models;
using Epa.Acres.Data.Acres.Model;
using FluentAssertions;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

[assembly: LevelOfParallelism(10)]
[assembly: Parallelizable(ParallelScope.Fixtures)]

namespace Epa.Acres.LoadPerformance.Tests.UserLoadTests
{
    [TestFixtureSource(typeof(LoadUserTypeData))]
    public class SimultaneousUserLoginTests
    {
        private readonly IUserService _user;
        private readonly UsersDto _userName;
        private readonly Dictionary<string, List<string>> _navKeyPairs = new Dictionary<string, List<string>>();
        private IWebDriver _driver;
        private List<string> _navigationSections;
        private string _screenShotLocation;
        private string _sessionId;
        private string _welcome;

        public SimultaneousUserLoginTests(UsersDto userName)
        {
            _userName = userName;
            _user = ContainerConfig.ConfigBuilder().Resolve<IUserService>();
        }

        [TearDown]
        public void AfterTest()
        {
            var hasError = TestContext.CurrentContext.Result.Outcome.Status ==
                           (TestStatus.Failed | TestStatus.Inconclusive | TestStatus.Warning)
                ? "Error-"
                : string.Empty;

            TestContext.AddTestAttachment(_driver.TakeScreenShot(
                $"{hasError}{_userName.First_Name}_{_userName.Last_Name}_{_userName.USER_ID}", _screenShotLocation));

            _driver.GetActiveElement(By.XPath(".//a[@href='/acres6/processLogout']")).Click();
        }

        [OneTimeSetUp]
        public void InvokeBrowser()
        {
            if (_driver is null)
            {
                new DriverManager().SetUpDriver(new ChromeConfig());
                _driver = new ChromeDriver(ChromeService(), ChromeOption());
                _driver.Navigate().GoToUrl(_user.UserConfig.AcresUrl);
                _driver?.Manage().Window.Maximize();
                _sessionId = (_driver as RemoteWebDriver)?.SessionId.ToString();
            }

            _screenShotLocation = $@"C:\LinTechProjects\Parallel\{_sessionId}\{_userName.USER_Name}";

            if (!Directory.Exists(_screenShotLocation)) Directory.CreateDirectory(_screenShotLocation);
        }

        [OneTimeTearDown]
        public void TearDownBrowser()
        {
            _driver?.Close();
            _driver?.Dispose();

            // var di = new DirectoryInfo();
            var allFiles = Directory.GetFiles(ProjectSettings.ParallelScreenShotDir, ".", SearchOption.AllDirectories);

            foreach (var file in allFiles)
                if (File.Exists(file))
                {
                    var fileInfo = new FileInfo(file);

                    if (fileInfo.LastWriteTime >= DateTime.Now.AddDays(-15)) continue;
                    fileInfo.Delete();
                    var directoryInfo = new DirectoryInfo(file).Parent;
                    if (directoryInfo != null) directoryInfo.Parent?.Delete(true);
                }
        }

        [Test]
        [Order(0)]
        public void UserLoginTest()
        {
            _driver.GetActiveElement(By.Id("userName")).SendKeys(_userName.USER_Name);
            _driver.GetActiveElement(By.Id("password")).SendKeys(_user.UserConfig.Password);
            _driver.GetActiveElement(By.XPath(".//input[@value='login' and @type='submit']")).Click();

            // wait for weclcome message to be longer than 9 char
            var i = 0;

            do
            {
                _welcome = string.Empty;

                var t = Task.Run(async delegate
                {
                    await Task.Delay(new Random().Next(500, 1500));
                    _welcome = _driver.GetActiveElement(By.Id("acresWelcome")).Text;
                });
                t.Wait();
                i++;
            } while (i < 5 && _welcome.Length <= 9);

            // check welcome message contains user full name
            _driver.GetActiveElement(By.Id("acresWelcome"))
                .Text.ToLower()
                .Should()
                .EndWith($"{_userName.First_Name.ToLower()} {_userName.Last_Name.ToLower()}");

            // collect all the navigation section names as string
            _navigationSections = _driver.FindElements(By.XPath(".//div[@id='mainNavBar']/ul[1]/li/a")).Skip(1)
                .Select(e => e.Text.Trim()).ToList();

            foreach (var item in _navigationSections)
            {
                var section =
                    _driver.GetActiveElement(By.XPath($".//div[@id='mainNavBar']/ul[1]/li/a[contains(.,'{item}')]"));

                // if block to build Dictionary Key Pair with section name and list of nav link name
                if (section.GetAttribute("id").Trim() == string.Empty)
                {
                    _navKeyPairs.Add(item, new List<string>());
                }
                else
                {
                    var dropdownNames = _driver
                        .FindElements(By.XPath(
                            $".//div[@id='mainNavBar']/ul[1]/li/a[contains(.,'{item}')]/following-sibling::ul/li/a"))
                        .Select(e => e.GetAttribute("innerText")).ToList();

                    _navKeyPairs.Add(item, dropdownNames);
                }
            }

            for (var n = 0; n < _navigationSections.Count(); n++)
                if (!_navKeyPairs[_navigationSections[n]].Any())
                {
                    _driver.MouseHoverClick(
                        _driver.GetActiveElement(
                            By.XPath($".//div[@id='mainNavBar']/ul[1]/li/a[contains(.,'{_navigationSections[n]}')]")
                        )
                    );
                    var j = 0;

                    do
                    {
                        Thread.Sleep(new Random().Next(500, 1500));
                        j++;
                    } while (!_driver.GetActiveElement(By.CssSelector("h2.acres-page-title")).Displayed
                             && j < 5);

                    var acresPageTitle = _driver.GetActiveElement(By.CssSelector("h2.acres-page-title")).Text;

                    TestContext.AddTestAttachment(_driver.TakeScreenShot($"{acresPageTitle.Replace(" ", "")}",
                        _screenShotLocation));

                    _driver.Navigate().Back();
                }
                else
                {
                    foreach (var page in _navKeyPairs[_navigationSections[n]])
                    {
                        _driver.MouseHoverClick(
                            _driver.GetActiveElement(
                                By.XPath($".//div[@id='mainNavBar']/ul[1]/li/a[contains(.,'{_navigationSections[n]}')]")
                            )
                        );

                        _driver.MouseHoverClick(
                            _driver.GetActiveElement(
                                By.XPath(
                                    $".//div[@id='mainNavBar']/ul[1]/li/a[contains(.,'{_navigationSections[n]}')]/following-sibling::ul/li/a[contains(., '{page}')]")
                            )
                        );

                        var j = 0;

                        do
                        {
                            Thread.Sleep(new Random().Next(500, 1500));
                            j++;
                        } while (!_driver.GetActiveElement(By.CssSelector("h2.acres-page-title")).Displayed
                                 && j < 5);

                        var acresPageTitle = _driver.GetActiveElement(By.CssSelector("h2.acres-page-title")).Text;

                        TestContext.AddTestAttachment(_driver.TakeScreenShot($"{acresPageTitle.Replace(" ", "")}",
                            _screenShotLocation));
                        _driver.Navigate().Back();
                    }
                }
        }

        private ChromeOptions ChromeOption()
        {
            var options = new ChromeOptions
            {
                AcceptInsecureCertificates = true,
                PageLoadStrategy = PageLoadStrategy.Normal
            };

            return options;
        }

        private ChromeDriverService ChromeService()
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.LogPath = $@"C:\LinTechProjects\Log\Chrome{DateTime.Now:yyyyMMddHHmmss}.log";

            return service;
        }
    }
}