﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Epa.Acres.Core.Config;
using Epa.Acres.Core.Interfaces;
using Epa.Acres.Data.Acres.Data;
using Epa.Acres.Data.Acres.Model;

namespace Epa.Acres.LoadPerformance.Tests.UserLoadTests
{
    public class LoadUserTypeData : IEnumerable<UsersDto>
    {
        public IEnumerator<UsersDto> GetEnumerator()
        {
            var userService = ContainerConfig.ConfigBuilder().Resolve<IUserService>();

            var users = new AcresUserQuery()
                .GetActiveUsers(userService.UserConfig.CarUsers.FirstOrDefault(u => u.Region.Equals(1010))?.UserName)
                .ToList();

            foreach (var user in users) yield return user;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}